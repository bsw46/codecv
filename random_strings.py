import random


characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.<>,;:'?/{}[]()*&^$#@!"
x=0

random_string = ""
string_length = 32
for i in range(string_length):
    x = random.randrange(len(characters))
    random_string += characters[x]

print(random_string)
