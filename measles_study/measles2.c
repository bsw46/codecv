/* 
   Monte Carlo simulation of Measles outbreaks
   with some initial diseased population

   Brandon S. Wong -- bsw46@drexel.edu

   Written for the course 
   CHE 614 Chem. Engr. Thermodynamics II - Winter 1718

   compile using "gcc -o measles measles.c -lm"

   runs as "./measles -L <sidelength(20)> \
                    -nd <numdays(100)> \
		    -s <seed(?)> -ps <initialPercentSick(.05)>"

   For example, to run a 20x20 system for 100 days
   where 5 percent of the population is sick
           
          ./ising -L 20 -nd 100 -ps .05
   
   The default values are shown in parentheses above.

   An external field is not implemented here.

   Drexel University, Department of Chemical Engineering
   Philadelphia
   (c) 2018
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

int main (int argc, char * argv[]) 
{

  // State space variables
  int ** P;          // The 2D array of population 
  int ** nextP;      // The population at the next time step
  int L = 20;        // The characteristic length of population 
  int N;             // The total number of spins = L*L 

  // Nearest neighbor parameters
  double vaccine;    // Vaccine potency 
  double vaccineRate;// Vaccination rate
  double infect=.25; // Infection parameters    
  double chanceSick; //  
  int contagiousN;   // sick neighbors

  // Disease parameters
  int minDays=7;     // Days until rash develops
  int maxDays=18;
  int daysCon=9;

  // Run parameters
  double ps=.01;     // percent sick 
  double pt=.01;     // percent travel
  int nDays= 100; // number of MC cycles to run; one cycle is N 
			   // consecutive attempted spin flips 
  int sims=1000; 
  
  // Default parameters
  vaccineRate=85;
  vaccine=93;

  // Run observables
  int a,b;              // a - total infected, b - current contagious

  
  int initSick;         // Number of initially sick in a run
  int sumInit=0;        // Number of initially sick in all runs

  int sickOut;          // Number of sick people who leave in a run
  int sickOutSum=0;     // Number of sick who left in all runs
  int * contagiousCount;
  int sum;

  // Computational variables 
  double x;             // random number 
  int i,j,k;            // loop counters - position, position, sick neighbors
  int c,d;              // loop counters - day in sim, sim

  // Here we parse the command line arguments 
  for (i=1;i<argc;i++) {
    if (!strcmp(argv[i],"-L")) L=atoi(argv[++i]);
    else if (!strcmp(argv[i],"-ps")) ps=atof(argv[++i]);
    else if (!strcmp(argv[i],"-nd")) nDays = (int)(atof(argv[++i]));
    else if (!strcmp(argv[i],"-vr")) vaccineRate=atof(argv[++i]);
    else if (!strcmp(argv[i],"-vp")) vaccine=atof(argv[++i]);
  }
  
  // Seed the random number generator 
  srand48((int)time(NULL));

  // Compute size of population
  N=L*L;

  // Allocate memory for the magnet (a 2-dimensional integer array) 
  P=(int**)malloc(L*sizeof(int*));
  nextP=(int**)malloc(L*sizeof(int*));
  contagiousCount=(int*)malloc(nDays*sizeof(int));
  for (i=0;i<L;i++) P[i]=(int*)malloc(L*sizeof(int));
  for (i=0;i<L;i++) nextP[i]=(int*)malloc(L*sizeof(int));

  // Multiple simulations
  for (d=0;d<sims;d++){
    // Initialize healthy population
    for (i=0;i<L;i++){
      for (j=0;j<L;j++){
        P[i][j]=0;
        nextP[i][j]=0;
        x=drand48();
        // vaccineRate determines if person vaccinates, random.
        if (x<vaccineRate*.01) P[i][j]=P[i][j]+1;
      }
    }

    // Initialize sick population
    a=0;
    initSick=0;
    for (i=0;i<L;i++){
      for (j=0;j<L;j++){
        x=drand48();
        if (x<ps){
          initSick=initSick+1;
          a=a+1;
          P[i][j]=100+4+(int)(minDays+(maxDays-minDays)*drand48());
        }
      }
    }
    sickOut=0;
    // perform nDays simulation
    for (c=0;c<nDays;c++) {
      b=0;
      // A precent of the population leaves
      for (k=0;k<(N*pt);k++){
        i=(int)(L*drand48());
        j=(int)(L*drand48());
        if (P[i][j]>100) sickOut=sickOut+1;
        x=drand48();
        P[i][j]=0;
        if (x<ps) P[i][j] = 100+4+(int)(minDays+(maxDays-minDays)*drand48());
        else {
          x=drand48();
          if (x<vaccineRate*.01) P[i][j]=1;
        }
      }

      // Generate new population state
      for (i=0;i<L;i++){
        for (j=0;j<L;j++){
          // Immune case - Stays immune
          if (P[i][j]==100) nextP[i][j]=P[i][j]; 
          // Carrying measles - Reduce sick timer. 
          else if (P[i][j]>100){
            nextP[i][j]=P[i][j]-1;
            if (P[i][j]<100+daysCon) b=b+1;
          }
          // Never been sick - May be infected
          else{
            // Contagious neighbors - neighbors are contagious for a fixed window.
            contagiousN=0;
            if (P[i?(i-1):(L-1)][j]>100&&P[i?(i-1):(L-1)][j]<100+daysCon) contagiousN=contagiousN+1; 
            if (P[(i+1)%L][j]>100&&P[(i+1)%L][j]<100+daysCon) contagiousN=contagiousN+1;
            if (P[i][j?(j-1):(L-1)]>100&&P[i][j?(j-1):(L-1)]<100+daysCon) contagiousN=contagiousN+1;
            if (P[i][(j+1)%L]>100&&P[i][(j+1)%L]<100+daysCon) contagiousN=contagiousN+1;
            
            // Calculate chance of getting sick
            nextP[i][j]=P[i][j];
            for(k=0;k<contagiousN;k++){ 
              x=drand48();
              if (P[i][j]==0){
                if (x<infect) {
                  a=a+1;
                  // The 1004 refers to being contagious four days, the second term indicates when the rash developes.
                  nextP[i][j]=100+4+(int)(minDays+(maxDays-minDays)*drand48());  
                  break;
                }
              }
              else if (P[i][j]==1){
                if (x<infect*(1-vaccine*.01)){
                  a=a+1;
                  nextP[i][j]=100+4+(int)(minDays+(maxDays-minDays)*drand48());
                  break;
                }
              }
            }
          }
        }
      }
        // Update P space with nextP space.
      for (i=0;i<L;i++){
        for (j=0;j<L;j++){
          P[i][j]=nextP[i][j];
          if (nextP[i][j]>100) contagiousCount[c]=contagiousCount[c]+1;
        }
      }
    } // End of day
  } // End of all simulations
  for (i=0;i<nDays;i++){
    fprintf(stdout,"%4i %.4f \n",i+1,(double)contagiousCount[i]/(double)sims);
  }
} 
