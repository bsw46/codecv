#!/bin/bash
#
# Umbrella sampling
#
# CHE 614 Winter 17-18
# Drexel University
# Department of Chemical and Biological Engineering
# cameron f abrams (c) 2018
# cfa22@drexel.edu
#
#
# parameters in potential

# parameters for BD simulations

# initial window [$x1,$x2] and number of windows

gcc -o measles3 measles3.c -lm

# loop over intervals; conduct BD using ./bd and distribution 
# construction using ./avgh; update window boundaries
./measles3 -vr 70 > vp93.dat
./measles3 -vr 70 -vp 97 > vp97.dat

for (( n=71 ; n<=100; n++ )) 
   do
   ./measles3 -vr $n >> vp93.dat
   ./measles3 -vr $n -vp 97 >> vp97.dat
done


gnuplot mkplot.gp
gnuplot mkplot2.gp

# 
gcc -o measles2 measles2.c -lm

# loop over intervals; conduct BD using ./bd and distribution 
# construction using ./avgh; update window boundaries

./measles2 > contagiousvr85.dat
./measles2 -vr 100 > contagiousvr100.dat
./measles2 -vr 70 > contagiousvr70.dat


gnuplot mkplot3.gp

gcc -o measlesShow measlesShow.c -lm
