#!/usr/bin/gnuplot
set term png enhanced lw 5 size 1000,750 font ",24"
 
set out "sick_over_time.png"

set xr [0:100]
set xtics 0,20,100 font ",16"
set xlabel "Day in simulation" 
set yr [0:10]
set ytics 0,2,10 font ",16"
set ylabel "Average number of sick" 
set format x "%.1f"
set format y "%.1f"

set border 3
set xtics nomirror
set ytics nomirror

set key at 95,8

p "contagiousvr85.dat" u 1:2 t "vr 85" w l lc rgb "blue", "contagiousvr100.dat" u 1:2 t "vr 100" w l lc rgb "green" , "contagiousvr70.dat" u 1:2 t "vr 70" w l lc rgb "red"
