#!/usr/bin/gnuplot
set term png enhanced lw 5 size 1000,750 font ",24"
 
set out "total_infected.png"

set xr [70:100]
set xtics 70,10,100 font ",16"
set xlabel "Percent of population vaccinated" 
set yr [0:10]
set ytics 0,2,10 font ",16"
set ylabel "Infection ratio" 
set format x "%.1f"
set format y "%.1f"

set border 3
set xtics nomirror
set ytics nomirror

set key at 95,7

p "vp93.dat" u 1:2 t "one dose" w l lc rgb "blue", "vp97.dat" u 1:2 t "two dose" w l lc rgb "green" 
