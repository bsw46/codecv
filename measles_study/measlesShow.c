/* 
   Monte Carlo simulation of Measles outbreaks
   with some initial diseased population

   Brandon S. Wong -- bsw46@drexel.edu

   Written for the course 
   CHE 614 Chem. Engr. Thermodynamics II - Winter 1718

   compile using "gcc -o measles measles.c -lm"

   runs as "./measles -L <sidelength(20)> \
                    -nd <numdays(100)> \
		    -s <seed(?)> -ps <initialPercentSick(.05)>"

   For example, to run a 20x20 system for 100 days
   where 5 percent of the population is sick
           
          ./ising -L 20 -nd 100 -ps .05
   
   The default values are shown in parentheses above.

   An external field is not implemented here.

   Drexel University, Department of Chemical Engineering
   Philadelphia
   (c) 2018
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

int main (int argc, char * argv[]) {

  /* System parameters */
  int ** P;          // The 2D array of population 
  int ** nextP;
  int L = 20;        // The sidelength of the magnet 
  int N;             // The total number of spins = L*L 
  double vaccine;  // Vaccine immune rate 
  double vaccineRate;// Vaccination rate
  double infect=.4;      // Infection parameters    
  double chanceSick; // 
  double ps=.01;     // percent sick (initial)
  int contagiousN;   // sick neighbors
  int minDays=7;     // Days until rash develops
  int maxDays=18;
  /* Run parameters */
  int nDays= 100; /* number of MC cycles to run; one cycle is N 
			    consecutive attempted spin flips */
  
  // Initialization parameters
  //double initPercentSick = 0.05;
  vaccineRate=.85;
  vaccine=0.93;
  int daysCon=9;
  int pauseNDays=5;

  /* Computational variables */
  double x;       /* random number */
  int i,j,k,a,b,c;    /* loop counters */

  /* Observables */
  unsigned long int Seed = 23410981;

  /* Here we parse the command line arguments */
  for (i=1;i<argc;i++) {
    if (!strcmp(argv[i],"-L")) L=atoi(argv[++i]);
    else if (!strcmp(argv[i],"-ps")) ps=atof(argv[++i]);
    else if (!strcmp(argv[i],"-nd")) nDays = (int)(atof(argv[++i]));
    else if (!strcmp(argv[i],"-vr")) vaccineRate=atof(argv[++i]);
    else if (!strcmp(argv[i],"-vp")) vaccine=atof(argv[++i]);
  }
  
  /* Seed the pseudorandom number generator */
  srand48((int)time(NULL));

  /* Compute the number of spins */
  N=L*L;

  /* Allocate memory for the magnet (a 2-dimensional integer array) */
  P=(int**)malloc(L*sizeof(int*));
  nextP=(int**)malloc(L*sizeof(int*));
  for (i=0;i<L;i++) P[i]=(int*)malloc(L*sizeof(int));
  for (i=0;i<L;i++) nextP[i]=(int*)malloc(L*sizeof(int));


  /* Generate an initial configuration */
  for (i=0;i<L;i++){
    for (j=0;j<L;j++){
      P[i][j]=0;
      nextP[i][j]=0;
      x=drand48();
      // How many vaccines did the person receive??
      if (x<vaccineRate) P[i][j]=P[i][j]+1;
    }
  }
  // Initialize sick population 
  //  P[9][9]=110;
  //  P[9][10]=110;
  //  P[10][9]=110;
  //  P[10][10]=110;
  a=0;
  for (i=0;i<L;i++){
    for (j=0;j<L;j++){

      x=drand48();
      if (x<ps){
        a=a+1;
        P[i][j]=110;
      }
    }
  }
  // perform nDays simulation
  for (c=0;c<nDays;c++) {
    b=0;
    // Generate new population state
    for (i=0;i<L;i++){
      for (j=0;j<L;j++){
        // Immune case - Stays immune
        if (P[i][j]==100) nextP[i][j]=P[i][j]; 
        // Carrying measles - Reduce sick timer. 
        else if (P[i][j]>100){
          nextP[i][j]=P[i][j]-1;
          if (P[i][j]<100+daysCon) b=b+1;
        }
        // Never been sick - May be infected
        else{
          // Contagious neighbors - neighbors are contagious for a fixed window.
          contagiousN=0;
          if (P[i?(i-1):(L-1)][j]>100&&P[i?(i-1):(L-1)][j]<100+daysCon) contagiousN=contagiousN+1; 
          if (P[(i+1)%L][j]>100&&P[(i+1)%L][j]<100+daysCon) contagiousN=contagiousN+1;
          if (P[i][j?(j-1):(L-1)]>100&&P[i][j?(j-1):(L-1)]<100+daysCon) contagiousN=contagiousN+1;
          if (P[i][(j+1)%L]>100&&P[i][(j+1)%L]<100+daysCon) contagiousN=contagiousN+1;
          
          // Calculate chance of getting sick
          nextP[i][j]=P[i][j];
          for(k=0;k<contagiousN;k++){ 
            x=drand48();
            if (P[i][j]==0){
              if (x<infect) {
                a=a+1;
                // The 1004 refers to being contagious four days, the second term indicates when the rash developes.
                nextP[i][j]=100+4+(int)(minDays+(maxDays-minDays)*drand48());  
                break;
              }
             }
            else if (P[i][j]==1){
              if (x<infect*(1-vaccine)){
                a=a+1;
                nextP[i][j]=100+4+(int)(minDays+(maxDays-minDays)*drand48());
                break;
              }
            }
          }
        }
      }
    }
    fprintf(stdout,"  Day: %-5i |  # contagious: %-5i |  # infected: %-5i\n",c,b,a);
    fprintf(stdout,"  percent vaccinated: %-5.2f  |  vaccine potency: %-5.2f \n",vaccineRate*100,vaccine);
    for (i=0;i<L;i++){
      for (j=0;j<L;j++){
        P[i][j]=nextP[i][j];
        fprintf(stdout,"%-3i ",P[i][j]);
      }
    fprintf(stdout,"\n");
    }
  fprintf(stdout,"\n");
  getchar(); 
  }
  
}
