#!/usr/bin/gnuplot
set term png enhanced lw 5 size 1000,750 font ",24"
 
set out "sick_leaving_population.png"

set xr [70:100]
set xtics 70,10,100 font ",16"
set xlabel "Percent of population vaccinated" 
set yr [0:2]
set ytics 0,.5,2 font ",16"
set ylabel "Sick to leave per initially sick population" 
set format x "%.1f"
set format y "%.1f"

set border 3
set xtics nomirror
set ytics nomirror

set key at 95,1.5

p "vp93.dat" u 1:3 t "one dose" w l lc rgb "blue", "vp97.dat" u 1:3 t "two dose" w l lc rgb "green" 
