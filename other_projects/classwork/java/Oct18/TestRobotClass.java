/* Program: testRobotClass.java
 * Purpose: This program tests the Robot class, defined in Robot.java. 
 * 			The program instantiates a new Robot object, then utilizes all of its different methods to demonstrate the class' features.
 *          
 * Notes:   Flavor text is added for fun.
 *
 * Author:  Brandon S. Wong
 * Date:    October 18, 2017
 * 
 */

public class TestRobotClass {

	public static void main(String[] args) {
		System.out.print("Welcome to the robot production and test facility!\n\n");
		
		// Create the Robot
		System.out.print("A scientific research facility creates a robot for travelling great distances.\n");
		Robot testRobot = new Robot();
		
		// Get its current Speed
		System.out.print("With its default charge, the robot's current speed is " + testRobot.currentSpeed() + ".\n");

		// Have it move forward 9 units
		System.out.print("The scientists test the robots movement abilities: the robot moves forward 9 units. \n");
		testRobot.moveForward(9);

		// Get its current Speed
		System.out.print(
				"After travelling 9 units, the robot's current speed drops to " + testRobot.currentSpeed() + ".\n");

		// Have it calculate its estimated time home
		System.out.print("The scientists realize the robot is now much slower. \n" + "The robot estimates it will take "
				+ testRobot.estTimeHome() + " units of time to get home.\n");

		// Charge it by 5 units.
		System.out.print(
				"The scientists decide the robot is too slow, so they give the robot an extra 5 new batteries. \n"
						+ "The robot is charged so it can travel faster!\n");
		testRobot.charge(5);

		// Get its current Speed
		System.out.print(
				"After being charged, the robot's current speed increases to " + testRobot.currentSpeed() + ".\n");

		System.out.print("The scientists are impressed with its speed and pleased with the robot's performance.\n");

	}

}
