import java.util.Scanner;

/* Program: NintendoKnowHow.java
 * Purpose: This program quizzes the user on their knowledge of Nintendo games and history. 
 *          It asks the user ten questions, and each question requests a specific input. (i.e. char, int, etc.)
 *          The program will report whether the input corresponds to the correct answer, 
 *          	and if the answer is wrong, the correct answer will be reported.  
 *         
 * Sources: 1. https://en.wikipedia.org/wiki/List_of_video_games_featuring_Mario
 * 			2. http://zelda.wikia.com/wiki/List_of_items_in_the_Legend_of_Zelda_series
 *  		3. http://nintendo.wikia.com/wiki/List_of_Nintendo_handhelds
 *          4. https://en.wikipedia.org/wiki/Super_Smash_Bros.
 *          5. https://en.wikipedia.org/wiki/Nintendo_DS
 *          6. http://fireemblem.wikia.com/wiki/Fire_Emblem_(series)
 *          7. http://nintendo.wikia.com/wiki/Wii_Vitality_Sensor
 *          8. https://en.wikipedia.org/wiki/Nintendo_Switch
 *          9. https://en.wikipedia.org/wiki/Shigeru_Miyamoto
 *          10.https://en.wikipedia.org/wiki/List_of_best-selling_video_games
 *
 * Author:  Brandon S. Wong
 * Date:    October 18, 2017
 * 
 */


public class NintendoKnowHow {

	public static void main(String[] args) {
		// Declare variables for interpreting user input.
		Scanner userAnswer = new Scanner(System.in);
		String stringUserInput;
		char userCharInput;
		int userIntInput;
		boolean userBoolInput;
        
		// Declare variable for counting correct answers.
		int correctCount;
		correctCount = 0;

		// Introduction
		System.out.println("Welcome to the Nintendo quiz!");
		System.out.println(
				"This quiz will test you on your knowledge of Nintendo game franchises, consoles, and history.\n\n");

		// Question 1, Multiple choice question.
		System.out.println("1. Mario is one of Nintendo's most iconic characters.\n"
				+ "What game did he first appear in? Please enter a letter.");
		System.out.println("a. Mario's Cement Factory \n" 
				+ "b. Mario Bros \n" 
				+ "c. Balloon Fight \n"
				+ "d. Donkey Kong \n" 
				+ "e. The Legend of Zelda \n");
		System.out.print("Your answer: ");

		// Interpret User Input
		stringUserInput = userAnswer.next();
		userCharInput = stringUserInput.charAt(0);

		// Determine correct or wrong
		if (userCharInput == 'd') {
			System.out.println("Correct!");
			correctCount++;
		} else {
			System.out.println("Sorry, wrong answer. The correct answer is d.");
		}
		System.out.println("\n");

		// Question 2, Multiple choice question.
		System.out.println("2. Link from the Legend of Zelda series is known to find and use many items on his quest.\n"
				+ "In the first game, which of the following items was NOT one of links items? Please enter a character a - e.");
		System.out.println("a. Bombs\n" 
				+ "b. Bow and Arrow \n" 
				+ "c. Hookshot \n" 
				+ "d. Boomerang \n" 
				+ "e. Stepladder \n");
		System.out.print("Your answer: ");

		// Interpret User Input
		stringUserInput = userAnswer.next();
		userCharInput = stringUserInput.charAt(0);

		// Determine correct or wrong
		if (userCharInput == 'c') {
			System.out.println("Correct!");
			correctCount++;
		} else {
			System.out.println("Sorry, wrong answer. The correct answer is c.");
		}
		System.out.println("\n");

		// Question 3, Multiple choice question.
		System.out.println("3. Nintendo has been making handhelds handhelds for almost four decades now.\n"
				+ "Which of the following was released earliest. Please enter a character a - e.");
		System.out.println("a. Game & Watch\n" 
				+ "b. Neo-Geo\n" 
				+ "c. Game Boy\n" 
				+ "d. Nintendo Walkman\n" 
				+ "e. Genesis\n");
		System.out.print("Your answer: ");

		// Interpret User Input
		stringUserInput = userAnswer.next();
		userCharInput = stringUserInput.charAt(0);

		// Determine correct or wrong
		if (userCharInput == 'a') {
			System.out.println("Correct!");
			correctCount++;
		} else {
			System.out.println("Sorry, wrong answer. The correct answer is a.");
		}
		System.out.println("\n");

		// Question 4, Numeric response.
		System.out.print("4. In Super Smash Bros., characters from Nintendo's many series fight one another.\n"
				+ "What year was the first title in the series released? \n\nPlease enter an integer: ");

		// Interpret User Input
		userIntInput = userAnswer.nextInt();

		// Determine correct or wrong
		if (userIntInput == 1999) {
			System.out.println("Correct!");
			correctCount++;
		} else {
			System.out.println("Sorry, wrong answer. The correct answer is 1999.");
		}
		System.out.println("\n");

		// Question 5, Numeric response.
		System.out.print("5. Nintendo's best selling console is the Nintendo DS.\n"
				+ "Rounded to the closest value in millions, how many units have been sold world-wide? \n\nPlease enter an integer: ");

		// Interpret User Input
		userIntInput = userAnswer.nextInt();

		// Determine correct or wrong
		if (userIntInput == 154) {
			System.out.println("Correct!");
			correctCount++;
		} else {
			System.out.println("Sorry, wrong answer. The correct answer is 154.");
		}
		System.out.println("\n");

		// Question 6, Numeric response.
		System.out.print("6. Nintendo's Fire Emblem series has released some entries in the U.S., but not all of them.\n"
				+ "How many non-spinoff Fire Emblem titles have been released in the U.S.? \n\nPlease enter an integer: ");

		// Interpret User Input
		userIntInput = userAnswer.nextInt();

		// Determine correct or wrong
		if (userIntInput == 7) {
			System.out.println("Correct!");
			correctCount++;
		} else {
			System.out.println("Sorry, wrong answer. The correct answer is 7.");
		}
		System.out.println("\n");

		// Question 7, True or False question
		System.out.println("7. At E3 2009, Nintendo's Satoru Iwata announced the development of the Wii Vitality Sensor.\n"
				+ "The Wii Vitality Sensor was released to poor reception in Japan, so it did not see a Western release. \n\nPlease enter true or false: ");

		// Interpret User Input
		userBoolInput = userAnswer.nextBoolean();

		// Determine correct or wrong
		if (userBoolInput == false) {
			System.out.println("Correct!");
			correctCount++;
		} else {
			System.out.println("Sorry, wrong answer. The Wii Vitality Sensor was cancelled before release anywhere.");
		}
		System.out.println("\n");
		
		// Question 8, True or False question
		System.out.print("8. Backwards compatibility allows a new console to play previous console titles.\n"
				+ "The Nintendo Switch is backwards compatible with Wii and WiiU games. \n\nPlease enter true or false: ");

		// Interpret User Input
		userBoolInput = userAnswer.nextBoolean();

		// Determine correct or wrong
		if (userBoolInput == false) {
			System.out.println("Correct!");
			correctCount++;
		} else {
			System.out.println("Sorry, wrong answer. The Nintendo Switch is not backwards compatible.");
		}
		System.out.println("\n");
		
		// Question 9, True or False question
		System.out.print("9. Shigeru Miyamoto has been designing and producing games for Nintendo since their Arcade beginnings. \n"
				+ "He is the creator of Donkey Kong, Mario, The Legend of Zelda, F-Zero, Star Fox, and Pikmin. \n\nPlease enter true or false: ");

		// Interpret User Input
		userBoolInput = userAnswer.nextBoolean();

		// Determine correct or wrong
		if (userBoolInput == true) {
			System.out.println("Correct!");
			correctCount++;
		} else {
			System.out.println("Sorry, wrong answer. Miyamoto created a lot of games.");
		}
		System.out.println("\n");

		// Question 10, Multiple choice question.
		System.out.print("10. What is Nintendo's best selling game accross all platforms? \n"
				+ "Please enter a character a - e.\n");
		System.out.println("a. Pokemon Red, Green, and Blue (combined)\n" 
				+ "b. Mario Kart Wii\n" 
				+ "c. The Legend of Zelda\n" 
				+ "d. Super Mario Bros.\n" 
				+ "e. Wii Sports\n");
		System.out.print("Your answer: ");

		// Interpret User Input
		stringUserInput = userAnswer.next();
		userCharInput = stringUserInput.charAt(0);

		// Determine correct or wrong
		if (userCharInput == 'e') {
			System.out.println("Correct!");
			correctCount++;
		} else {
			System.out.println("Sorry, wrong answer. The correct answer is e.");
		}
		System.out.println("\n");

		// Report overall results to the user. 
		System.out.print("You answered " + correctCount + " of 10 questions correctly.\n");
		if (correctCount==10) {
			System.out.print("Great job! You know Nintendo very well.\n");
		} else if (correctCount>7) {
			System.out.print("Well done. You know things about Nintendo, minus a detail or two.\n");
		} else if (correctCount>4) {
			System.out.print("You probably know Nintendo well, but maybe not Nintendo trivia so much.\n");			
		} else {
			System.out.print("You probably didn't grow up on Nintendo. Your Nintendo knowledge could use work.\n");
		}
		
		// Thank the user.
		System.out.print("Thanks for taking the Nintendo quiz!\n");
	}

}
