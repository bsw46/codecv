/* Program: Robot.java
 * Purpose: This program defines the class Robot. 
 *          It asks the user ten questions, and each question requests a specific input. (i.e. char, int, etc.)
 *          The program will report whether the input corresponds to the correct answer, 
 *          	and if the answer is wrong, the correct answer will be reported.  
 *          
 * Notes:   Each method is described in its respective section. 
 *          
 * Author:  Brandon S. Wong
 * Date:    October 18, 2017
 * 
 */
public class Robot {
	// Declare class variables
	int totalDistanceTraveled;
	double currentBatteryLevel;

	// The only constructor for the Robot class.
	Robot() {
		totalDistanceTraveled = 0;
		currentBatteryLevel = 3.0;
	}

	// This method increases the current battery level of the robot.
	void charge(int addedEnergy) {
		currentBatteryLevel = currentBatteryLevel + (double) addedEnergy;
	}

	// This method calculates the current speed of the robot.
	// Higher charge => higher speed.
	double currentSpeed() {
		return Math.pow(currentBatteryLevel, 2) * 2;
	}

	// This method calculated a new total traveled distance and
	// current battery level given some input travel distance.
	void moveForward(int distance) {
		totalDistanceTraveled = totalDistanceTraveled + distance;
		currentBatteryLevel = currentBatteryLevel * (1 - ((double) distance) / ((double) distance + 1));
	}

	// This method estimates the travel time home for the robot.
	// Equals total distance travelled / current speed.
	double estTimeHome() {
		return (double) totalDistanceTraveled / (Math.pow(currentBatteryLevel, 2) * 2);
	}
}
