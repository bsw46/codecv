/* Program: TestPublicationClasses.java
 * Purpose: This program instantiates an array of publications. 
 *          The array is then populated with types of publication objects such as magazines and books. 
 *          Information pertaining to elements of the array are printed out, including information specific to the subclass publication. 
 *          After printing that list, the program sorts the elements of the array by title.   
 *          The sorted list is then printed out.  
 *
 * Author:  Brandon S. Wong
 * Date:    November 14, 2017
 * 
 */

import java.util.Arrays;

public class TestPublicationClasses {

	public static void main(String[] args) {
		// Declares an array of Publication objects.
		Publication[] arrayOfPublications = new Publication[5];
		
		// Fill in the array with magazines, books, and kids magazines. 
		// Note: The example publications from the homework are used. 
        arrayOfPublications[0] = new Magazine(50,4.99,"Times Magazine","Weekly");
        arrayOfPublications[1] = new Magazine(100,9.95,"The Philadelphia Magazine","Monthly");
        arrayOfPublications[2] = new KidsMagazine(36,3.50,"National Geographic Kids","Monthly",7,12);
        arrayOfPublications[3] = new Book(764,99.14,"Java Software Solutions","J. Lewis and W. Loftus");
        arrayOfPublications[4] = new Book(309,9.59,"Harry Potter and the Sorcerer's Stone","J. K. Rowling");
        
        // Publisher information added
        arrayOfPublications[0].SetPublisher("Time Inc.");
        arrayOfPublications[1].SetPublisher("Metrocorp");
        arrayOfPublications[2].SetPublisher("National Geographic Partners");
        arrayOfPublications[3].SetPublisher("Pearson");
        arrayOfPublications[4].SetPublisher("Scholastic");
                
        // Display all publications
        System.out.print("List of publications\n");
        System.out.print("==============================\n");
                
        for (int i = 0 ; i<arrayOfPublications.length;i++) {
        	System.out.print("********************************************************\n");
        	arrayOfPublications[i].print();
        	System.out.print("********************************************************\n\n");
        }
        
        // Sort array of publications by title
        Arrays.sort(arrayOfPublications);
        
        // Print sorted array
        System.out.print("==============================\n");
        System.out.print("List of sorted publications\n");
        System.out.print("==============================\n");
        for (int i = 0 ; i<arrayOfPublications.length;i++) {
        	System.out.print("********************************************************\n");
        	arrayOfPublications[i].print();
        	System.out.print("********************************************************\n\n");
        }
	}
}
