/* Program: KidsMagazine.java
 * Purpose: This java file defines the KidsMagazine class. KidsMagazine is child to the Magazine class. 
 * 			The class has the following variables which pertain to the class: upper and lower age bounds.
 * 			The class has a constructor for the Magazine variables and these new variables as well as accessors and mutators. 
 * 			The class has a print() method which invokes Magazine.print(), and displays the extra kids magazine information.
 * 			
 * Author:  Brandon S. Wong
 * Date:    November 14, 2017
 * 
 */
public class KidsMagazine extends Magazine{
	int ageLowerBound, ageUpperBound;
	
	// Constructors
	// Constructor where all kids magazine information is supplied.
	KidsMagazine(int pageCount, double price, String title, String frequency, int lowerAge, int upperAge) {
		super(pageCount, price, title,frequency);						
		ageLowerBound=lowerAge;
		ageUpperBound=upperAge;
	}
	
	// Constructor where no information is supplied.
	KidsMagazine() {
		super();
		ageLowerBound=ageUpperBound=0;
	}
	
	// Mutators for for age range
	public void SetAgeLowerBound(int lowerAge) {
		ageLowerBound=lowerAge;
	}

	public void SetAgeUpperBound(int upperAge) {
		ageUpperBound=upperAge;
	}
	
	// Accessors for age range
	public int GetAgeLowerBound() {
		return ageLowerBound;
	}

	public int GetAgeUpperBound() {
		return ageUpperBound;
	}
	
	public void print() {
		super.print();
		System.out.print("For Ages:        "+ageLowerBound+" to "+ageUpperBound+"\n");
	}
	
}
