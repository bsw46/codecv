/* Program: Employee.java
 * Purpose: This java file defines the Employee class.  
 * 			The class has four variables ID number, hours worked, hourly wage, and gross pay.
 * 			The class has mutators and accessors for each of these variables.  
 * 			The class has a printEmployee() method which displays all the employee information.
 * 			
 * Author:  Brandon S. Wong
 * Date:    November 14, 2017
 * 
 */
public class Employee {
	String employeeID;
	double hoursWorkedThisWeek;
	double hourlyWage;
	double grossWages;
	
	Employee(){
		employeeID="";
	}
	
	Employee(String newID){
		employeeID=newID;
	}
	
	void setEmployeeID(String newID){
		employeeID=newID;
	}
	void setHoursWorkedThisWeek(double hours){
		hoursWorkedThisWeek = hours;
	}
	void setHourlyWage(double wagePerHour){
		hourlyWage = wagePerHour;
	}
	void setGrossWages(double wagesGrossed){
		grossWages = wagesGrossed;
	}
	
	String getEmployeeID(){
		return employeeID;
	}
	double getHoursWorkedThisWeek(){
		return hoursWorkedThisWeek;
	}
	double getHourlyWage(){
		return hourlyWage;
	}
	double getGrossWages(){
		return grossWages;
	}

	void printEmployee() {
		System.out.format("%-8s%-8.2f$%6.2f  $%8.2f%n",employeeID,hoursWorkedThisWeek,hourlyWage,grossWages);

	}
	
}
