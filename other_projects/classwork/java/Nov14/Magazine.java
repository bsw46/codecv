/* Program: Magazine.java
 * Purpose: This java file defines the Magazine class. Magazine is parent to the KidsMagazine class and child to the Publication class. 
 * 			The class has the following variables which pertain to the class: frequency of publication.
 * 			The class has a constructor for the Publication variables and these new variables as well as accessors and mutators. 
 * 			The class has a print() method which overwrites Publication.print(); it displays magazine information.
 * 			
 * Author:  Brandon S. Wong
 * Date:    November 14, 2017
 * 
 */
public class Magazine extends Publication {
	String frequencyOfPublication;
	
	// Constructor where all magazine information is supplied.
	Magazine(int pageCount, double price, String title, String frequency) { 
		super(pageCount, price, title);
		frequencyOfPublication = frequency;
	}
	
	// Constructor where no publication information is known.
	// String variables are left empty and values are set to zero.
	Magazine() {
		super();
		frequencyOfPublication = "";
	}
	
	// Mutator for Frequency
	public void SetFrequencyOfPublication(String frequency) {
		frequencyOfPublication = frequency;
	}
	
	// Accessor for Frequency
	public String GetFrequencyOfPublication() {
		return frequencyOfPublication;
	}
	
	public void print() {
		
		System.out.print("Magazine title:  "+titleOfPublication+"\n");
		System.out.print("Publisher:       "+nameOfPublisher+"\n");
		System.out.print("Number of pages: "+numberOfPages+"\n");
		System.out.format("Price per issue: %.2f\n",priceOfPublication);
		System.out.print("Frequency:       "+frequencyOfPublication+". ");
		if (frequencyOfPublication.equalsIgnoreCase("Monthly")) {
			System.out.print("12 issues per year.\n");
		}
		if (frequencyOfPublication.equalsIgnoreCase("Weekly")) {
			System.out.print("52 issues per year.\n");
		}

	}
}
