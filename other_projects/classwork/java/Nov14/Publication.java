/* Program: Publication.java
 * Purpose: This java file defines the Publication class. Publication is an abstract class, and parent to the Magazine and Book classes. 
 * 			The class has the following variables which pertain to the class: publisher, page count, price, and title.
 * 			The class has constructors for these variables as well as accessors and mutators. 
 * 			Publication has a print() method defined which is overwritten by the print() methods of child classes.
 * 			Last, Publication has a compareTo method for the sake of sorting Publication objects. 
 *
 * Author:  Brandon S. Wong
 * Date:    November 14, 2017
 * 
 */

public abstract class Publication implements Comparable<Publication> {
	// Declare variables
	String nameOfPublisher;
	int numberOfPages;
	double priceOfPublication;
	String titleOfPublication;

	// Constructor where all publication information is supplied.
	// By instruction, publisher may only be set by mutator.
	Publication(int pageCount, double price, String title) {
		nameOfPublisher = "Not Specified";
		numberOfPages = pageCount;
		priceOfPublication = price;
		titleOfPublication = title;
	}

	// Constructor where no publication information is known.
	// String variables are left empty and values are set to zero.
	Publication() {
		nameOfPublisher = "Not Specified";
		numberOfPages = 0;
		priceOfPublication = 0.0;
		titleOfPublication = "";
	}

	// Below are mutators for modifying the object variables.
	// They overwrite the current object variable with an input one.
	void SetPublisher(String publisherName) {
		nameOfPublisher = publisherName;
	}

	void SetNumberOfPages(int pageCount) {
		numberOfPages = pageCount;
	}

	void SetPriceOfPublication(double price) {
		priceOfPublication = price;
	}

	void SetTitleOfPublication(String title) {
		titleOfPublication = title;
	}

	// Below are accessors for specific variables of the Publication class.
	String GetPublisher() {
		return nameOfPublisher;
	}

	int GetNumberOfPages() {
		return numberOfPages;
	}

	double GetPriceOfPublication() {
		return priceOfPublication;
	}

	String GetTitleOfPublication() {
		return titleOfPublication;
	}

	// Below is the print method for the abstract class.
	public void print() {
		// Will be used for call, but this is specified later.
	}
	
	// This serves as the basis for sorting Publication objects. 
	public int compareTo(Publication other) {
		return this.GetTitleOfPublication().compareTo(other.GetTitleOfPublication());
	}
}
