/* Program: Book.java
 * Purpose: This java file defines the Book class. Book is child to the Publication class. 
 * 			The class has the following variables which pertain to the class: author(s).
 * 			The class has a constructor for the Publication variables and the new variable as well as accessors and mutators. 
 * 			The class has a print() method which overwrites Publication.print(); it displays book information.
 * 			
 * Author:  Brandon S. Wong
 * Date:    November 14, 2017
 * 
 */
public class Book extends Publication {
	String authorOfBook;

	// Constructors
	// Constructor where all book information is supplied.
	Book(int pageCount, double price, String title, String authorNames) {
		super(pageCount, price, title);
		authorOfBook = authorNames;
	}

	// Constructor where no publication information is known.
	Book() {
		super();
		authorOfBook = "";
	}

	public void SetAuthor(String authorName) {
		authorOfBook = authorName;
	}

	public String GetAuthor() {
		return authorOfBook;
	}

	public void print() {

		System.out.print("Book title:      " + titleOfPublication + "\n");
		System.out.print("Author(s):       " + authorOfBook + "\n");
		System.out.print("Publisher:       " + nameOfPublisher + "\n");
		System.out.print("Number of pages: " + numberOfPages + "\n");
		System.out.format("Price:           %.2f\n", priceOfPublication);

	}
}
