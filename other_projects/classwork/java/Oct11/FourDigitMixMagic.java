/* Program: FourDigitMixMagic.java
 * Purpose: This program asks the user to inupt a four digit number. It then rearranges the digits to form two different numbers. 
 *          After that, it takes the difference of the different numbers and prompts the user to enter three digits from the difference.
 *          Once three are entered. The fourth 'secret' digit is reported.
 * Notes:   This program assumes the users input is 'well-behaved'. It assumes the following and possibly more:
 *          - The user never enters a non-integer data type when prompted for input. 
 *          - The user enters a four digit integer followed by three single digit integers.
 *          - The user enters an input which yields a four digit difference. (i.e. not 2222 or 1000)
 *          This program also does not have a means of checking whether the single digits entered by the user are actually those of the difference. 
 *          This program has a ternary operator and arrays which have not yet been covered.  
 * Author:  Brandon S. Wong
 * Date:    October 11, 2017
 * 
 */

// Program uses scanner.
import java.util.Scanner;

public class FourDigitMixMagic {
	public static void main(String[] arg) {
        
    	// Declare variables
    	int i,j,k,input,permute1,permute2,difference,diff_digit_sum,secretdigit;
    	boolean done;
    	double order_index;
    	int[] all_digits = new int[4]; // arrays have not been covered in class yet. 
    	int[] diff_digits = new int[4];
    	
    	// Declare scanners
    	Scanner fourdigitinput = new Scanner(System.in);
    	Scanner singledigitinput = new Scanner(System.in);
        
        // Describe program for user
        System.out.println("Magic Trick Program^TM");
        
        // Prompt user
    	System.out.print("Enter a four-digit number: ");
    	
    	// Prompt the user for input
    	input = fourdigitinput.nextInt();
    	
    	// Take apart input into digits
        all_digits[0]=input/1000;
        input = input - 1000*all_digits[0];
        all_digits[1]=input/100;
        input = input - 100*all_digits[1];
        all_digits[2]=input/10;
        input = input - 10*all_digits[2];
        all_digits[3]=input;
        
        // Permute digits
        permute1 = 1000*all_digits[1] + 100*all_digits[0] + 10*all_digits[3] + 1*all_digits[2];
        permute2 = 1000*all_digits[0] + 100*all_digits[2] + 10*all_digits[1] + 1*all_digits[3];
        
        // Calculate difference
    	difference = permute1-permute2;
    	difference = difference < 0 ? -difference : difference; // Uses ternary operator not taught yet in class. 

    	// Take apart difference into digits, could be used to check digits individually.
        diff_digits[0]=difference/1000;
        difference = difference - 1000*diff_digits[0];
        diff_digits[1]=difference/100;
        difference = difference - 100*diff_digits[1];
        diff_digits[2]=difference/10;
        difference = difference - 10*diff_digits[2];
        diff_digits[3]=difference;
        
        // Sum the digits in the difference, this quantity can distinguish whether the remaining digit is a 0 or 9. 
        diff_digit_sum=diff_digits[0]+diff_digits[1]+diff_digits[2]+diff_digits[3];
        
    	// Tell the user what happened, and instruct user. 
    	System.out.println("The number you entered has been scrambled into two new numbers: " + permute1 + " and " +permute2 +".");
    	System.out.println("Now subtract the smaller permutation from the larger one.");
    	System.out.println("Secretly pick a non-zero digit from the difference.");
    	System.out.println("Enter the other three digits of the difference:");
    	
    	// Store difference of digits.
    	i = singledigitinput.nextInt();
    	j = singledigitinput.nextInt();
    	k = singledigitinput.nextInt();
    	secretdigit = diff_digit_sum-(i+j+k);
    	// secretdigit = 9-(i+j+k)%9; // Calculating the secret digit with an alternate method. Slightly less relaible. 
        
        // Report the secret digit. Dazzle the user!
        System.out.println("The secret digit is "+secretdigit);       
	}
}