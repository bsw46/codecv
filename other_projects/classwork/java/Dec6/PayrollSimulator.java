import java.util.LinkedList;
import java.util.Scanner;
/* Program: PayrollSimulator.java
 * Purpose: This program can display pay information of employees.
 *          The user is prompted to enter multiple employee ID numbers to indicate the employees of interest.  
 *          The user is then prompted to enter the time worked and wage information of the employee.
 *          The program will also calculate the employee's gross wages.
 *          Finally, the program will print out all the pay information of all the employees. 
 *          
 * Author:  Brandon S. Wong
 * Date:    December 6, 2017
 */

public class PayrollSimulator {

	public static void main(String[] args) {
		// Declare constants
		LinkedList<Employee> listOfEmployees = new LinkedList<Employee>(); // Assignment specifies use of Linked List
		boolean stillEnteringEmployees = true;
		boolean stillEnteringHoursWorked = true;
		boolean stillEnteringHourlyWage = true;
		Scanner userInput = new Scanner(System.in);
		String inputID;
		Employee toBeEnteredEmployee;
		double inputHours = 0, inputWages = 0;

		System.out.print("            Drexel Payroll           \n");
		System.out.print("-------------------------------------\n\n");

		// Prompt the user to enter employee IDs.
		while (stillEnteringEmployees) {
			System.out.print("Enter an employee ID or 000 to stop: ");
			inputID = userInput.next();
			if (inputID.equals("000")) {
				stillEnteringEmployees = false;
				continue;
			} else {
				toBeEnteredEmployee = new Employee(inputID);
				listOfEmployees.add(toBeEnteredEmployee);
			}
		}
		System.out.print("\n");

		// Prompt the user for wage and work time information.
		for (int i = 0; i < listOfEmployees.size(); i++) {

			// Prompt the user to enter hours worked.
			stillEnteringHoursWorked = true;
			while (stillEnteringHoursWorked) {
				System.out.print("Enter the number of hours employee " + listOfEmployees.get(i).getEmployeeID()
						+ " worked this week: ");
				inputHours = userInput.nextDouble();
				if (inputHours < 0) {
					System.out.print("Negative values are not allowed. Try again.\n");
				} else if (inputHours > 7 * 24) {
					System.out.print("A week only has 168 hours for a worker to work; no time loops. Try again.\n");
				} else {
					stillEnteringHoursWorked = false;
				}
			}

			// Prompt the user to enter wages.
			stillEnteringHourlyWage = true;
			while (stillEnteringHourlyWage) {
				System.out.print("Enter the number hourly wage of employee " + listOfEmployees.get(i).getEmployeeID()
						+ " in dollars ($6.00 or more): ");
				inputWages = userInput.nextDouble();
				if (inputWages < 6.0) {
					System.out.print("The hourly wage must be at least $6.00. Try again.\n");
				} else {
					stillEnteringHourlyWage = false;
				}
			}
			
			// Set the information on the employee and calculate gross wage.
			toBeEnteredEmployee = listOfEmployees.get(i);
			toBeEnteredEmployee.setHoursWorkedThisWeek(inputHours);
			toBeEnteredEmployee.setHourlyWage(inputWages);
			toBeEnteredEmployee.setGrossWages(inputWages*inputHours);
			listOfEmployees.set(i,toBeEnteredEmployee);
			System.out.print("\n");
		}
		userInput.close();
		System.out.print("\n");

		// Display employee information.
		System.out.print("Wages due this week\n");
		System.out.print("----------------------------------\n");
		System.out.print("ID      Hours   Rate      Wages   \n");
		System.out.print("----------------------------------\n");
		for (int i = 0; i < listOfEmployees.size(); i++) {
			listOfEmployees.get(i).printEmployee();
		}
	}
}
