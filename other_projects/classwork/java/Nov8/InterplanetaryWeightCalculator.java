
/* Program: InterplanetaryWeightCalculator.java
 * 
 * Purpose: This program calculates the user's weight on other planets in the solar system.
 *          The user is first prompted whether they want to:
 *          - find out their weight on a given planet. 
 *          - find out their weight on all other planets.
 *          - find out the specific gravity on different planet surfaces.
 *          - review the menu options again.
 *          - exit the program. 
 *          
 *          If the user selects the first option, user is prompted to input a weight and planet index until valid values are provided.
 *          Then the user's weight on the specified planet is displayed. 
 *          A method is invoked to calculate the weight.
 *          
 *          If the user selects the second option, user is prompted to input a weight until a valid answer is entered. 
 *          Then the user's weight on all the different planet surfaces is displayed as a table. 
 *          A method is invoked to generate this table.
 *          
 *          If the user selects the third option, the different planet surface gravities are displayed as a table. 
 *          A method is invoked to generate this table.
 *           
 *          If the user selects the fourth option, the menu options are immediately displayed again.
 *          
 *          If the user selects the fifth option, the program closes.
 *          
 * Author:  Brandon S. Wong
 * Date:    November 8, 2017
 * 
 */

import java.text.NumberFormat;
import java.util.*;

public class InterplanetaryWeightCalculator {

	public static void main(String[] args) {
		// Declare constants
		String[] PLANET_NAMES = { "Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune",
				"Pluto" };
		// Alternatively PLANET_SURFACE_GRAVITY
		double[] SPECIFIC_GRAVITY = { .39, .91, 1.00, .38, 2.87, 1.32, .93, 1.23, .03 };
		int EARTH_INDEX;
		EARTH_INDEX = 2;

		// Declare variables
		Scanner userInput = new Scanner(System.in);
		int userDecisionInt, userPlanetIndex;
		userDecisionInt = userPlanetIndex = 0;
		double userEarthWeight;
		userEarthWeight = 0;
		boolean exitProgram, exitWeightInputLoop, exitIndexInputLoop;

		// The user will stay in the program until the exit condition is fulfilled.
		exitProgram = false;
		while (!exitProgram) {
			// A brief program introduction and description
			System.out.print("Welcome to the Interplanetary Weight Calculator!\n");
			System.out.print("This program tells you your weight on other planets in our solar system.\n");
			System.out.print("************************************************************************\n");

			// Inform the user of their options.
			System.out.print("Enter 1 to find your weight on a given planet.\n");
			System.out.print("Enter 2 to find on all planets.\n");
			System.out.print("Enter 3 to display all plantes and gravity factors.\n");
			System.out.print("Enter 4 to show this menu again.\n");
			System.out.print("Enter any other integer to exit the program.\n");

			// Prompt and collect user input.
			System.out.print("\nPlease enter an integer: ");
			userDecisionInt = userInput.nextInt();

			// The program branches to five different branches.
			// Branch 1
			if (userDecisionInt == 1) {
				// Prompt user to enter their earth weight.
				exitWeightInputLoop = false;
				while (!exitWeightInputLoop) {
					System.out.print("\nEnter your weight on Earth, in pounds: ");
					userEarthWeight = userInput.nextDouble();

					// Check the user input.
					// The user can't have a negative or zero weight.
					if (userEarthWeight <= 0) {
						System.out.print("Weight must be a value greater than zero.\n");
					} else {
						exitWeightInputLoop = true;
					}
				}
				System.out.print("\n");

				// Prompt user to enter a planet index.
				exitIndexInputLoop = false;
				while (!exitIndexInputLoop) {
					System.out.print("Planet indices are integers which represent different solar system planets.\n");
					System.out.print(
							"These indices are organized by distance from the sun and range from 0 (Mercury) to 8 (Pluto).\n");
					System.out.print("Enter the index of the planet you're interested in: ");
					userPlanetIndex = userInput.nextInt();

					// Check the user input.
					// The user can't have a negative or zero weight.
					if (userPlanetIndex < 0 || userPlanetIndex > 8) {
						System.out.print("The planet indices range between 0 and 8.\n\n");
					} else {
						exitIndexInputLoop = true;
					}
				}
				System.out.print("\n");

				// Calculate the weight of the user on the designated other planet.
				System.out.print("Your weight on " + PLANET_NAMES[userPlanetIndex] + " is "
						+ otherPlanetWeight(userEarthWeight, SPECIFIC_GRAVITY, userPlanetIndex, EARTH_INDEX)
						+ " pounds.\n\n\n");

				// Branch 2
			} else if (userDecisionInt == 2) {
				// Prompt user to enter their earth weight.
				exitWeightInputLoop = false;
				while (!exitWeightInputLoop) {
					System.out.print("\nEnter your weight on Earth, in pounds: ");
					userEarthWeight = userInput.nextDouble();

					// Check the user input.
					// The user can't have a negative or zero weight.
					if (userEarthWeight <= 0) {
						System.out.print("Weight must be a value greater than zero.\n");
					} else {
						exitWeightInputLoop = true;
					}
				}
				System.out.print("\n");

				// Invoke a method for making a table.
				displayUserWeightAllPlanets(userEarthWeight, PLANET_NAMES, SPECIFIC_GRAVITY, EARTH_INDEX);
				System.out.print("\n\n");

				// Branch 3
			} else if (userDecisionInt == 3) {
				System.out.print("\n");
				displaySpecificGravityPlanets(PLANET_NAMES, SPECIFIC_GRAVITY);
				System.out.print("\n\n");

			} else if (userDecisionInt == 4) {
				System.out.print("\n\n");

			} else {
				exitProgram = true;
				System.out.print("\nThanks for trying out the Interplanetary Weight Calculator!");
				System.out.print("\nHave a good day!");
			}
		}
	}

	public static double otherPlanetWeight(double userWeight, double[] specificGravity, int targetIndex,
			int sourceIndex) {
		return userWeight * specificGravity[targetIndex] / specificGravity[sourceIndex];
	}

	public static void displayUserWeightAllPlanets(double userWeight, String[] planetNames, double[] specificGravity,
			int sourceIndex) {
		System.out.print("Planet          Weight\n");
		System.out.print("----------------------\n");
		for (int i = 0; i < planetNames.length; i++) {
			System.out.format("%-16s%-12.2f%n", planetNames[i],
					otherPlanetWeight(userWeight, specificGravity, i, sourceIndex));
		}
	}

	public static void displaySpecificGravityPlanets(String[] planetNames, double[] specificGravity) {
		System.out.print("Planet          Gravity Factor\n");
		System.out.print("------------------------------\n");
		for (int i = 0; i < planetNames.length; i++) {
			System.out.format("%-16s%-12.2f%n", planetNames[i], specificGravity[i]);
		}
	}
}
