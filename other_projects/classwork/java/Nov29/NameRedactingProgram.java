/* Program: NameRedactingProgram.java
 * Purpose: This program redacts classified names from text documents.
 *          The user is prompted to enter a text file which has a name to be redacted.  
 *          The user is then prompted to enter an output file name.
 *          The output file will be either overwritten if it exists, or a new file will be generated.
 *          The program has exception handling abilities. 
 *          
 * Author:  Brandon S. Wong
 * Date:    November 29, 2017
 * 
 */
import java.io.*;
import java.util.*;

public class NameRedactingProgram {

	public static void main(String[] args) throws IOException {
		// Declare variables
		String inputFileName;
		String outputFileName;
		String nameToBeRedacted;
		Scanner userInput = new Scanner(System.in);
		
		// Replacement word, this is expressed in an integer array for convenience later on.
		String REPLACEMENT_NAME = "REDACTED";
		int[] REPLACEMENT_INT = new int[REPLACEMENT_NAME.length()];
		for (int i=0;i<REPLACEMENT_NAME.length();i++){
			REPLACEMENT_INT[i]=(int)REPLACEMENT_NAME.charAt(i);
		}
		
		// Prompt the user for input
		System.out.print("Please enter the name of the data file: ");
		inputFileName = userInput.next();

		System.out.print("Please enter the name of the output file: ");
		outputFileName = userInput.next();

		System.out.print("Please enter the name to be redacted: ");
		nameToBeRedacted = userInput.next();
		int[] nameToBeRedactedInt = new int[nameToBeRedacted.length()];
		for (int j=0;j<nameToBeRedacted.length();j++)
			{
			nameToBeRedactedInt[j]=(int)nameToBeRedacted.charAt(j);
		}
		
		// Initialize input and output streams
		FileReader inputFile = null;
		FileWriter outputFile = null;
		
		// try first, to check if input file name is appropriate. 
		try {
			inputFile = new FileReader(inputFileName);
			outputFile = new FileWriter(outputFileName);

			// Variables for copying and redacting properly. 
			int nextCharAsInt;
			int numberOfStoredInts=0;
			boolean hasStoredInts = false;
			int[] tempStoreReadInts = new int[nameToBeRedacted.length()]; 
			
			// Read until finished with file. 
			while ((nextCharAsInt = inputFile.read()) != -1) {
				
				// The letter is not a part of the redeacted name.
				if (nextCharAsInt!=nameToBeRedactedInt[numberOfStoredInts] && hasStoredInts==false) { 
					outputFile.write(nextCharAsInt);
				
				// The letter c matches the first letter of the redacted name, need to check next letter.
				} else if (nextCharAsInt==nameToBeRedactedInt[numberOfStoredInts] && hasStoredInts==false){
					tempStoreReadInts[numberOfStoredInts] = nextCharAsInt;
					hasStoredInts=true;
					numberOfStoredInts++;
					
				// The letter c matches the ith letter of the redacted name, 
				} else if (nextCharAsInt==nameToBeRedactedInt[numberOfStoredInts] && hasStoredInts==true){
					// If this is the last letter, the name needs to be redacted.
					if (numberOfStoredInts==nameToBeRedacted.length()-1) {
						for (int j=0;j<REPLACEMENT_NAME.length();j++)
							outputFile.write(REPLACEMENT_INT[j]);
						numberOfStoredInts=0;
						hasStoredInts=false;
					// If this is not the last letter, then continue reading.
					} else {
						tempStoreReadInts[numberOfStoredInts] = nextCharAsInt;
						numberOfStoredInts++;
					}
					
				// The next characte does not match the ith letter of the redacted name. Print stored information.
				} else if (nextCharAsInt!=nameToBeRedactedInt[numberOfStoredInts] && hasStoredInts==true){
					for (int j=0;j<numberOfStoredInts;j++)
						outputFile.write(tempStoreReadInts[j]);
					numberOfStoredInts=0;
					hasStoredInts=false;
					outputFile.write(nextCharAsInt);			
				}
			}
			// May have stored characters at the end.
			if (hasStoredInts) {
				for (int j=0;j<numberOfStoredInts;j++)
					outputFile.write(tempStoreReadInts[j]);
			}
		// In case input file name does not find anything.
		} catch(FileNotFoundException fileDNE){
			System.out.print("The file could not be found.");
		} catch (IOException e) {
			System.out.print("There was an IOException.");
		} finally {
			// Close streams to prevent resource leaks.
			if (inputFile != null) {
				inputFile.close();
			}
			if (outputFile != null) {
				outputFile.close();
			}			
		System.out.print("\nAll done! Check the output file.");
		}
	}
}