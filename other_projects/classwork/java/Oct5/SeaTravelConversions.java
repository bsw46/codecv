/* Program: SeaTravelConversions.java
 * Purpose: This program reports a distance in nautical miles, converts it to miles, and calculates travel times given some speeds.  
 *  		It does not prompt the user for input and only calculates values from the numbers in the program. 
 * Author:  Brandon S. Wong
 * Date:    October 5, 2017
 * 
 */

public class SeaTravelConversions {
    
	public static void main(String[] arg) {
        
    	// Declare and assign variables
    	double trip_distance,top_speed,avg_speed;
    	trip_distance = 18; 				//nautical miles
    	top_speed = 5.8;					//nautical miles per hour
    	avg_speed = 4.0;					//nautical miles per hour
    	
    	// Declare and assign constants
    	double MILES_PER_NAUTICAL_MILES;	//miles per nautical miles
    	MILES_PER_NAUTICAL_MILES = 1.1508;
    	
    	// Describe program for user
    	System.out.println("Sea Travel Time Calculator");
    	System.out.println("This program calculates a distance in miles given a number of nautical miles.");
    	System.out.println("It also calculates the time to travel this distance at a top speed and at an average speed.");
    	System.out.println("");
    	
    	// Display the program's outputs
    	System.out.println("The distance to travel in nautical miles is: " + trip_distance);
    	System.out.println("The equivalent distance in miles is: " + trip_distance*MILES_PER_NAUTICAL_MILES);
    	System.out.println("At the top speed " + top_speed + " knots, the travel time is " + trip_distance/top_speed + " hours.");
    	System.out.println("At the average speed " + avg_speed + " knots, the travel time is " + trip_distance/avg_speed + " hours.");
    }
}
