
/* Program: EvenNumbersToZero.java
 * Purpose: This program reports the even numbers between 0 and positive integer n, inclusive.
 *          The user inputs the integer n, and the program checks the input. 
 *          If the input is positive, it reports all the even integers from 0 to the input integer.
 *          If the input is negative, it returns an error and asks the user to enter another input.
 *          If the input is 0, the program shuts down.
 *          Once a valid input is entered, the program returns each even integer between 0 and the input.
 *          It also displays the count of even numbers.
 *          The method for finding all the even numbers and counting the even numbers relies on recursion. 
 *          
 *          
 * Author:  Brandon S. Wong
 * Date:    October 25, 2017
 * 
 */

import java.util.Scanner;

public class EvenNumbersToZero {

	public static void main(String[] args) {
		// Declare variables
		int inputNumber;
		int evenCount;
		boolean enteredZero;
		Scanner userInput = new Scanner(System.in);

		// Describe program for user.
		System.out.print("Hi! I'm a program who reports the positive integers between 0 and a number you give me.\n");
		System.out.print(
				"If you enter a positive integer, I'll report the even integers between it and 0 (inclusive).\n");
		System.out.print("If you enter a negative integer, I'll ask you again for a positive integer.\n");
		System.out.print("If you enter 0, this program will end.\n");

		// Ask the user for input and provide responses.
		enteredZero = false;
		while (!enteredZero) {
			// Instructions
			System.out.print("\nEnter a positive integer to get even numbers or enter 0 to end this program: ");
			inputNumber = userInput.nextInt();

			if (inputNumber > 0) {
				// Positive integer => return even numbers and count.
				System.out.print("The even integer between 0 and " + inputNumber + " are: ");
				evenCount = RecursiveEven(inputNumber);
				System.out.print("\nThere are " + evenCount + " even integers between 0 and " + inputNumber + ".\n");
			} else if (inputNumber == 0) {
				// 0 => Goodbye, end program.
				enteredZero = true;
				System.out.print("Thanks for trying out the program! Goodbye!");
				continue;
			} else {
				// Negative integer => request a new input
				System.out.print("Error: you entered a negative integer. Please try again.\n");
			}
		}
	}

	public static int RecursiveEven(int n) {
		n = n % 2 == 0 ? n : n - 1; // If n is odd, change it to the next lowest even number. We only care for even
									// ones.
		System.out.print(n + " "); // Report n
		if (n != 0) {
			return 1 + RecursiveEven(n - 2); // The recursive step: count one even number, plus the even numbers
												// counting from the next lowest even number.
		} else {
			return 1; // The base case where n is zero. n=0 is just one even number.
		}
	}
}
