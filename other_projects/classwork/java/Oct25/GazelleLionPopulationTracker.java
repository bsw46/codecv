
/* Program: GazelleLionPopulationTracker.java
 * Purpose: This program tracks the population of gazelles and lions over a 1000-day simulation time.
 *          The user chooses the initial population count of both gazelles and lions.
 *          If the user enters non-integer starting population counts, the program breaks.
 *          If the user enters non-positive integers, the user is asked to re-enter population counts until the program receives proper values.
 *          Once valid initial populations are entered, the simulation models the population of lions and gazelles over a thousand days.
 *          Every one hundred days, a population screenshot is taken and the current population counts are displayed.
 *          After 1000 days, the average populations over time and the maximum populations at a given time are presented. 
 *          
 * Author:  Brandon S. Wong
 * Date:    October 25, 2017
 * 
 */

import java.text.NumberFormat;
import java.util.*;

public class GazelleLionPopulationTracker {
	public static void main(String[] args) {

		// Declare variables for checking user input
		int initialGazelleCount = 0;
		int initialLionCount = 0;
		boolean correctInput;
		Scanner userInput = new Scanner(System.in);

		// Declare variables for simulation
		int i = 0;
		double oldGazellePop, oldLionPop, newGazellePop, newLionPop; // pop will be a shorthand for population
		double sumGazelleDaysPop, sumLionDaysPop;
		double averageGazellePop, averageLionPop;
		double maxGazellePop, maxLionPop;

		// Declare constants
		int MAX_DAYS;
		double GAZELLE_GROWTH_RATE, CONSUMPTION_RATE, LION_DEATH_RATE, LION_GROWTH_CONSUMPTION_RATIO;
		MAX_DAYS = 1000;
		GAZELLE_GROWTH_RATE = .01;
		LION_DEATH_RATE = .008;
		CONSUMPTION_RATE = .00005;
		LION_GROWTH_CONSUMPTION_RATIO = .015;

		// Program description
		System.out.print("This program simulates the ecological balance of lion and gazelle populations.\n");
		System.out.print("******************************************************************************\n\n");

		// Get user input for gazelle population and check for correctness.
		correctInput = false;
		while (!correctInput) {
			// Prompt and collect user input.
			System.out.print("Please enter the initial number of gazelles (an integer greater than zero): ");
			initialGazelleCount = userInput.nextInt();

			// Check user input
			if (initialGazelleCount <= 0) {
				System.out.print("You must enter a value greater than zero. Please try again.\n\n");
			} else {
				correctInput = true;
			}
		}
		System.out.print("\n");

		// Get user input for lion population and check for correctness.
		correctInput = false;
		while (!correctInput) {
			// Prompt and collect user input.
			System.out.print("Please enter the initial number of lions (an integer greater than zero): ");
			initialLionCount = userInput.nextInt();

			// Check user input
			if (initialLionCount <= 0) {
				System.out.print("You must enter a value greater than zero. Please try again.\n\n");
			} else {
				correctInput = true;
			}
		}
		System.out.print("\n");

		// Execute simulation
		System.out.print("Gazelle vs. Lion population, every 100 days.\n");
		System.out.print("Days      Gazelles       Lions\n");
		System.out.print("--------------------------------------------\n");

		maxGazellePop = oldGazellePop = (double) initialGazelleCount;
		maxLionPop = oldLionPop = (double) initialLionCount;

		sumGazelleDaysPop = 0;
		sumLionDaysPop = 0;

		// Simulate the lion and gazelle population
		for (i = 1; i <= MAX_DAYS; i++) {
			// Calculate the next generations population
			newGazellePop = (1 + GAZELLE_GROWTH_RATE) * oldGazellePop - CONSUMPTION_RATE * oldLionPop * oldGazellePop;
			newLionPop = (1 - LION_DEATH_RATE) * oldLionPop
					+ CONSUMPTION_RATE * LION_GROWTH_CONSUMPTION_RATIO * oldLionPop * oldGazellePop;

			if ((i % 100) < 1) {
				System.out.format("%-10d%-15.2f%-15.2f%n", i, newGazellePop, newLionPop);
			}
			// Check if the new population value exceeds the previous maximum.
			maxGazellePop = maxGazellePop > newGazellePop ? maxGazellePop : newGazellePop;
			maxLionPop = maxLionPop > newLionPop ? maxLionPop : newLionPop;

			// Update the sum of Gazelle and Lion day-count
			sumGazelleDaysPop += (newGazellePop + oldGazellePop) / 2;
			sumLionDaysPop += (newLionPop + oldLionPop) / 2;
			// In the above two lines, there is a choice to use:
			// a left riemann sum: +=new
			// a right riemann sum: +=old
			// the trapezoidal rule: +=(new+old)/2
			// I chose to use the trapezoidal rule because it tends to have less error.
			// I realize the HW example utilizes a right riemann sum, but it doesn't specify
			// it is required.

			// Update Population values for next iteration.
			oldGazellePop = newGazellePop;
			oldLionPop = newLionPop;
		}
		// Calculate the average population over time interval.
		averageGazellePop = sumGazelleDaysPop / MAX_DAYS;
		averageLionPop = sumLionDaysPop / MAX_DAYS;

		System.out.println("");

		// Display extra results
		System.out.format("Average number of gazelles: %.2f%n", averageGazellePop);
		System.out.format("Average number of lions: %.2f%n", averageLionPop);
		System.out.format("Maximum number of gazelles: %.2f%n", maxGazellePop);
		System.out.format("Maximum number of lions: %.2f%n", maxLionPop);

		// End program
		System.out.print("\nEnd of population simulation program.");

	}

}
