# README #

Welcome to the Interview Questions directory!
This directory is meant to demonstrate my problem solving capabilities.
I will have my own attempts and better solutions which I can find.  

### What is this directory for? ###
I want to have this directory so I can keep track of my problem solving capabilities over time.  

### How do I get set up? ###
If you'd like, download these files and try running them yourself. 

### Who do I talk to? ###
Contact me at 
bsw46@drexel.edu