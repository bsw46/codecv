# File: 	FindMaxSubArray.py
# Contributor:  Brandon Wong
# Date:         February 9, 2018
# Problem:
# Given a vector of arbitrary random numbers, find the subarray with the
# maximum summed value. In the problem, elements of the array are real numbers
# ranging from -1 to 1. 

import random

def maxSubArray(givenArray):
    tempSum=maxSum=givenArray[0]
    tMinI=minIndex=0
    tMaxI=maxIndex=1
    for i in range(1,len(givenArray)):
        if givenArray[i]>=0:
            if tempSum<0:
                tempSum=givenArray[i]
                tMinI=i
                tMaxI=i+1
            else:
                tempSum+=givenArray[i]
                tMaxI=tMaxI+1
        else:
            if  tempSum+givenArray[i]>0:
                tempSum+=givenArray[i]
                tMaxI=i+1
            else:
                tempSum=givenArray[i]
                tMinI=i
                tMaxI=i+1    

        if maxSum<tempSum:
            minIndex=tMinI
            maxIndex=tMaxI
            maxSum=tempSum
    if maxSum>0:
        print("The subArray with the maximum value is randArray("+str(minIndex)+":"+str(maxIndex-1)+")")
        print("The subarray's sum is "+str(maxSum))
    else:
        print("All elements of the given array are negative.")
        print("The maximum subArray is an empty set with a sum of zero.")

randArray=[]
for i in range(40):
    randArray.append(-1+2*random.random())
    print(str(i)+'   '+str(randArray[i]))
maxSubArray(randArray)               
                 
            
