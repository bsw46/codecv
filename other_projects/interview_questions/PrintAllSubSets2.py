# File: 	PrintAllSubSets.py
# Contributor:  Brandon Wong
# Date:         February 3, 2018
#
# Problem:
# Given a set of elements of length S, print out all subsets of S.
# Include the empty set.
#
# Better Approach:
# From the CS Dojo on youtube. 
# The recursive case in PrintAllSubSets2.py is much more efficient. 


def helper(hSet,hSubSet,i):
    # Base case: The helper has gone through every element.  
    if i==len(hSet):
        print(hSubSet)
    else:
        # Recursive case 1: don't add ith element to hSubSet and use helper on i+1:N elements.
        # This line is needed because hSubSet is stored in one memory location and changed like a global variable.
        hSubSet[i]=None   
        helper(hSet,hSubSet,i+1)
        # Recursive case 2: add ith element to hSubSet and use helper on i+1:N elements.
        hSubSet[i]=hSet[i]
        helper(hSet,hSubSet,i+1)

def allSubSet(set):
    subSet=[None]*len(set)
    helper(set,subSet,0)


Array=['a','b','c','d']
allSubSet(Array)
