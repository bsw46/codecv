# File: 	PrintAllSubSets.py
# Contributor:  Brandon Wong
# Date:         February 3, 2018
#
# Problem:
# Given a set of elements of length S, print out all subsets of S.
# Include the empty set.
#
# My Approach:
# For a set of size N, there are 2^N subsets to consider. 
# This is equal to the number of N-bit binary numbers (2^N).
# This approach utilizes a for loop, and
# it is equivalent of counting up on an N-bit binary scale.
#
# Better Approach: 
# The recursive case in PrintAllSubSets2.py is much more efficient. 


def subSet(set,num,max):
    # Base case 1: return an empty string.
    if num==0:
        return ''
    else:
        # Base case 2: don't include any more numbers.
        if num==2**(max-1):
            return set[max-1]
        # Recursive case 1: print number at max index, and recur.
        elif num>2**(max-1):
            num=num-2**(max-1)
            return subSet(set[:max-1],num,max-1)+set[max-1]
        # Recursive case 2: recur
        else:
            return subSet(set,num,max-1)

# Iterator set up
def counter(set):
    N=len(set)
    print(range(2**N))
    for i in range(2**N):
        print(i)
        print(subSet(set,i,N))


Array=['a','b','c','d']
counter(Array)
