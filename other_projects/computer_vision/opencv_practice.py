# https://docs.opencv.org/3.4.1/dc/d2e/tutorial_py_image_display.html

import numpy as np
import cv2 as cv

# Load a color image in grayscale
img = cv.imread('JPEG.jpg',0)
cv.imshow('image',img)
k=cv.waitKey(0)
if k == 27:
    cv.destroyAllWindows()
elif k== ord('s'):
    cv.imwrite('GRAYJPEG.png',img)
    cv.destroyAllWindows()
