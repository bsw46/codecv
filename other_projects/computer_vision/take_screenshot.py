# import the necessary packages
import numpy as np 
import pyautogui
import imutils
import cv2
import time

# take a screenshot of the screen, store it in memory, then
# convert the PIL/Pillow image to an openCV compatible numpy array
# and finally write image to disk.

image = pyautogui.screenshot()
# This is an interesting object type.
# Not sure 
#print(image)

image = cv2.cvtColor(np.array(image),cv2.COLOR_RGB2GRAY)
# can use image.shape to get dimensions.
# RGB2GRAY reduces the dimensionality!


#print('')
#print('')


#print(image)
#cv2.imwrite("in_memory_to_disk.png",image)

def process_img(original_image):
    processed_img = cv2.cvtColor(np.array(image),cv2.COLOR_RGB2GRAY)
    processed_img = cv2.Canny(processed_img, threshold1=200, threshold2=300)
    return processed_img

last_time = time.time()
while(True):
    print(time.time()-last_time)
    last_time = time.time()
    image = pyautogui.screenshot()    # This step takes about .18 seconds
    new_image = process_img(image)
    cv2.imshow('window',new_image[60:780,60:1340])
    cv2.waitKey(20)

