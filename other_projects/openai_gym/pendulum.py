import gym
import numpy as np
env = gym.make('Pendulum-v0')
env.reset()

def right():
    env.render()
    env.step(np.array([.5]))

def left():
    env.render()
    env.step(np.array([-.5]))
    
next_action = env.action_space.sample()        


for _ in range(400):
    next_action = env.action_space.sample()          
    env.render()
    temp=env.step(next_action)
    print(temp)
    
for _ in range(400):
    env.render()
    temp=env.step(next_action)
    next_action = np.array([0])
