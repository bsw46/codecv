import gym
import numpy as np
env = gym.make('MountainCarContinuous-v0')
env.reset()

def right():
    env.render()
    env.step(np.array([.5]))

def left():
    env.render()
    env.step(np.array([-.5]))
    
next_action = env.action_space.sample()        


for _ in range(400):
    env.render()
    temp=env.step(next_action)
    if temp[0][1] > 0:
        next_action = np.array([.1])
    else:
        next_action = np.array([-.1])

for _ in range(400):
    env.render()
    temp=env.step(next_action)
    next_action = np.array([0])
