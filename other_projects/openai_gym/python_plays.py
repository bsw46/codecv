# import the necessary packages
import numpy as np 
import pyautogui
import imutils
import cv2
import time

# take a screenshot of the screen, store it in memory, then
# convert the PIL/Pillow image to an openCV compatible numpy array
# and finally write image to disk.

def process_img(original_image):

    processed_img = cv2.Canny(original_image,threshold1=200,threshold2=10)
    return processed_img



last_time = time.time()
while(True):
    print(time.time()-last_time)
    last_time = time.time()
    screen_array = np.array(pyautogui.screenshot())    # This step takes about .18 seconds
    gray_array = cv2.cvtColor(screen_array,cv2.COLOR_RGB2GRAY)
    game_window = process_img(gray_array[0:100,0:200])
    cv2.imshow('window',game_window)
    cv2.waitKey(20)
