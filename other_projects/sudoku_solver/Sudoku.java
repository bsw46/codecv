/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudokusolver;

/**
 *
 * @author bsw46
 */
public class Sudoku {
    int sum,i,j,k,i33,j33,vi,vj,ie,je,iAL,jAL;
    int[] possible = new int[9];
    int[][] Grid = new int[9][9];
    int[][] Temp = new int[9][9];
    int[][] Last = new int[9][9];
    int[][] Valid = new int[9][9];
    boolean Present33=false;
    
    Sudoku(int[][] input_grid){
        Grid=input_grid;
        Temp=Grid;
        Last=Temp;
    }
    
    void Display(int k){
        if (k==1){
            System.out.println("This is the grid in the Sudoku object.");
            for (i=0;i<9;i++){
                for (j=0;j<9;j++){
                    System.out.print(" "+Grid[i][j]);
                }
            System.out.println();
            }   
        } else if (k==2) {
            System.out.println("This is the grid in the Sudoku object.");
            for (i=0;i<9;i++){
                for (j=0;j<9;j++){
                    System.out.print(" "+Temp[i][j]);
                }
                System.out.println();
            }   
        } else if (k==3) {
            System.out.println("This is the grid in the Sudoku object.");
            for (i=0;i<9;i++){
                for (j=0;j<9;j++){
                    System.out.print(" "+Valid[i][j]);
                }
                System.out.println();
            }   
        }

    }    
    
    boolean NotSolved(){
        for (i=0;i<9;i++){
            for (j=0;j<9;j++){
                if (Temp[i][j]==0){
                    return true;
                }
            }
        }   
        return false;
    }

    void Position1(int a){
        //Reset Valid
        for (vi=0;vi<9;vi++){
            for (vj=0;vj<9;vj++){
                Valid[vi][vj]=0;
            }
        }

        for (i=0;i<9;i++){
            for (j=0;j<9;j++){
                if (Temp[i][j]==a){
                    for (vi=0;vi<9;vi++){
                        Valid[vi][j] = -1;
                        // Two of the same number can't be in the same column.
                    }   
                    for (vj=0;vj<9;vj++){
                        Valid[i][vj] = -1;
                        // Two of the same number can't be in the same row.

                    }
                } 
                if (Temp[i][j]>0)
                    Valid[i][j]=-1;
                    // Number can't overwrite an occupied position.
            }
        }   
        // Two of the same number can't be in the same 3x3 box. 
        for (i33=0;i33<3;i33++){
            for (j33=0;j33<3;j33++){
                Present33 = false;
                for (i=0;i<3;i++){
                    for (j=0;j<3;j++){
                        if (Temp[3*i33+i][3*j33+j]==a) {
                            Present33=true;
                        }
                    }
                }
                if (Present33){
                    for (i=0;i<3;i++){
                        for (j=0;j<3;j++){
                            Valid[3*i33+i][3*j33+j]=-1;
                        }                      
                    }
                }
            }
        }
        // Advanced logic, 
        for (i33=0;i33<3;i33=i33+1){
            for (j33=0;j33<3;j33=j33+1){
                sum=0;
                iAL=jAL=-1;
                for (i=0;i<3;i++){
                    for (j=0;j<3;j++){
                        sum=sum+Valid[3*i33+i][3*j33+j];
                    }
                }
                if (sum==-7){
                    for (i=0;i<3;i++){
                        for (j=0;j<3;j++){
                            if (iAL!=-1&&Valid[3*i33+i][3*j33+j]==0){
                                if (3*i33+i==iAL){
                                    for (k=0;k<9;k++){
                                        if (k/3!=j33)
                                            Valid[iAL][k]=-1;
                                    }
                                }
                                if (3*j33+j==jAL){
                                    for (k=0;k<9;k++){
                                        if (k/3!=i33)
                                            Valid[k][jAL]=-1;
                                    }
                                }
                            }
                            if (iAL==-1&&Valid[3*i33+i][3*j33+j]==0){
                                iAL=i33*3+i;
                                jAL=j33*3+j;
                            }
                        }
                    }
                }
            }
        }
            
        //Perform Assignment, if any
        for (i33=0;i33<3;i33=i33+1){
            for (j33=0;j33<3;j33=j33+1){
                sum=0;
                for (i=0;i<3;i++){
                    for (j=0;j<3;j++){
                        sum=sum+Valid[3*i33+i][3*j33+j];
                    }
                }
                if (sum==-8){
                    for (i=0;i<3;i++){
                        for (j=0;j<3;j++){
                            if (Valid[3*i33+i][3*j33+j]==0){
                                Temp[3*i33+i][3*j33+j]=a;
                            }                      
                        }
                    }
                }
            }
        }
    }
    
    void Elimination(){
        // Look for empty box.
        for (i=0;i<9;i++){
            for (j=0;j<9;j++){
                // Discard possibilities
                if (Temp[i][j]==0){
                    for (k=0;k<9;k++){
                        possible[k]=1;
                    }
                    for (ie=0;ie<9;ie++){
                        if (Temp[ie][j]!=0) {
                            possible[Temp[ie][j]-1]=0;
                        }
                    }
                    for (je=0;je<9;je++){
                        if (Temp[i][je]!=0){
                            possible[Temp[i][je]-1]=0;
                        }
                    }
                    // Look at numbers in box
                    for (i33=0;i33<3;i33++){
                        for (j33=0;j33<3;j33++){
                            if (Temp[(i/3)*3+i33][(j/3)*3+j33]!=0){
                               possible[Temp[(i/3)*3+i33][(j/3)*3+j33]-1]=0;
                            }
                        }
                    }
                    
                    sum=0;
                    for (k=0;k<9;k++){
                        sum=sum+possible[k];
                    }
                    if (sum==1) {
                        for (k=0;k<9;k++){
                            if (possible[k]==1){
                                Temp[i][j]=k+1;
                            }
                        }
                    }
                }
            }
        }
    }
}

   