/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sudokusolver;

/**
 *
 * @author bsw46
 */
public class SudokuSolver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int MaxIterations = 70;
        int count=0;
        int pos; 
        // input stage of program
        int[][] Medium = {
            {9,0,8,0,0,6,0,0,5},
            {0,0,0,3,0,0,7,0,0},
            {0,0,3,0,0,0,6,8,0},
            {0,0,0,0,9,0,0,7,2},
            {8,0,0,6,0,3,0,0,4},
            {2,9,0,0,7,0,0,0,0},
            {0,6,9,0,0,0,5,0,0},
            {0,0,0,0,0,1,0,0,0},
            {1,0,0,4,0,0,9,0,7}
        };
        Sudoku Puzzle1 = new Sudoku(Medium);
        Puzzle1.Display(1);
        
        
        // solution stage of program
        while (Puzzle1.NotSolved()&&count<MaxIterations){
            System.out.println("The puzzle is not yet solved.");
            count++;
            //Logic pertaining to position
            for (pos=1;pos<10;pos++){
                Puzzle1.Position1(pos);
//                Puzzle1.Display(2);
            }
            //Logic pertaining to elimination
            Puzzle1.Elimination();
                        
            
        }
        for (pos=1;pos<10;pos++){
            Puzzle1.Position1(pos);
            Puzzle1.Display(3);
        }
        Puzzle1.Display(2);
    }
    
}
