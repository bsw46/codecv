#Main Repo #

My name is Brandon, and I'm aiming to be the data scientist you'll want at your company.

This repository presents some of my previous works. Most of them are recent projects I've done to train for the role and others are from previous roles to demonstrate my skills. Here is a list:

[**advanced\_process\_controls**](https://bitbucket.org/bsw46/codecv/src/master/advanced_process_controls/) - contains my implementation of a control structure for stabilizing a simulated distributed process system. (MATLAB)

[**chemical\_plant**](https://bitbucket.org/bsw46/codecv/src/master/chemical_plant/) - contains the spreadsheets, the aspen file, and the report for a chemical plant design. (Aspenplus, Excel, Matlab)

[**investment\_study**](https://bitbucket.org/bsw46/codecv/src/master/investment_study/) - contains simulators for determining the future value of IRAs. Utilizes historical market data and compares investing strategies. (Python, Quandl, numpy, other packages)

[**kaggle**](https://bitbucket.org/bsw46/codecv/src/master/kaggle/titanic/) - practiced working with datasets from kaggle. Mainly for knowledge and practice, partially out of curiosity.

[**learn**](https://bitbucket.org/bsw46/codecv/src/master/learn/) - work completed to teach myself computer science, programming, data science and machine learning. (Python and libraries, Octave)  

[**measles\_study**](https://bitbucket.org/bsw46/codecv/src/master/measles_study/) - Estimated the basic reproduction number with Monte Carlo methods in vaccinated and unvaccinated population models. (C/Bash scripting)

[**mnist**](https://bitbucket.org/bsw46/codecv/src/master/mnist/) - trained, validated and tested neural network models for classifying handwritten digits and the clothing datasets. (Python, Keras)

[**paper\_cv**](https://bitbucket.org/bsw46/codecv/src/master/paper_cv/brandon_wong_resume.pdf) - view my resume. (Libre office)


### What is this repository for? ###
For potential employers, I hope this repository presents the range of my abilities. 

### Who do I talk to? ###
Contact me at: 
brandonshermanwong@gmail.com

These are my other profiles: 
[**LinkedIn**](https://www.linkedin.com/in/brandon-wong-45766793/) 
and 
[**glassdoor**](https://www.glassdoor.com/member/profile/index.htm) 



