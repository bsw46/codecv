function [ dCA1dt ] = dCA1dtfindSS( X)%,CA0 )
%% Data inputs %%
% Flow rates (m^3/hr)
F0=4.998 ; F1=39.996 ; F3=30.0; Fr=34.998;
% Tank volumes (m^3)
V1=1.0 ; V2=3.0;
% Reaction enthalpies of three reactions (kJ/kmol)
delH=[-5*10^4 ; -5.2*10^4 ; -5.4*10^4];
% Pre-exponential rate constant (1/hr)
k0=[3*10^6 ; 3*10^5 ; 3*10^5];
% Activation energy of the reactions (kJ/kmol)
E=[5.0*10^4 ; 7.53*10^4 ; 7.53*10^4];
% Density of fluid (kg/m^3)
Rho=1000.0; 
% Heat cpapcity kJ/kgK
cp=.231;
% Initial steady state reactor temperatures (K)
T1S=457.9 ; T2S=415.5;
% Initial steady state reactor concentrations 
CAS1=1.77 ; CAS2=1.75;

% Inlet concentration (kmol/m^3)
CA0=4;

dCA1dt=F0/V1*(CA0-X(2))+Fr/V1*(X(4)-X(2));


for k=1:3
    dCA1dt=dCA1dt-R(k,X(1))*X(2);
end

end

