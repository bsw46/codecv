clear;clc;
%% Data inputs %%
% Flow rates (m^3/hr)
F0=4.998 ; F1=39.996 ; F3=30.0; Fr=34.998;
% Tank volumes (m^3)
V1=1.0 ; V2=3.0;
% Gas constant (kJ/(kmol*K))
R_gas=8.314;
% Inlet flow temperatures (K)
T0=300.0 ; T03=300.0;
% Initial steady state inlet concentrations (kmol/m^3)
CAS0=4.0 ; CAS03=2.0;
% Reaction enthalpies of three reactions (kJ/kmol)
delH=[-5*10^4 ; -5.2*10^4 ; -5.4*10^4];
% Pre-exponential rate constant (1/hr)
k0=[3*10^6 ; 3*10^5 ; 3*10^5];
% Activation energy of the reactions (kJ/kmol)
E=[5.0*10^4 ; 7.53*10^4 ; 7.53*10^4];
% Density of fluid (kg/m^3)
Rho=1000.0;
% Heat cpapcity kJ/kgK
cp=.231;
% Initial steady state reactor temperatures (K)
T1S=457.9 ; T2S=415.5;
% Initial steady state reactor concentrations (kmol/m^3)
CA1S=1.77 ; CA2S=1.75;


%% Find steady state near that found in the paper
XS_paper=[T1S CA1S T2S CA2S];

dDynamicsdt=@(X)[dT1dtfindSS(X) dCA1dtfindSS(X)...
    dT2dtfindSS(X) dCA2dtfindSS(X)];
SS=fsolve(dDynamicsdt,XS_paper)

%% Verify instability

InitialDisturb=SS+SS.*(-.01+.02*rand(1,4));
Traj=zeros(3000,4);
Traj(1,:)=InitialDisturb;
for i=1:2999
    Traj(i+1,:)=Traj(i,:)+dDynamicsdt(Traj(i,:))*1/3600;
end
Timeforstabplot=linspace(0,2999,3000);
figure(1)
plot(Timeforstabplot,Traj(:,1),Timeforstabplot,Traj(:,3))
xlabel('Time, seconds')
ylabel('Temperature, Kelvin')

figure(2)
plot(Timeforstabplot,Traj(:,2),Timeforstabplot,Traj(:,4))
xlabel('Time, seconds')
ylabel('Concentration, kmol/m^3')

%% State space variable transformation
dX1dt=@(X,CA0,u)[(dT1dt(X,u)-SS(1))/SS(1) (dCA1dt(X,CA0)-SS(2))/SS(2)];
dX2dt=@(X,CA03,u)[(dT2dt(X,u)-SS(3))/SS(3) (dCA2dt(X,CA03)-SS(4))/SS(4)];

%% Matrix A1, [ ddT1dtdT1 ddT1dtdCA1 ; ddCA1dtdT1 ddCA1dtdCA1 ]
% Reactor one dynamics from self.
ddT1dtdT1=-(F0+Fr)/V1;
ddCA1dtdT1=0;
ddCA1dtdCA1=(-F0-Fr)/V1;

for i=1:3
    ddT1dtdT1=ddT1dtdT1+SS(2)*G(i,SS(1))*-E(i)/R_gas*-1/SS(1)^2;
    ddCA1dtdT1=ddCA1dtdT1-R(i,SS(1))*-E(i)/R_gas*-1/SS(1)^2*SS(2)*SS(1)/SS(2);
    ddCA1dtdCA1=ddCA1dtdCA1-R(i,SS(1));
end
ddT1dtdCA1=(G(1,SS(1))+G(2,SS(1))+G(3,SS(1)))/SS(1)*SS(2);
ddCA1dtdT1;
ddCA1dtdCA1
A1=[ ddT1dtdT1 ddT1dtdCA1 ; ddCA1dtdT1 ddCA1dtdCA1 ]

%% Matrix A2, [ ddT2dtdT2 ddT2dtdCA2 ; ddCA2dtdT2 ddCA2dtdCA2 ]
% Reactor two dynamics from self.
ddT2dtdT2=-(F1+F3)/V2;
ddCA2dtdT2=0;
ddCA2dtdCA2=(-F1-F3)/V2;

for i=1:3
    ddT2dtdT2=ddT2dtdT2+SS(4)*G(i,SS(3))*-E(i)/R_gas*-1/SS(3)^2;
    ddCA2dtdT2=ddCA2dtdT2-R(i,SS(3))*-E(i)/R_gas*-1/SS(3)^2*SS(4)*SS(3)/SS(4);
    ddCA2dtdCA2=ddCA2dtdCA2-R(i,SS(3));
end
ddT2dtdCA2=(G(1,SS(3))+G(2,SS(3))+G(3,SS(3)))/SS(3)*SS(4);
ddCA2dtdT2;
ddCA2dtdCA2
A2=[ ddT2dtdT2 ddT2dtdCA2 ; ddCA2dtdT2 ddCA2dtdCA2 ]

%% Matrix A12, [ ddT1dtdT2 ddT1dtdCA2 ; ddCA1dtdT2 ddCA1dtdCA2 ]
% Reactor one dynamics from reactor two state.
ddT1dtdT2=Fr/V1*SS(3)/SS(1);
ddCA1dtdCA2=Fr/V1*(SS(4)/SS(2));
A12=diag([ddT1dtdT2 ddCA1dtdCA2 ])

%% Matrix A21, [ ddT2dtdT1 ddT2dtdCA1 ; ddCA2dtdT1 ddCA2dtdCA1 ]
% Reactor one dynamics from reactor two state.
ddT2dtdT1=F1/V2*SS(1)/SS(3);
ddCA2dtdCA1=F1/V2*(SS(2)/SS(4));
A21=diag([ddT2dtdT1 ddCA2dtdCA1 ])

%% Matrix B1, [ ddT1dtdQ1 ddT1dtdCA0 ; ddCA1dtdQ1 ddCA1dtdCA0 ]
ddT1dtdQ1=1/(Rho*cp*V1)/SS(1);
ddCA1dtdCA0=F0/V1/SS(2);
B1=diag([ddT1dtdQ1 ddCA1dtdCA0])
%% Matrix B2, [ ddT2dtdQ2 ddT2dtdCA03 ; ddCA2dtdQ1 ddCA2dtdCA03 ]
ddT2dtdQ2=1/(Rho*cp*V2)/SS(3);
ddCA2dtdCA03=F3/V2/SS(4);
B2=diag([ddT2dtdQ2 ddCA2dtdCA03])



%% Constructing estimate matrices
del1=-.01;

%% Matrix A1hat, [ ddT1dtdT1 ddT1dtdCA1 ; ddCA1dtdT1 ddCA1dtdCA1 ]
% Reactor one estimate of A1 for controller.
A1hat=A1;
A1hat(1,1)=A1hat(1,1)+del1*SS(2)*G(1,SS(1))*E(1)/R_gas/SS(1)^2;
A1hat(1,2)=A1hat(1,2)+del1*G(1,SS(1))/SS(1)*SS(2);

%% Matrix A2hat, [ ddT2dtdT2 ddT2dtdCA2 ; ddCA2dtdT2 ddCA2dtdCA2 ]
% Reactor two estimate of A2 for controller.
A2hat=A2;
A2hat(1,1)=A2hat(1,1)+del1*SS(4)*G(1,SS(3))*E(1)/R_gas/SS(3)^2;
A2hat(1,2)=A2hat(1,2)+del1*G(1,SS(3))/SS(3)*SS(4);

%% Matrix A12 hat, [ ddT1dtdT2 ddT1dtdCA2 ; ddCA1dtdT2 ddCA1dtdCA2 ]
% Reactor one estiamte of dynamics from reactor two state.
A12hat=A12;

%% Matrix A21 hat, [ ddT2dtdT1 ddT2dtdCA1 ; ddCA2dtdT1 ddCA2dtdCA1 ]
% Reactor two estiamte of dynamics from reactor one state.
A21hat=A21;

%% Matrix B1hat, [ ddT1dtdQ1 ddT1dtdCA0 ; ddCA1dtdQ1 ddCA1dtdCA0 ]
B1hat=B1;

%% Matrix B2, [ ddT2dtdQ2 ddT2dtdCA03 ; ddCA2dtdQ1 ddCA2dtdCA03 ]
B2hat=B2;

%% Matrix K1: Choose K1 such that individual unit is stable.
% Should refer to estimates put into controller. A-hat vs A.
K1_k11=-1/B1(1,1)*A1(1,1)-5/B1(1,1);
K1_k12=-A1(1,2)/B1(1,1);
K1_k21=-A1(2,1)/B1(2,2);
K1_k22=-1/B1(2,2)*A1(2,2)-1/B1(2,2);
K1=[K1_k11 K1_k12; K1_k21 K1_k22];

%% Matrix K1: Choose K1 such that individual unit is stable.
% Should refer to estimates put into controller.
K2_k11=-1/B2(1,1)*A2(1,1)-5/B2(1,1);
K2_k12=-A2(1,2)/B2(1,1);
K2_k21=-A2(2,1)/B2(2,2);
K2_k22=-1/B2(2,2)*A2(2,2)-1/B2(2,2);
K2=[K2_k11 K2_k12; K2_k21 K2_k22];

%% Matrix K12, K12 elements : A12+B1*K12=0 => K12=-inv(B1)*A12
% Should refer to A12 hat and B1 hat
K12=-inv(B1)*A12;

%% Matrix K21, K21 elements : A21+B2*K21=0 => K21=-inv(B2)*A21
% Should refer to A21 hat and B2 hat
K21=-inv(B2)*A21;

%% Simulation
MaxTimeh=10; %hrs
N=250000;    % Steps
SimTime=linspace(0,MaxTimeh,N);
dt=SimTime(2)-SimTime(1);
UpdatePeriodh=.22; %hrs
InitialState=[-0.1;0.1; 450/SS(3)-1; 1.5/SS(4)-1];
StateTraj=zeros(4,N);
EstTraj=zeros(4,N);
StateTraj(1:4,1)=InitialState;
EstTraj(1:4,1)=InitialState;
ControlTraj=zeros(4,N);
Updates=1;
for i=1:N-1
    ControlTraj(1:2,i)=K1*StateTraj(1:2,i)+...
        K12*EstTraj(3:4,i);
    ControlTraj(3:4,i)=K2*StateTraj(3:4,i)+...
        K21*EstTraj(1:2,i);
    
    %State Variables
    StateTraj(1:2,i+1)=StateTraj(1:2,i) ...
        +A1*StateTraj(1:2,i)*dt ...
        +A12*StateTraj(3:4,i)*dt ...
        +B1*ControlTraj(1:2,i)*dt;
    StateTraj(3:4,i+1)=StateTraj(3:4,i) ...
        +A2*StateTraj(3:4,i)*dt ...
        +A21*StateTraj(1:2,i)*dt ...
        +B2*ControlTraj(3:4,i)*dt;
    if i/UpdatePeriodh*dt>=Updates
        StateTraj(1:2,i+1)=StateTraj(1:2,i) ...
            +(A1+B1*K1)*StateTraj(1:2,i)*dt ...
            +(A12+B1*K12)*StateTraj(3:4,i)*dt;
        StateTraj(3:4,i+1)=StateTraj(3:4,i) ...
            +(A2+B2*K2)*StateTraj(3:4,i)*dt ...
            +(A21+B2*K21)*StateTraj(1:2,i)*dt;
        EstTraj(1:4,i)=StateTraj(1:4,i);
        Updates=Updates+1;
    end
    %Estimates of reactor 1 in reactor 2
    EstTraj(1:2,i+1)= EstTraj(1:2,i) ...
        +(A1hat+B1hat*K1)*EstTraj(1:2,i)*dt ...
        +(A12hat+B1hat*K12)*StateTraj(3:4,i)*dt;
    %Estimates of reactor 2 in reactor 1
    EstTraj(3:4,i+1)= EstTraj(3:4,i) ...
        +(A2hat+B2hat*K2)*EstTraj(3:4,i)*dt ...
        +(A21hat+B2hat*K21)*StateTraj(1:2,i)*dt;

end
%% Control actions over time
u1=K1*StateTraj(1:2,:)+K12*EstTraj(3:4,:);
u2=K2*StateTraj(3:4,:)+K21*EstTraj(1:2,:);

%% State plots Reactor 2
figure (3)
plot(SimTime,StateTraj(3,:),'b')
xlabel('Time, hours')
ylabel('Dimensionless Temperature for Reactor 2')

figure (4)
plot(SimTime,StateTraj(4,:),'b')
xlabel('Time, hours')
ylabel('Dimensionless Concentration for Reactor 2')

%% Control action plots for Reactor 2
figure (5)
plot(SimTime,u2(1,:),'b')
xlabel('Time, hours')
ylabel('Heat Flow for Reactor 2 (kJ/hr)')

figure (6)
plot(SimTime,u2(2,:)+CAS03,'b')
xlabel('Time, hours')
ylabel('Inlet Concentration for Reactor 2 (kmol/m^3)')
