function [ dCA2dt ] = dCA2dtfindSS( X)%,CA03 )
%% Data inputs %%
% Flow rates (m^3/hr)
F0=4.998 ; F1=39.996 ; F3=30.0; Fr=34.998;
% Tank volumes (m^3)
V1=1.0 ; V2=3.0;
% Reaction enthalpies of three reactions (kJ/kmol)
delH=[-5*10^4 ; -5.2*10^4 ; -5.4*10^4];
% Pre-exponential rate constant (1/hr)
k0=[3*10^6 ; 3*10^5 ; 3*10^5];
% Activation energy of the reactions (kJ/kmol)
E=[5.0*10^4 ; 7.53*10^4 ; 7.53*10^4];
% Density of fluid (kg/m^3)
Rho=1000.0; 
% Heat cpapcity kJ/kgK
cp=.231;
% Initial steady state reactor temperatures (K)
T1S=457.9 ; T2S=415.5;
% Initial steady state reactor concentrations (kmol/m^3)
CAS1=1.77 ; CAS2=1.75;

% Inlet concentration (kmol/m^3)
CA03=2;

dCA2dt=F1/V2*(X(2)-X(4))+F3/V2*(CA03-X(4));
for k=1:3
    dCA2dt=dCA2dt-R(k,X(3))*X(4);
end

end