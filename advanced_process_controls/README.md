#Advanced Process Controls Repo#
This repo contains the source code for an advanced process controls project. This project is based on a 2008 paper titled _Quasi-decentralized model-based networked control of process systems_ by Yulei Sun and Nael H. El-Farra. I reimplemented the simulation and advanced control structure they published while I was an undergraduate at UC Davis. Later, I wrote a report on it for a course in graduate school. 
  
[Read the report.](https://bitbucket.org/bsw46/codecv/src/master/advanced_process_controls/Project.pdf)

The main file here is General.m. 

[Back to the main repo.](https://bitbucket.org/bsw46/codecv)
