function [ R_out ] = R( i,Tj )
%% Data inputs %%
% Gas constant (kJ/(kmol*K))
R_gas=8.314;
% Reaction enthalpies of three reactions (kJ/kmol)
delH=[-5*10^4 ; -5.2*10^4 ; -5.4*10^4];
% Pre-exponential rate constant (1/hr)
k0=[3*10^6 ; 3*10^5 ; 3*10^5];
% Activation energy of the reactions (kJ/kmol)
E=[5.0*10^4 ; 7.53*10^4 ; 7.53*10^4];

R_out=k0(i)*exp(-E(i)/(R_gas*Tj));

end

