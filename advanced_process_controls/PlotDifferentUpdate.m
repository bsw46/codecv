clear;clc
close all
load('SimTime.mat')

load('Update200h.mat')

figure (1)
hold on
plot(SimTime,StateTraj(3,:),'Color',[0 0 1],'LineWidth',1.5)
figure (2)
hold on
plot(SimTime,StateTraj(4,:),'Color',[0 0 1],'LineWidth',1.5)
figure (3)
hold on
plot(SimTime,u2(1,:),'Color',[0 0 1],'LineWidth',1.5)
figure (4)
hold on
plot(SimTime,u2(2,:)+2,'Color',[0 0 1],'LineWidth',1.5)

load('Update216h.mat')

figure (1)
plot(SimTime,StateTraj(3,:),'Color',[.5 0 .5],'LineWidth',1.5)
figure (2)
plot(SimTime,StateTraj(4,:),'Color',[.5 0 .5],'LineWidth',1.5)
figure (3)
plot(SimTime,u2(1,:),'Color',[.5 0 .5],'LineWidth',1.5)
figure (4)
plot(SimTime,u2(2,:)+2,'Color',[.5 0 .5],'LineWidth',1.5)

load('Update220h.mat')

figure (1)
plot(SimTime,StateTraj(3,:),'Color',[1 0 0],'LineWidth',1.5)
figure (2)
plot(SimTime,StateTraj(4,:),'Color',[1 0 0],'LineWidth',1.5)
figure (3)
plot(SimTime,u2(1,:),'Color',[1 0 0],'LineWidth',1.5)
figure (4)
plot(SimTime,u2(2,:)+2,'Color',[1 0 0],'LineWidth',1.5)

figure (1)
ylabel('Normalized Temperature rxr 2','FontSize',16,'FontWeight','bold')
xlabel('Time, hours','FontSize',16,'FontWeight','bold')
legend('Update=.2h','Update=.216h','Update=.22h')
figure (2)
ylabel('Normalized Concentration rxr 2','FontSize',16,'FontWeight','bold')
xlabel('Time, hours','FontSize',16,'FontWeight','bold')
legend('Update=.2h','Update=.216h','Update=.22h')

figure (3)
ylabel('Q_2, kJ/hour','FontSize',16,'FontWeight','bold')
xlabel('Time, hours','FontSize',16,'FontWeight','bold')
legend('Update=.2h','Update=.216h','Update=.22h')

figure (4)
ylabel('CA_0_3, kmol/m^3','FontSize',16,'FontWeight','bold')
xlabel('Time, hours','FontSize',16,'FontWeight','bold')
legend('Update=.2h','Update=.216h','Update=.22h','Location','southeast')


