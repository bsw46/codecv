function [ dT2dt_out ] = dT2dtfindSS( X ) %,u )
%% Data inputs %%
% Flow rates (m^3/hr)
F0=4.998 ; F1=39.996 ; F3=30.0; Fr=34.998;
% Tank volumes (m^3)
V1=1.0 ; V2=3.0;
% Inlet flow temperatures (K)
T0=300.0 ; T03=300.0;
% Density of fluid (kg/m^3)
Rho=1000.0;
% Heat cpapcity kJ/kgK
cp=.231;

dT2dt_out=F1/V2*(X(1)-X(3))+F3/V2*(T03-X(3)); %+u(2)/(Rho*cp*V2);
for k=1:3
    dT2dt_out=dT2dt_out+G(k,X(3))*X(4);
end



end

