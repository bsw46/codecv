import numpy as np

honors = np.array([4,4,4,3,3,3,2,2,2,1,1,1]) # the missing honors

def binary_array(scalar_input, base):
    if np.isscalar(scalar_input):
        if scalar_input < 0:
            return False
        if scalar_input > 2**(base):
            return False
        temp_input = scalar_input
        temp_base = base-1 
        vector = []
        for binary_i in range(base):
            if temp_input>=2**(temp_base-binary_i):
                vector = vector + [temp_input//2**(temp_base-binary_i)]
                temp_input = temp_input - 2**(temp_base-binary_i)
            else:
                vector = vector + [0]
        return np.array(vector)
    return False

all_ace_hands = 0
non_ace_hands = 0
ace_count = 0
ace_array = binary_array(2**11+2**10+2**9,12)
no_ace_array = 1 - ace_array
for i in range(2**12):
    hand_array = binary_array(i,12)
    handHCP = np.dot(honors,hand_array)
    if handHCP<15:
        continue
    if handHCP>15:
        continue
    ace_count = np.dot(ace_array,hand_array)
    if ace_count == 3:
        all_ace_hands = all_ace_hands + 1
    if ace_count == 0:
        non_ace_hands = non_ace_hands + 1

print(non_ace_hands)    
print(all_ace_hands)
