# README.md #
Workflow:
#1. Objective
We have data on passengers who did and did not survive on the titanic. 
Given a passenger not from the data set, we want to predict whether that passenger survived the titanic crash. 
Modern boats are probably safer than the titanic, but hopefully we can determine what features are strong indicators of survivability. 

#2. Importing Data
The data we are working with was imported from kaggle.com. 
We have 891 training examples and 418 testing examples.
There are 10 features including survival status. 
Some of the data points are missing. 

#3. Exploring Data and Cleansing
The features used to describe the examples are as follows: 
Survived, Passenger class, Name, Sex, Age, # of siblings/spouse onboard, # of parents/children onboard, ticket number, fare, cabin, point of embark.

Whether the passenger survived or not is the output variable we want to predict. 

Based on my knowledge and preliminary explorations of the data, I think:

- Sex, Age, # of siblings/spouse onboard, and # of parents/children onboard will each be influential variables. There may have been a bias towards saving women and children, and passengers who had more loved ones to look out for seem like they'd be more likely to try and save them. 

- Passenger Class or Fare would likely determine where on the boat the example would reside in. In the high class part of the boat, the ship should be less crowded promoting survival. Not sure whether to use Passenger Class or Fare or both as the feature. 

- Point of embark might matter a bit. The people who were on the ship longer might have been more familiar with the ship layout. The ship picked up passengers from Southhampton a week before the others. Passengers from Southhampton had 11 days to get familiar with the titanic whereas the passengers from Cherbourg and Queendtown had about 3 days. 

- Cabin seems to be a decent indicator of whether a person survived. Perhaps if the passengers Cabin can be reported at all is a positive indicator.

- Name and ticket number seem like weak indicators. Names are unique. Last names could be extracted to get relationships, but that may already be covered by parch and sibsp. Ticket numbers are largely unique. However, I don't have any insight on what the numbers mean in terms of vicinity, relationships, positions, etc. These characteristics seem to be better understood through the other features. 

Used features: Survived, Sex, Age, sibsp, parch, Pclass, Fare, point of embark
Unused features: Name, tkt number, cabin

These features will be translated into a feature vector by the follwoing transormations:
    
x_Survived = Survived

x_Sex = 0,1 if female/male

x_Age = (Age - mean\_age)/SD\_age

x_sibsp = (Sibsp - mean\_sibsp)/SD\_sibsp

x_parch = (parch - mean\_parch)/SD\_parch

x_pclass = (pclass - mean\_pclass)/SD\_pclass

x_Fare = (parch - mean\_Fare)/SD\_Fare

x_embarkS = 0,1

x_embarkC = 0,1

x_embarkQ = 0,1

x_CabinKnown = 0,1

Edit: Whether the Cabin of a passenger is known or unknown has an effect on survival.
      May investigate the Cabin as an additional feature in version 2. 

Data seems sufficiently cleaned and explored.

#4. Baseline modeling

As a baseline, we have predicting no one survived. This gives an accuracy of 61.6%.
Our next attempt will be a linear classifier, the linear classifier achieves an accuracy of 82% on training data.
This is a significant jump in performance, but I think we can hit improved performance with a support vector machine.

#5. Secondary Modeling
The support vector machine reaches an accuracy of 83% on training data. 
A bit of research online shows better performance could be reached using kNN, decision trees, and random forests. Their accuracies are 84.7, 86.7 and 86.7 respectively.

