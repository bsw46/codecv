import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import linear_model, svm

# 2 importing data
df = pd.read_csv('train.csv',index_col=0,encoding='latin-1')

# 3 Exploring and cleaning data
print(df.columns)
print(df.describe())
#print(df.Cabin.describe())
#print(df.Cabin.value_counts())
#print((df.Cabin.isna()).value_counts())

#print(df.Embarked.describe())
#print(df.Embarked.value_counts())
#print((df.Embarked.isna()).value_counts())
# Index(['Survived', 'Pclass', 'Name', 'Sex', 'Age','SibSp', 'Parch',
#        'Ticket','Fare', 'Cabin', 'Embarked'],dtype='object')

#tkts = np.array(train_data_pd.Ticket)
#tkts.sort()
#print(tkts)

#print(df.Age.describe())
print(df.Pclass.value_counts())
#print((df.Age.isna()).value_counts())
df['unknownCabin'] = df['Cabin'].isna()
print("Is the passenger's cabin unknown?")
print(df['unknownCabin'].value_counts())
print(df.groupby(['unknownCabin']).sum())
print("A much higher proportion of passengers survived with known cabins.")

#train_data_pd = train_data_pd[['Survived','Pclass','Sex','Age','SibSp','Parch']]

x = pd.DataFrame(df['Sex'])
x = x.replace({'male':1,'female':0})
x['Age'] = (df['Age']-df['Age'].mean())/df['Age'].std()
x['Age'] = x['Age'].replace({np.NaN:0})
x['SibSp'] = (df['SibSp']-df['SibSp'].mean())/df['SibSp'].std()
x['Parch'] = (df['Parch']-df['Parch'].mean())/df['Parch'].std()
x['Fare'] = (df['Fare']-df['Fare'].mean())/df['Fare'].std()
x['Class1'] = df['Pclass'].replace({1:1,2:0,3:0}) 
x['Class2'] = df['Pclass'].replace({1:0,2:1,3:0})
x['Class3'] = df['Pclass'].replace({1:0,2:0,3:1})
x['EmbarkS'] = df['Embarked'].replace({'S':1,'Q':0,'C':0,np.NaN:0}) 
x['EmbarkQ'] = df['Embarked'].replace({'S':0,'Q':1,'C':0,np.NaN:0})
x['EmbarkC'] = df['Embarked'].replace({'S':0,'Q':0,'C':1,np.NaN:0})
x['CabinUnknown'] = df['Cabin'].isna().replace({True:1}).replace({False:0})

# Find the missing data points pd.isna(x).replace({True:1,False:0}).mean()

print(x.isna().describe())


y = pd.Series(df.Survived)

np_x = np.array(x)
np_y = np.array(y).reshape((len(y),1))

# 4 baseline modeling

# Predict no survivors
print((df['Survived']==0).sum())

# Predict with a linear classifier
reg = linear_model.LinearRegression()
reg.fit(np_x,np_y)
#print(reg.coef_)
theta = np.array(reg.coef_).reshape((np_x.shape[1],1))
#print(theta)
np_y_lin = np.matmul(np_x,theta)

max_accuracy = np.sum(0==np_y)/len(y)
best_threshold = -10
iterations = 1000
accuracies = np.zeros((iterations,1))
false_positives = np.zeros((iterations,1))
false_negatives = np.zeros((iterations,1))
thresholds = np.linspace(np_y_lin.min(),np_y_lin.max(),num=iterations).reshape((iterations,1))
for i in range(iterations):
    np_y_lin_class = (np_y_lin>thresholds[i]).astype('int')
    accuracies[i] = np.sum(np_y_lin_class==np_y)/len(y)
    false_positives[i] = np.sum(np_y_lin_class>np_y)/len(y)
    false_negatives[i] = np.sum(np_y_lin_class<np_y)/len(y)
    if accuracies[i] > max_accuracy:
        max_accuracy = accuracies[i]
        best_threshold = thresholds[i]
print("The best accuracy for the linear classifier on the training data is: "+str(max_accuracy))
print("The best threshold for the linear classifier on the training data is: "+str(best_threshold))

plt.plot(thresholds,accuracies,'b-')
plt.plot(thresholds,false_positives,'r-')
plt.plot(thresholds,false_negatives,'k-')
plt.ylim((0,.9))

plt.xlabel("Threshold")
plt.ylabel("Accuracy (Blue), False Positive (Red), False Negative (Black)")
#plt.show()

# 5 Secondary modeling
gaussian_svc = svm.SVC(kernel = 'rbf')
gaussian_svc.fit(np_x,np_y.ravel())
np_y_svm = gaussian_svc.predict(np_x)
print("Support Vector Machine with a Gaussian Kernel")
print("The accuracy is "+str(sum(np_y.ravel()==np_y_svm)/len(y)))
print("The false positive rate is " +str(sum(np_y.ravel()<np_y_svm)/len(y)))
print("The false negative rate is " +str(sum(np_y.ravel()>np_y_svm)/len(y)))
