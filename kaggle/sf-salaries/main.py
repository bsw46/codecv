import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import linear_model, svm

# 2 inporting data
df = pd.read_csv('Salaries.csv',index_col=0,encoding='latin-1')

# 3 Exploring and cleaning data
print(df.columns)
print(df.describe())
#print(df.Cabin.describe())
#print(df.Cabin.value_counts())
#print((df.Cabin.isna()).value_counts())

#print(df.Embarked.describe())
#print(df.Embarked.value_counts())
#print((df.Embarked.isna()).value_counts())
# Index(['Survived', 'Pclass', 'Name', 'Sex', 'Age','SibSp', 'Parch',
#        'Ticket','Fare', 'Cabin', 'Embarked'],dtype='object')

#tkts = np.array(train_data_pd.Ticket)
#tkts.sort()
#print(tkts)

