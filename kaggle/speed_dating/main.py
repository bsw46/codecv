

import os
import re
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats

def y_line(x_data,y_data):
    bm = np.polyfit(x_data,y_data,1)
    print( bm)
    return bm[1]+ bm[0]*x_data
def rescale_dataframe(data_frame,columns,start,total_output_size):
    current = 0
    output_dict={}
    for column in columns:
        if np.isnan(data_frame.at[start,column]):
            output_dict[column] = 0
        else:
            output_dict[column] = data_frame.at[start,column]
            current += data_frame.at[start,column]
    if current!=total_output_size:
        if current==0:
            for column in columns:
                output_dict[column] = np.nan
        else:
            output_dict[column] = output_dict[column]/current
    return output_dict


os.chdir('/home/brandon/codecv/kaggle/speed_dating/speed-dating-experiment')
print(os.getcwd())
df = pd.read_csv('Speed Dating Data.csv')

#print(df.describe())

## all of the features
with pd.option_context('display.max_seq_items', None):
    print (df.columns)
#print(df.career.to_string(index=False))

# Which variables seem relevant to the individual?
#
# age, field, field_cd, 

#df2 = 
#df_ind =

summ_df= pd.DataFrame()
append_df = summ_df
init_index = 1
likes=[0]*553
potential_likes = [0]*553
getonce=['gender','age','field_cd','race','imprace','imprelig','income','goal','date','go_out','career_c','date_3']
lookfor=['attr1_1','sinc1_1','intel1_1','fun1_1','amb1_1','shar1_1']
for i in range(1,553):
    if i==118:
        continue
    tempdf = df.loc[df['iid']==i]
    init_index = tempdf.index[0]
    final_index = tempdf.index[-1]
    n=final_index-init_index+1
    record_dict = {}
    for feature in getonce:
         record_dict[feature] = [tempdf.at[init_index,feature]]
    record_dict['match_per'] = [np.mean(tempdf.match)]
    record_dict['dec'] = [np.mean(tempdf.dec)] # how many people this person liked.
    record_dict['like'] = [np.mean(tempdf.like)]
    record_dict['prob'] = [np.mean(tempdf.prob)]
    record_dict['match_es'] = [tempdf.at[init_index,'match_es']/n]
    record_dict['you_call'] = [tempdf.at[init_index,'you_call']/n]
    record_dict['them_cal'] = [tempdf.at[init_index,'them_cal']/n]
    record_dict['date_3'] = [tempdf.at[init_index,'date_3']]
    record_dict['liked_by_ratio'] = [np.mean(tempdf.dec_o)]
    record_dict.update(rescale_dataframe(tempdf,lookfor,init_index,100))
    append_df = pd.DataFrame(data=record_dict)
    summ_df = summ_df.append(append_df,ignore_index=True)

##with pd.option_context('display.max_seq_items', None):
##    with pd.option_context('display.max_columns', 2000):
##        print(summ_df.describe())

# Investigate gender versus like ratio
plt.subplot(3,1,1)
plt.hist(summ_df.liked_by_ratio,bins=10,range=(0,1.1))
plt.ylabel('overall')

plt.subplot(3,1,2)
plt.hist(summ_df.liked_by_ratio[summ_df.gender==1],bins=10,range=(0,1.1))
plt.ylabel('males')

plt.subplot(3,1,3)
plt.hist(summ_df.liked_by_ratio[summ_df.gender==0],bins=10,range=(0,1.1))
plt.ylabel('females')
plt.xlabel('Percentage of partners who decided yes')

print('gender as a factor')
print('')
print('overall')
print(summ_df.liked_by_ratio.describe())
print('')
print('male')
print(summ_df[summ_df.gender==1].liked_by_ratio.describe())
print('')
print('female')
print(summ_df[summ_df.gender==0].liked_by_ratio.describe())
print(stats.ttest_ind(summ_df.liked_by_ratio[summ_df.gender==0],summ_df.liked_by_ratio[summ_df.gender==1]))
print('girls get a significantly larger percentage of yes decisions than boys')

plt.show()


# Investigate race versus like ratio
plt.subplot(3,2,1)
plt.hist(summ_df.liked_by_ratio[summ_df.race==1],bins=10,range=(0,1.1))
plt.ylabel('black')

plt.subplot(3,2,2)
plt.hist(summ_df.liked_by_ratio[summ_df.race==2],bins=10,range=(0,1.1))
plt.ylabel('european')

plt.subplot(3,2,3)
plt.hist(summ_df.liked_by_ratio[summ_df.race==3],bins=10,range=(0,1.1))
plt.ylabel('latino')

plt.subplot(3,2,4)
plt.hist(summ_df.liked_by_ratio[summ_df.race==4],bins=10,range=(0,1.1))
plt.ylabel('asian')

plt.subplot(3,2,5)
plt.hist(summ_df.liked_by_ratio,bins=10,range=(0,1.1))
plt.ylabel('overall')

plt.subplot(3,2,6)
plt.hist(summ_df.liked_by_ratio[summ_df.race==6],bins=10,range=(0,1.1))
plt.ylabel('other')

##plt.subplot(3,2,7)
##plt.hist(np.array(summ_df.liked_by_ratio),bins=10,range=(0,1.1))
##plt.ylabel('samerace')
##
##plt.subplot(3,2,8)
##plt.hist(np.array(summ_df.liked_by_ratio[summ_df.race==6]),bins=10,range=(0,1.1))
##plt.ylabel('other')

plt.xlabel('Percentage of partners who decided yes')

print('')
print('Race as a factor')
print('overall')
print(summ_df.liked_by_ratio.describe())
print('')
print('black')
print(summ_df[summ_df.race==1].liked_by_ratio.describe())
print('')
print('white')
print(summ_df[summ_df.race==2].liked_by_ratio.describe())
print('')
print('latino')
print(summ_df[summ_df.race==3].liked_by_ratio.describe())
print('')
print('asian')
print(summ_df[summ_df.race==4].liked_by_ratio.describe())
print('')
print('other')
print(summ_df[summ_df.race==6].liked_by_ratio.describe())
print('')

print('same race match percent:')
print('overall')
print(np.average(df.match[df.samerace==1]))
print('black')
print(np.average(df.match[df.samerace==1][df.race==1]))
print('white')
print(np.average(df.match[df.samerace==1][df.race==2]))
print('latino')
print(np.average(df.match[df.samerace==1][df.race==3]))
print('asian')
print(np.average(df.match[df.samerace==1][df.race==4]))
print('other')
print(np.average(df.match[df.samerace==1][df.race==6]))

print('')
print('different race match percent:')
print('overall')
print(np.average(df.match[df.samerace==0]))
print('black')
print(np.average(df.match[df.samerace==0][df.race==1]))
print('white')
print(np.average(df.match[df.samerace==0][df.race==2]))
print('latino')
print(np.average(df.match[df.samerace==0][df.race==3]))
print('asian')
print(np.average(df.match[df.samerace==0][df.race==4]))
print('other')
print(np.average(df.match[df.samerace==0][df.race==6]))

print('whites and asians are the most represented in the speed dating pool.')
print('There were 304 whites and 136 asians.')

plt.show()


# Investigate income versus like ratio

plt.plot(summ_df.income,summ_df.liked_by_ratio,'.')
plt.ylabel('Percentage of partners who decided yes')
plt.xlabel('Participant income (USD)')
plt.plot(summ_df.income[summ_df.income>-1],y_line(summ_df.income[summ_df.income>-1],summ_df.liked_by_ratio[summ_df.income>-1]))
print('')
print('People who report their incomes')
print(summ_df.liked_by_ratio[summ_df.income>-1].describe())
print('')
print('People who do not report their incomes')
print(summ_df.liked_by_ratio[summ_df.income.isnull()].describe())
print('')
print(stats.ttest_ind(summ_df.liked_by_ratio[summ_df.income>-1],summ_df.liked_by_ratio[summ_df.income.isnull()]))
print('There is a statistically significant imrpovement in like ratio between people who do and do not report their income.')
print('Income is not self-reported. \nIncome is median household income from reported zip code of participant')
print('It is unclear what causes this difference; unreported daters might: \n- have no income \n- not be confident \n- not be trusting \n- seem not trustworthy...')
plt.show()


# Investigate age versus like ratio
plt.subplot(3,1,1)
plt.plot(summ_df.age,summ_df.liked_by_ratio,'.')
plt.ylabel('overall')
plt.xlim((17,56))

plt.subplot(3,1,2)
plt.plot(summ_df.age[summ_df.gender==1],summ_df.liked_by_ratio[summ_df.gender==1],'.')
plt.ylabel('males')
plt.xlim((17,56))

plt.subplot(3,1,3)
plt.plot(summ_df.age[summ_df.gender==0],summ_df.liked_by_ratio[summ_df.gender==0],'.')
plt.ylabel('females')
plt.xlim((17,56))
plt.xlabel('age')



plt.show()




# Investigate goal versus like ratio
plt.subplot(3,3,1)
plt.hist(summ_df.liked_by_ratio[summ_df.goal==1],bins=10,range=(0,1.1))
plt.ylabel('for fun')

plt.subplot(3,3,2)
plt.hist(summ_df.liked_by_ratio[summ_df.goal==2],bins=10,range=(0,1.1))
plt.ylabel('new people')

plt.subplot(3,3,3)
plt.hist(summ_df.liked_by_ratio[summ_df.goal==3],bins=10,range=(0,1.1))
plt.ylabel('find date')

plt.subplot(3,3,4)
plt.hist(summ_df.liked_by_ratio[summ_df.goal==4],bins=10,range=(0,1.1))
plt.ylabel('srs relationship')

plt.subplot(3,3,5)
plt.hist(summ_df.liked_by_ratio[summ_df.goal==5],bins=10,range=(0,1.1))
plt.ylabel('to say i did')

plt.subplot(3,3,6)
plt.hist(summ_df.liked_by_ratio[summ_df.goal==6],bins=10,range=(0,1.1))
plt.ylabel('other')

plt.subplot(3,3,8)
plt.hist(summ_df.liked_by_ratio,bins=10,range=(0,1.1))
plt.ylabel('overall')

plt.xlabel('Percentage of partners who decided yes')

print('')
print('goal as a factor')
print('for fun')
print(summ_df[summ_df.goal==1].liked_by_ratio.describe())
print('')
print('new people')
print(summ_df[summ_df.goal==2].liked_by_ratio.describe())
print('')
print('find date')
print(summ_df[summ_df.goal==3].liked_by_ratio.describe())
print('')
print('srs relationship')
print(summ_df[summ_df.goal==4].liked_by_ratio.describe())
print('')
print('to say i did')
print(summ_df[summ_df.goal==5].liked_by_ratio.describe())
print('')
print('other')
print(summ_df[summ_df.goal==6].liked_by_ratio.describe())
print('')
print('overall')
print(summ_df.liked_by_ratio.describe())
print('')
print('The people who are looking for fun are more liked than people looking for new people.')
plt.show()


# Investigate date frequency versus like ratio
plt.subplot(3,3,1)
plt.hist(summ_df.liked_by_ratio[summ_df.date==1],bins=10,range=(0,1.1))
plt.ylabel('several dates a week')

plt.subplot(3,3,2)
plt.hist(summ_df.liked_by_ratio[summ_df.date==2],bins=10,range=(0,1.1))
plt.ylabel('two dates a week')

plt.subplot(3,3,3)
plt.hist(summ_df.liked_by_ratio[summ_df.date==3],bins=10,range=(0,1.1))
plt.ylabel('one date a week')

plt.subplot(3,3,4)
plt.hist(summ_df.liked_by_ratio[summ_df.date==4],bins=10,range=(0,1.1))
plt.ylabel('two dates a month')

plt.subplot(3,3,5)
plt.hist(summ_df.liked_by_ratio[summ_df.date==5],bins=10,range=(0,1.1))
plt.ylabel('one date a month')

plt.subplot(3,3,6)
plt.hist(summ_df.liked_by_ratio[summ_df.date==6],bins=10,range=(0,1.1))
plt.ylabel('several dates a year')

plt.subplot(3,3,7)
plt.hist(summ_df.liked_by_ratio[summ_df.date==7],bins=10,range=(0,1.1))
plt.ylabel('almost never')

plt.subplot(3,3,8)
plt.hist(summ_df.liked_by_ratio,bins=10,range=(0,1.1))
plt.ylabel('overall')

plt.xlabel('Percentage of partners who decided yes')

print('')
print('date frequency as a factor')
print('several dates a week')
print(summ_df[summ_df.date==1].liked_by_ratio.describe())
print('')
print('two dates a week')
print(summ_df[summ_df.date==2].liked_by_ratio.describe())
print('')
print('one date a week')
print(summ_df[summ_df.date==3].liked_by_ratio.describe())
print('')
print('two dates a month')
print(summ_df[summ_df.date==4].liked_by_ratio.describe())
print('')
print('one date a month')
print(summ_df[summ_df.date==5].liked_by_ratio.describe())
print('')
print('several dates a year')
print(summ_df[summ_df.date==6].liked_by_ratio.describe())
print('')
print('almost never')
print(summ_df[summ_df.date==7].liked_by_ratio.describe())
print('')
print('overall')
print(summ_df.liked_by_ratio.describe())
print('')
print('People who go on dates more often were more likely to be decided yes on.')
print('This is amongst speed daters.')
print('Not a lot of representation among people who do not go out often.')
plt.show()


# Investigate go out frequency versus like ratio
plt.subplot(3,3,1)
plt.hist(summ_df.liked_by_ratio[summ_df.go_out==1],bins=10,range=(0,1.1))
plt.ylabel('several go out a week')

plt.subplot(3,3,2)
plt.hist(summ_df.liked_by_ratio[summ_df.go_out==2],bins=10,range=(0,1.1))
plt.ylabel('two go_outs a week')

plt.subplot(3,3,3)
plt.hist(summ_df.liked_by_ratio[summ_df.go_out==3],bins=10,range=(0,1.1))
plt.ylabel('one go_out a week')

plt.subplot(3,3,4)
plt.hist(summ_df.liked_by_ratio[summ_df.go_out==4],bins=10,range=(0,1.1))
plt.ylabel('two go_outs a month')

plt.subplot(3,3,5)
plt.hist(summ_df.liked_by_ratio[summ_df.go_out==5],bins=10,range=(0,1.1))
plt.ylabel('one go_out a month')

plt.subplot(3,3,6)
plt.hist(summ_df.liked_by_ratio[summ_df.go_out==6],bins=10,range=(0,1.1))
plt.ylabel('several go_outs a year')

plt.subplot(3,3,7)
plt.hist(summ_df.liked_by_ratio[summ_df.go_out==7],bins=10,range=(0,1.1))
plt.ylabel('almost never')

plt.subplot(3,3,8)
plt.hist(summ_df.liked_by_ratio,bins=10,range=(0,1.1))
plt.ylabel('overall')

plt.xlabel('Percentage of partners who decided yes')

print('')
print('go_out frequency as a factor')
print('several go_outs a week')
print(summ_df[summ_df.go_out==1].liked_by_ratio.describe())
print('')
print('two go_outs a week')
print(summ_df[summ_df.go_out==2].liked_by_ratio.describe())
print('')
print('one go_out a week')
print(summ_df[summ_df.go_out==3].liked_by_ratio.describe())
print('')
print('two go_outs a month')
print(summ_df[summ_df.go_out==4].liked_by_ratio.describe())
print('')
print('one go_out a month')
print(summ_df[summ_df.go_out==5].liked_by_ratio.describe())
print('')
print('several go_outs a year')
print(summ_df[summ_df.go_out==6].liked_by_ratio.describe())
print('')
print('almost never')
print(summ_df[summ_df.go_out==7].liked_by_ratio.describe())
print('')
print('overall')
print(summ_df.liked_by_ratio.describe())
print('')
print('People who go on go_outs more often were more likely to be decided yes on.')
print('This is amongst speed daters.')
print('Not a lot of representation among people who do not go out often.')

plt.show()




# Investigate yeses given versus yes ratio

plt.subplot(3,1,1)
plt.plot(summ_df.dec,summ_df.liked_by_ratio,'.')
plt.ylabel('partners who decided yes')
plt.xlabel('partners said yes to')
plt.plot(summ_df.dec,y_line(summ_df.dec,summ_df.liked_by_ratio))

plt.subplot(3,1,2)
plt.plot(summ_df[summ_df.gender==1].dec,summ_df[summ_df.gender==1].liked_by_ratio,'.')
plt.ylabel('girls who decided yes')
plt.xlabel('As a male, girls said yes to')
plt.plot(summ_df[summ_df.gender==1].dec,y_line(summ_df[summ_df.gender==1].dec,summ_df[summ_df.gender==1].liked_by_ratio))

plt.subplot(3,1,3)
plt.plot(summ_df[summ_df.gender==0].dec,summ_df[summ_df.gender==0].liked_by_ratio,'.')
plt.ylabel('guys who decided yes')
plt.xlabel('As a female, guys said yes to')
plt.plot(summ_df[summ_df.gender==0].dec,y_line(summ_df[summ_df.gender==0].dec,summ_df[summ_df.gender==0].liked_by_ratio))


plt.show()

# Investigate like versus decision
plt.subplot(3,1,1)
plt.hist(df.like,bins=10,range=(0,11))
plt.ylabel('overall')

plt.subplot(3,1,2)
plt.hist(df.like[df.dec==1],bins=10,range=(0,11))
plt.ylabel('decide yes')

plt.subplot(3,1,3)
plt.hist(df.like[df.dec==0],bins=10,range=(0,11))
plt.ylabel('decide no')
plt.xlabel('On a one to ten scale, how much do you like a partner')

print('')
print('On a one to ten scale, how much do you like a partner')
print('overall')
print(df.like.describe())
print('')
print('person said yes to')
print(df[df.dec==1].like.describe())
print('')
print('person said no to')
print(df[df.dec==0].like.describe())
plt.show()


# Investigate attr versus decision
plt.subplot(3,1,1)
plt.hist(df.attr,bins=10,range=(0,11))
plt.ylabel('overall')

plt.subplot(3,1,2)
plt.hist(df.attr[df.dec==1],bins=10,range=(0,11))
plt.ylabel('decide yes')

plt.subplot(3,1,3)
plt.hist(df.attr[df.dec==0],bins=10,range=(0,11))
plt.ylabel('decide no')
plt.xlabel('On a one to ten scale, how attractive is partner')

print('')
print('On a one to ten scale, how do you rate attr of partner')
print('overall')
print(df.attr.describe())
print('')
print('person said yes to')
print(df[df.dec==1].attr.describe())
print('')
print('person said no to')
print(df[df.dec==0].attr.describe())
plt.show()


# Investigate sinc versus decision
plt.subplot(3,1,1)
plt.hist(df.sinc,bins=10,range=(0,11))
plt.ylabel('overall')

plt.subplot(3,1,2)
plt.hist(df.sinc[df.dec==1],bins=10,range=(0,11))
plt.ylabel('decide yes')

plt.subplot(3,1,3)
plt.hist(df.sinc[df.dec==0],bins=10,range=(0,11))
plt.ylabel('decide no')
plt.xlabel('On a one to ten scale, how sincere is partner')

print('')
print('On a one to ten scale, how do you rate sinc of partner')
print('overall')
print(df.sinc.describe())
print('')
print('person said yes to')
print(df[df.dec==1].sinc.describe())
print('')
print('person said no to')
print(df[df.dec==0].sinc.describe())
plt.show()


# Investigate intel versus decision
plt.subplot(3,1,1)
plt.hist(df.intel,bins=10,range=(0,11))
plt.ylabel('overall')

plt.subplot(3,1,2)
plt.hist(df.intel[df.dec==1],bins=10,range=(0,11))
plt.ylabel('decide yes')

plt.subplot(3,1,3)
plt.hist(df.intel[df.dec==0],bins=10,range=(0,11))
plt.ylabel('decide no')
plt.xlabel('On a one to ten scale, how intelligent is partner')

print('')
print('On a one to ten scale, how do you rate intel of partner')
print('overall')
print(df.intel.describe())
print('')
print('person said yes to')
print(df[df.dec==1].intel.describe())
print('')
print('person said no to')
print(df[df.dec==0].intel.describe())
plt.show()


# Investigate fun versus decision
plt.subplot(3,1,1)
plt.hist(df.fun,bins=10,range=(0,11))
plt.ylabel('overall')

plt.subplot(3,1,2)
plt.hist(df.fun[df.dec==1],bins=10,range=(0,11))
plt.ylabel('decide yes')

plt.subplot(3,1,3)
plt.hist(df.fun[df.dec==0],bins=10,range=(0,11))
plt.ylabel('decide no')
plt.xlabel('On a one to ten scale, how fun is partner')

print('')
print('On a one to ten scale, how do you rate fun of partner')
print('overall')
print(df.fun.describe())
print('')
print('person said yes to')
print(df[df.dec==1].fun.describe())
print('')
print('person said no to')
print(df[df.dec==0].fun.describe())
plt.show()


# Investigate amb versus decision
plt.subplot(3,1,1)
plt.hist(df.amb,bins=10,range=(0,11))
plt.ylabel('overall')

plt.subplot(3,1,2)
plt.hist(df.amb[df.dec==1],bins=10,range=(0,11))
plt.ylabel('decide yes')

plt.subplot(3,1,3)
plt.hist(df.amb[df.dec==0],bins=10,range=(0,11))
plt.ylabel('decide no')
plt.xlabel('On a one to ten scale, how ambitious is partner')

print('')
print('On a one to ten scale, how do you rate amb of partner')
print('overall')
print(df.amb.describe())
print('')
print('person said yes to')
print(df[df.dec==1].amb.describe())
print('')
print('person said no to')
print(df[df.dec==0].amb.describe())
plt.show()


# Investigate shar versus decision
plt.subplot(3,1,1)
plt.hist(df.shar,bins=10,range=(0,11))
plt.ylabel('overall')

plt.subplot(3,1,2)
plt.hist(df.shar[df.dec==1],bins=10,range=(0,11))
plt.ylabel('decide yes')

plt.subplot(3,1,3)
plt.hist(df.shar[df.dec==0],bins=10,range=(0,11))
plt.ylabel('decide no')
plt.xlabel('On a one to ten scale, how shared are interests with partner')

print('')
print('On a one to ten scale, how do you rate shar of partner')
print('overall')
print(df.shar.describe())
print('')
print('person said yes to')
print(df[df.dec==1].shar.describe())
print('')
print('person said no to')
print(df[df.dec==0].shar.describe())
plt.show()
print('')

print('ratings of traits based on whether partner was given a yes or no')
print('like  (yes): '+str(df.like[df.dec==1].mean())+' like  (no): '+str(df.like[df.dec==0].mean())+' like  diff: '+str(df.like[df.dec==1].mean()-df.like[df.dec==0].mean()))
print('attr  (yes): '+str(df.attr[df.dec==1].mean())+' attr  (no): '+str(df.attr[df.dec==0].mean())+' attr  diff: '+str(df.attr[df.dec==1].mean()-df.attr[df.dec==0].mean()))
print('sinc  (yes): '+str(df.sinc[df.dec==1].mean())+' sinc  (no): '+str(df.sinc[df.dec==0].mean())+' sinc  diff: '+str(df.sinc[df.dec==1].mean()-df.sinc[df.dec==0].mean()))
print('intel (yes): '+str(df.intel[df.dec==1].mean())+' intel (no): '+str(df.intel[df.dec==0].mean())+' intel diff: '+str(df.intel[df.dec==1].mean()-df.intel[df.dec==0].mean()))
print('fun   (yes): '+str(df.fun[df.dec==1].mean())+' fun   (no): '+str(df.fun[df.dec==0].mean())+' fun   diff: '+str(df.fun[df.dec==1].mean()-df.fun[df.dec==0].mean()))
print('amb   (yes): '+str(df.amb[df.dec==1].mean())+' amb   (no): '+str(df.amb[df.dec==0].mean())+' amb   diff: '+str(df.amb[df.dec==1].mean()-df.amb[df.dec==0].mean()))
print('shar  (yes): '+str(df.shar[df.dec==1].mean())+' shar  (no): '+str(df.shar[df.dec==0].mean())+' shar  diff: '+str(df.shar[df.dec==1].mean()-df.shar[df.dec==0].mean()))
print('')

print('what people claim matters (distribute 100 points across 6 categories, 16.6 avg)')
print('attr matters: '+str(summ_df.attr1_1.mean()))
print('sinc matters: '+str(summ_df.sinc1_1.mean()))
print('intel matters: '+str(summ_df.intel1_1.mean()))
print('fun matters: '+str(summ_df.fun1_1.mean()))
print('amb matters: '+str(summ_df.amb1_1.mean()))
print('shar matters: '+str(summ_df.shar1_1.mean()))
