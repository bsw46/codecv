# Speed Dating Repo #

The speed dating dataset has a lot of information. There are variables which pertain to the individual: age, race, income, etc. Then, there are variables that pertain to an individual's impression of other candidates: "how would you rate this person's attractiveness, sincerity, etc.", did you match with this person, etc. 
After that, there are some variables which are collected at multiple times: "How do you perceive characteristic X before the speed dating, after the speed dating, a week later, a couple months later?"

The first objective I'll pursue is making an isolated individual dataset extracting individual specific characteristics and summarized dating data like match percentage, average ratings from others, etc. I don't want to focus on specific interactions. i.e. person A is X and person B is also X, therefore they like each other more. 

#Objective: 
Figure out what factors have the most influence on whether a person matches or not. 

#The Speed Dating data set: 
The data set has a large variety of variables. 


[Back to the main repo.](https://bitbucket.org/bsw46/codecv)
