import numpy as np
import random

# Neural Network with the following features:
# Sigmoid neurons
# Quadratic cost function 
# 

def a_fun(x):
    return (x+np.absolute(x))/2

#### Miscellaneous functions
def sigmoid(z):
    """The sigmoid function."""
    return 1.0/(1.0+np.exp(-z))

def sigmoid_prime(z):
    """Derivative of the sigmoid function."""
    return sigmoid(z)*(1-sigmoid(z))

class Network(object):
    def __init__(self,sizes):
        self.num_layers = len(sizes)
        self.sizes = sizes
        self.biases = []
        self.weights = []
        self.biases_momentum = []
        self.weights_momentum = []
        
        for i in range(self.num_layers-1):
            self.biases += [np.random.randn(sizes[i+1],1)]
            self.weights += [np.random.randn(sizes[i+1],sizes[i])]
            self.biases_momentum += [np.zeros((sizes[i+1],1))]
            self.weights_momentum += [np.zeros((sizes[i+1],sizes[i]))]

    def feedforward(self,input_layer,translator = None):
        output_layer = input_layer
        N = len(input_layer.transpose())
        for i in range(self.num_layers-1):
            output_layer = sigmoid(np.dot(self.weights[i],output_layer)+np.ones((1,N))*self.biases[i])
        if translator:
            return np.dot(translator,output_layer)
        return output_layer

    def SGD(self,train_inputs,train_outputs,epochs, batch_size, eta,test_inputs=[],test_outputs=[]):
        if isinstance(test_inputs,np.ndarray):
            n_test = len(test_inputs.T)
        n =len(train_inputs.T)
        indeces = [i for i in range(0,n)]
        for epoch in range(epochs):
            #print("Start epoch "+str(epoch))
            random.shuffle(indeces)
            mini_batch_inputs = [train_inputs[:,indeces[k:k+batch_size]]
                            for k in range(0,n,batch_size)]
            mini_batch_outputs = [train_outputs[:,indeces[k:k+batch_size]]
                            for k in range(0,n,batch_size)]
            for m in range(len(mini_batch_inputs)):
                self.update_mini_batch(mini_batch_inputs[m],mini_batch_outputs[m],eta)
            if isinstance(test_inputs,np.ndarray):
                print("Epoch "+str(epoch)+": "+str(self.evaluate(test_inputs,test_outputs))+
                      " / " +str(n_test))

    def update_mini_batch(self,inputs,outputs,eta):
        delta_b, delta_w = self.backprop(inputs,outputs)
        N = len(inputs.transpose())
        for i in range(self.num_layers-1):
            self.weights_momentum[i] = .5*self.weights_momentum[i]-(eta/N)*delta_w[i]
            self.biases_momentum[i] = .5*self.biases_momentum[i]-(eta/N)*np.sum(delta_b[i],axis=1).reshape((self.sizes[i+1],1))
            self.weights[i] += self.weights_momentum[i]
            self.biases[i] += self.biases_momentum[i]
        

    def backprop(self,inputs,outputs):
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        # feedforward
        activation = inputs
        activations = [inputs] # list to store all the activations, layer by layer
        zs = [] # list to store all the z vectors, layer by layer
        N = len(inputs.transpose())
        for b, w in zip(self.biases, self.weights):
            z = np.dot(w, activation)+np.ones((1,N))*b
            zs.append(z)
            activation = sigmoid(z)
            activations.append(activation)
        # backward pass
        delta = self.cost_derivative(activations[-1], outputs) * \
            sigmoid_prime(zs[-1])
        nabla_b[-1] = np.sum(delta,axis=1).reshape((self.sizes[-1],1))
        nabla_w[-1] = np.dot(delta, activations[-2].transpose())
        # Note that the variable l in the loop below is used a little
        # differently to the notation in Chapter 2 of the book.  Here,
        # l = 1 means the last layer of neurons, l = 2 is the
        # second-last layer, and so on.  It's a renumbering of the
        # scheme in the book, used here to take advantage of the fact
        # that Python can use negative indices in lists.
        for l in range(2, self.num_layers):
            z = zs[-l]
            sp = sigmoid_prime(z)
            delta = np.dot(self.weights[-l+1].transpose(), delta) * sp
            nabla_b[-l] = np.sum(delta,axis=1).reshape((self.sizes[-l],1))
            nabla_w[-l] = np.dot(delta, activations[-l-1].transpose())
        return (nabla_b, nabla_w)

    def evaluate(self,inputs,outputs):
        N=outputs.size
        test_results = np.argmax(self.feedforward(inputs),axis=0).reshape((1,N))
        return np.sum((test_results==outputs).astype(int))

    def cost_derivative(self, output_activations, y):
        """Return the vector of partial derivatives \partial C_x /
        \partial a for the output activations."""
        N = len(y.transpose())
        y_feature = ((np.ones((1,N))*np.arange(0,10).reshape(10,1)-y)==0).astype(int)
        return (output_activations-y_feature)


