import better_neural_network as mn
import numpy as np
import os
import re
import matplotlib.pyplot as plt

os.chdir('./datasets')

train_stream = open('train.csv','r')
data = train_stream.readlines()
io_regex = re.compile(r'^(\d),(.*)$')
train_inputs = []
train_outputs = []
valid_inputs = []
valid_outputs = []
train = []
valid = []

for i in range(1,35001):
    mo = io_regex.search(data[i])
    train_inputs.append(mo.group(2).split(','))
    train_outputs.append(int(mo.group(1)))
train_N = len(train_inputs)
train_inputs = np.array(train_inputs,dtype=float).transpose()/255.0
train_outputs = np.reshape(train_outputs,(1,train_N))
train_outputs = ((np.ones((1,train_N))*np.arange(0,10).reshape(10,1)-train_outputs)==0).astype(int)

for i in range(35001,42001):
    mo = io_regex.search(data[i])
    valid_inputs.append(mo.group(2).split(','))
    valid_outputs.append(int(mo.group(1)))
valid_N = len(valid_inputs)
valid_inputs = np.array(valid_inputs,dtype=float).transpose()/255.0
valid_outputs = np.reshape(valid_outputs,(1,valid_N))
valid_outputs = ((np.ones((1,valid_N))*np.arange(0,10).reshape(10,1)-valid_outputs)==0).astype(int)



#classifier.feedforward(train_feature_input)
classifier = mn.Network([784,100,10])

#print(classifier.feedforward(train_inputs).shape)
for i in range (1,31):
    batch_size = 15*i
    eta = .03
    print("mini batch size is: "+str(batch_size)+", eta: "+str(eta)) 
    classifier.SGD(train_inputs,train_outputs,2,batch_size,eta,valid_inputs,valid_outputs,True,True,True,True)

#classifier.SGD(train_inputs,train_outputs,30,40,3.0,valid_inputs,valid_outputs,True,True)


##SP500_low = []1
##SP500_time = []   
##
##SP500_array = np.array(match_object).transpose().astype(float)
##SP500_low = np.array(SP500_array[5][:])
##SP500_open = np.array(SP500_array[3][:])
##SP500_time = SP500_array[0][:] + SP500_array[1][:]*(1.0/12.0) + SP500_array[2][:]*(1.0/365.0)
##data_points = len(SP500_time)
##
##training_data =[]
##testing_data = []
##for i in range(60,data_points-6000,60):
##    input_layer = SP500_open[i-60:i].reshape((60,1))/np.mean(SP500_open[i-60:i])-.5
##    two_month_low = np.min(SP500_low[i:i+60])/np.mean(SP500_open[i-60:i])
##    if two_month_low < .934:
##        output_layer = np.array([[1],[0],[0],[0]])
##    elif  two_month_low < .965:
##        output_layer = np.array([[0],[1],[0],[0]])
##    elif  two_month_low < .986:
##        output_layer = np.array([[0],[0],[1],[0]])
##    else:
##        output_layer = np.array([[0],[0],[0],[1]])
##    training_data = training_data +[(input_layer,output_layer)]
##
##for i in range(data_points-6000,data_points,60):
##    input_layer = SP500_open[i-60:i].reshape((60,1))/np.mean(SP500_open[i-60:i])-.5
##    two_month_low = np.min(SP500_low[i:i+60])/np.mean(SP500_open[i-60:i])
##    if two_month_low < .934:
##        output_layer = np.array([[1],[0],[0],[0]])
##    elif  two_month_low < .965:
##        output_layer = np.array([[0],[1],[0],[0]])
##    elif  two_month_low < .986:
##        output_layer = np.array([[0],[0],[1],[0]])
##    else:
##        output_layer = np.array([[0],[0],[0],[1]])
##    testing_data = testing_data +[(input_layer,output_layer)]
##
##
##predictor = mn.Network([60,20,4])
##answer = predictor.feedforward(training_data[0][0])
##print(predictor.feedforward(training_data[0][0]))
##
##predictor.SGD(training_data, 400,30,1,testing_data)
##
##start = 1951
##initial_years = np.arange(start,1977)
##simulation_years = 40
##annual_contribution = 5500
##print("Strategy G: Use neural net to predict price.")
### Requires future prediction.
### A benchmark for future prediction performance 
##end_value_G = np.zeros(initial_years.shape)
##for initial_year in initial_years:
##    initial_index = bisection_search(SP500_time,initial_year,0,data_points)
##    end_index = bisection_search(SP500_time,initial_year+simulation_years,0,data_points)
##    cash = 0
##    shares = 0
##    purchases = 1.0
##    temp_initial_year = initial_year + 0
##    buy_bias = .03
##    # Every day
##    for i in range(initial_index,end_index):
##        # at start of year
##        if (SP500_time[i] > temp_initial_year):
##            # Predictor says price will go low
##            prediction = np.argmax(predictor.feedforward(np.array(SP500_open[i-60:i].reshape((60,1)))))
##            #It won't buy now
##            if prediction==3:
##                shares+= annual_contribution/SP500_open[i]
##            #It will drop, set buy limit
##            else:
##                # The predicted price
##                if prediction == 2:
##                    buy_price = (.986+buy_bias)*np.mean(SP500_open[i-60:i])
##                elif prediction == 1:
##                    buy_price = (.965+buy_bias)*np.mean(SP500_open[i-60:i])
##                else:
##                    buy_price = (.934+buy_bias)*np.mean(SP500_open[i-60:i])
##                # Does it work?
##                if buy_price > np.min(SP500_low[i:i+60]):
##                    shares+= annual_contribution/buy_price
##                else:
##                    shares+= annual_contribution/SP500_open[i+60]
##            temp_initial_year += 1
##    end_value_G[initial_year-initial_years[0]] = shares*SP500_open[end_index]
##
##plt.hist(end_value_G/1000,bins=10)
##plt.xlabel("Value of account after 40 years ($ thousand)")
##print("The average value of the account is "+str(np.mean(end_value_G)))
##print("The median value of the account is "+str(np.median(end_value_G)))
##print("The standard deviation of account value is "+str(np.std(end_value_G)))
##plt.show()
print("")
