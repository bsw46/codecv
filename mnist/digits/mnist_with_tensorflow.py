import numpy as np
import os
import re
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras

os.chdir('./datasets')

train_stream = open('train.csv','r')
data = train_stream.readlines()
io_regex = re.compile(r'^(\d),(.*)$')
train_inputs = []
train_outputs = []
valid_inputs = []
valid_outputs = []
train = []
valid = []

for i in range(1,35001):
    mo = io_regex.search(data[i])
    train_inputs.append(mo.group(2).split(','))
    train_outputs.append(int(mo.group(1)))
train_N = len(train_inputs)
train_inputs = np.array(train_inputs,dtype=float)/255.0
train_outputs = np.array(train_outputs)

for i in range(35001,42001):
    mo = io_regex.search(data[i])
    valid_inputs.append(mo.group(2).split(','))
    valid_outputs.append(int(mo.group(1)))
valid_N = len(valid_inputs)
valid_inputs = np.array(valid_inputs,dtype=float)/255.0
valid_outputs = np.array(valid_outputs)

print(train_inputs.shape)
model = keras.Sequential([
    keras.layers.Dense(128,activation=tf.nn.relu),
    keras.layers.Dense(128,activation=tf.nn.softmax),
])
model.compile(optimizer = tf.train.AdamOptimizer(),
              loss = 'sparse_categorical_crossentropy',
              metrics=['accuracy'])
model.fit(train_inputs,train_outputs,epochs = 5)

# Evaluate accuracy
test_loss, test_acc = model.evaluate(valid_inputs,valid_outputs)
print('Test accuracy:',test_acc)

#predictions = model.predict(test_images)

