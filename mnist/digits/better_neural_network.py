import numpy as np
import matplotlib.pyplot as plt
import random

# Neural Network with the following
# Cross entropy function
# Momentum
# Plotting
# Regularization
# Improved initialization

class CrossEntropyCost(object):
    @staticmethod
    def fn(a,y):
        return np.sum(np.nan_to_num(-y*np.log(a)-(1-y)*np.log(1-a)))
    @staticmethod
    def delta(z,a,y):
        return (a-y)

class QuadraticCost(object):
    @staticmethod
    def fn(a,y):
        return .5*np.linalg.norm(a-y)**2
    @staticmethod
    def delta(z,a,y):
        return (a-y) * sigmoid_prime(z)

def a_fun(x):
    return (x+np.absolute(x))/2

#### Miscellaneous functions
def sigmoid(z):
    """The sigmoid function."""
    return 1.0/(1.0+np.exp(-z))

def sigmoid_prime(z):
    """Derivative of the sigmoid function."""
    return sigmoid(z)*(1-sigmoid(z))

class Network(object):
    def __init__(self,sizes,cost=CrossEntropyCost):
        self.num_layers = len(sizes)
        self.sizes = sizes
        self.biases = []
        self.weights = []
        self.biases_momentum = []
        self.weights_momentum = []
        self.lmbda = .08
        self.momentum = 0.995
        self.default_weight_initializer()
        self.cost = cost
        
    def default_weight_initializer(self):
        for i in range(self.num_layers-1):
            self.biases += [np.random.randn(self.sizes[i+1],1)]
            self.weights += [np.random.randn(self.sizes[i+1],self.sizes[i])*2/(self.sizes[i]**(.5))]
            self.biases_momentum += [np.zeros((self.sizes[i+1],1))]
            self.weights_momentum += [np.zeros((self.sizes[i+1],self.sizes[i]))]
    def large_weight_initializer(self):
        for i in range(self.num_layers-1):
            self.biases += [np.random.randn(self.sizes[i+1],1)]
            self.weights += [np.random.randn(self.sizes[i+1],self.sizes[i])*2]
            self.biases_momentum += [np.random.randn((self.sizes[i+1],1))*10]
            self.weights_momentum += [np.random.randn((self.sizes[i+1],self.sizes[i]))*10]
##            self.biases_momentum += [np.zeros((self.sizes[i+1],1))]
##            self.weights_momentum += [np.zeros((self.sizes[i+1],self.sizes[i]))]

    def feedforward(self,input_layer,translator = None):
        output_layer = input_layer
        N = len(input_layer.transpose())
        for i in range(self.num_layers-1):
            output_layer = sigmoid(np.dot(self.weights[i],output_layer)+np.ones((1,N))*self.biases[i])
        if translator:
            return np.dot(translator,output_layer)
        return output_layer

    def SGD(self,train_inputs,train_outputs,epochs, batch_size, eta,
            test_inputs=[],test_outputs=[],
            monitor_training_cost=False,
            monitor_training_accuracy = False,
            monitor_evaluation_cost=False,
            monitor_evaluation_accuracy = False):
        evaluation_cost, evaluation_accuracy = [], []
        training_cost, training_accuracy = [], []
        if isinstance(test_inputs,np.ndarray):
            n_test = len(test_inputs.T)
        n =len(train_inputs.T)
        indeces = [i for i in range(0,n)]
        if monitor_training_cost:
            train_cost = [self.accuracy(train_inputs,train_outputs)*1.0/(n*1.0)]
        
        for epoch in range(epochs):
            random.shuffle(indeces)
            mini_batch_inputs = [train_inputs[:,indeces[k:k+batch_size]]
                            for k in range(0,n,batch_size)]
            mini_batch_outputs = [train_outputs[:,indeces[k:k+batch_size]]
                            for k in range(0,n,batch_size)]
            for m in range(len(mini_batch_inputs)):
                self.update_mini_batch(mini_batch_inputs[m],mini_batch_outputs[m],eta)
            if isinstance(test_inputs,np.ndarray):
                print("Epoch "+str(epoch)+" complete: ")
            if monitor_training_cost:
                cost = self.total_cost(train_inputs,train_outputs, self.lmbda)
                training_cost.append(cost)
                print("Cost on training data: {}".format(cost))#/n))
            if monitor_training_accuracy:
                accuracy = self.accuracy(train_inputs,train_outputs)
                training_accuracy.append(accuracy)
                print("Accuracy on training data: {} / {}".format(
                    accuracy, n))
            if monitor_evaluation_cost:
                cost = self.total_cost(test_inputs,test_outputs, self.lmbda)
                evaluation_cost.append(cost)
                print ("Cost on evaluation data: {}".format(cost))#/n_test))
            if monitor_evaluation_accuracy:
                accuracy = self.accuracy(test_inputs,test_outputs)
                evaluation_accuracy.append(accuracy)
                print ("Accuracy on evaluation data: {} / {}".format(
                    self.accuracy(test_inputs,test_outputs),n_test))
            print("")
        
    def update_mini_batch(self,inputs,outputs,eta):
        delta_b, delta_w = self.backprop(inputs,outputs)
        N = len(inputs.transpose())
        for i in range(self.num_layers-1):
            self.weights_momentum[i] = self.momentum*self.weights_momentum[i]-(eta/N)*delta_w[i]-(eta/N)*self.lmbda*self.weights[i]
            self.biases_momentum[i] = self.momentum*self.biases_momentum[i] -(eta/N)*np.sum(delta_b[i],axis=1).reshape((self.sizes[i+1],1))
            self.weights[i] += self.weights_momentum[i]
            self.biases[i] +=self.biases_momentum[i]
        

    def backprop(self,inputs,outputs):
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        # feedforward
        activation = inputs
        activations = [inputs] # list to store all the activations, layer by layer
        zs = [] # list to store all the z vectors, layer by layer
        N = len(inputs.transpose())
        for b, w in zip(self.biases, self.weights):
            z = np.dot(w, activation)+np.ones((1,N))*b
            zs.append(z)
            activation = sigmoid(z)
            activations.append(activation)
        # backward pass
        delta = (self.cost).delta(zs[-1],activations[-1],
                                  outputs)
                                  #((np.ones((1,N))*np.arange(0,10).reshape(10,1)-outputs)==0).astype(int))#*sigmoid_prime(zs[-1])
        
        nabla_b[-1] = np.sum(delta,axis=1).reshape((self.sizes[-1],1))
        nabla_w[-1] = np.dot(delta, activations[-2].transpose())
        # Note that the variable l in the loop below is used a little
        # differently to the notation in Chapter 2 of the book.  Here,
        # l = 1 means the last layer of neurons, l = 2 is the
        # second-last layer, and so on.  It's a renumbering of the
        # scheme in the book, used here to take advantage of the fact
        # that Python can use negative indices in lists.
        for l in range(2, self.num_layers):
            z = zs[-l]
            sp = sigmoid_prime(z)
            delta = np.dot(self.weights[-l+1].transpose(), delta) * sp
            nabla_b[-l] = np.sum(delta,axis=1).reshape((self.sizes[-l],1))
            nabla_w[-l] = np.dot(delta, activations[-l-1].transpose())
        return (nabla_b, nabla_w)

    def accuracy(self,inputs,outputs):
        N=len(outputs.T)
        test_results = np.argmax(self.feedforward(inputs),axis=0)
        correct_results = np.argmax(outputs,axis=0)
        
        return np.sum(correct_results==test_results)

    def total_cost(self, inputs,outputs, lmbda, convert=False):
        """Return the total cost for the data set ``data``.  The flag
        ``convert`` should be set to False if the data set is the
        training data (the usual case), and to True if the data set is
        the validation or test data.  See comments on the similar (but
        reversed) convention for the ``accuracy`` method, above.
        """
        cost = 0.0
        a = self.feedforward(inputs)
        cost = self.cost.fn(a, outputs)/len(inputs)
        return cost

    def save(self, filename):
        """Save the neural network to the file ``filename``."""
        data = {"sizes": self.sizes,
                "weights": [w.tolist() for w in self.weights],
                "biases": [b.tolist() for b in self.biases],
                "cost": str(self.cost.__name__)}
        f = open(filename, "w")
        #json.dump(data, f)
        f.close()
        
    def cost_derivative(self, output_activations, y):
        """Return the vector of partial derivatives \partial C_x /
        \partial a for the output activations."""
        N = len(y.transpose())
        y_feature = ((np.ones((1,N))*np.arange(0,10).reshape(10,1)-y)==0).astype(int)
        return (output_activations-y_feature)


