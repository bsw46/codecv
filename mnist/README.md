#mnist Repo#
This repo contains the source code for calssifying images for two different mnist data sets: digits and clothing. The classifiers were built with neural networks, and for the purpose of learning more about machine learning and deep learning. I built neural two neural network variants from scratch for the digit data set following Michael Nielsen's guide Neural Networks and Deep Learning. After that, I learned to use Keras and build convolutional networks for classifying data from these two data sets. 
  
[Back to the main repo.](https://bitbucket.org/bsw46/codecv)
