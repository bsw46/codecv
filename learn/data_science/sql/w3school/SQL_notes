-- Tutorial is from w3school.com
-- Get all records (rows) from database. Also displays all columns.
-- Semicolon sometimes required after SQL statement.
SELECT * FROM table_name;

-- Key Commands
SELECT, UPDATE, DELETE, INSERT INTO, CREATE DATABASE, ALTER DATABASE, CREATE TABLE, ALTER TABLE, DROP TABLE, CREATE INDEX, DROP INDEX

-- Display specific columns from the table
SELECT CustomerName, ContactName FROM table_name;

-- Get a list of the different types of customers. 
SELECT DISTINCT Country FROM Customers;

-- The WHERE clause allows a condition to be added to the data being pulled.
-- Note, use quotes when working with strings. 
-- Operators =, <>, >, <, >=, <=, BETWEEN, LIKE, IN
SELECT * FROM Customers WHERE Country = 'Mexico';
SELECT * FROM Customers WHERE CustomerID=1; 

-- SQL supports logical operators
SELECT * FROM Customers WHERE Country='Germany' AND City='Berlin'; 
SELECT * FROM Customers WHERE City='Berlin' OR City='London'; 
SELECT * FROM Customers WHERE NOT Country='Germany'; 

-- ORDER BY keyword is used to sort records.
-- Use with the ASC and DESC keywords for ascending and descending orders.
SELECT * FROM Customers ORDER BY Country;
SELECT * FROM Customers ORDER BY Country DESC;
SELECT * FROM Customers ORDER BY Country ASC, CustomerName DESC;

-- INSERT INTO adds a record to a table.
-- If only some of the columns are being added, they need to be specified with corresponding values.
-- If a complete record with all columns, only the values need to be specified
-- Note: CustomerID auto-increments.
INSERT INTO Customers (CustomerName,Country) VALUES ('Brandon','USA');
INSERT INTO Customers VALUES ('Cardinal', 'Tom B. Erichsen', 'Skagen 21', 'Stavanger', '4006', 'Norway');

-- NULL fields have no value.
-- There are syntax for checking if a column has NULL fields.
SELECT LastName,FirstName,Address FROM Persons WHERE Address IS NULL;

-- UPDATE statements are used to modify an existing record
-- Examples: Updating 1, multiple or all data.
-- Always use UPDATE with a WHERE statement to avoid overwriting a column of data.
UPDATE Customers SET ContactName = 'Alfred Schmidt', City = 'Frankfurt' WHERE CustomerID = 1;
UPDATE Customers SET ContactName = 'Juan' WHERE Country = 'Mexico';

-- DELETE Statement is used to delete records from tables.
-- Always use with a WHERE statement to control what is deleted.
-- Clear all rows without deleting the table
DELETE FROM Customers WHERE CustomerName='Alfreds Futterkiste';
DELETE FROM Customers;
INSERT INTO Customers (ContactName) VALUES ('BRANDON WAS HERE') 

-- SELECT TOP specifies the number of records to return, is useful given large tables.
-- There are multiple keywords for this including LIMIT, and PERCENT
SELECT TOP 3 * FROM Customers; SELECT * FROM Customers LIMIT 3;
SELECT TOP 50 PERCENT * FROM Customers;
SELECT TOP 3 * FROM Customers WHERE Country='Germany';

-- The MIN() and MAX() functions return the smallest and largest values respectively.
SELECT MIN(ContactName) FROM Customers WHERE Country='USA';
SELECT MIN(Price) AS SmallestPrice FROM Products;

-- Other utility functions
-- COUNT() returns the number of rows that match a specified criteria.
-- AVG() returns the average value of a numerc column.
-- SUM() returns the total sum of a numeric column.
SELECT COUNT(ProductID) FROM Products;
SELECT AVG(Price) FROM Products;
SELECT SUM(Quantity) FROM OrderDetails;

-- SQL LIKE operator - Similar to a regular expression. Uses % (.* wildcard) and _ (. wildcard) 
-- a% any values that starts with "a".       %a and entry that ends with "a".
-- %or% any values with string "or"          _r% any value with "r" in second position.
-- a_%_% starts with "a" and >2 length       a%o starts with "a", ends with "o"
SELECT * FROM Customers WHERE CustomerName LIKE 'a%';
SELECT * FROM Customers WHERE CustomerName LIKE 'a_%_%';
SELECT * FROM Customers WHERE CustomerName NOT LIKE 'a%o';

-- SQL wildcards
-- _ and % are general wildcards. 
-- [charlist] matches any character in string. '[abc]%' matches value which starts with 'a', 'b' or 'c'
-- Negated charlist [!charlist]
SELECT * FROM Customers WHERE City LIKE '[bsp]%';
SELECT * FROM Customers WHERE City LIKE '[a-c]%';
SELECT * FROM Customers WHERE City LIKE '[!bsp]%';

-- The IN operator is equivalent to the equal operator but with multiple values
-- ... WHERE Country IN ('Germany', 'France', 'UK');
-- Can use another query with the IN operator
SELECT * FROM Customers WHERE Country IN ('Germany', 'France', 'UK');
SELECT * FROM Customers WHERE Country IN (SELECT DISTINCT Country FROM Suppliers);

-- The BETWEEN operator selects values in a given range. The range is inclusive. 
-- Values can be numbers, text or dates.
SELECT * FROM Products WHERE Price (NOT) BETWEEN 10 AND 20;
SELECT * FROM Products WHERE (Price BETWEEN 10 AND 20) AND NOT CategoryID IN (1,2,3);
SELECT * FROM Products WHERE ProductName BETWEEN 'Carnarvon Tigers' AND 'Mozzarella di Giovanni'ORDER BY ProductName;
SELECT * FROM Orders WHERE OrderDate BETWEEN #07/04/1996# AND #07/09/1996#;

-- Aliases which involve the AS operator can make columns or tables more readable.
-- Third example uses information from two tables to make a query.
-- Aliases are useful when more than one table involved in a query, column names are not very readable, two or more columns are combines, functions are used in the query.
SELECT CustomerID as ID, CustomerName AS Customer FROM Customers;
SELECT CustomerName, Address + ', ' + PostalCode + ' ' + City + ', ' + Country AS Address FROM Customers;
SELECT o.OrderID, o.OrderDate, c.CustomerName
FROM Customers AS c, Orders AS o
WHERE c.CustomerName="Around the Horn" AND c.CustomerID=o.CustomerID;

-- The JOIN clause is used to combine rows from two or more tables. 
-- There are four types of joins: inner, left, right, and full
-- The ON operator is used as a conditional
SELECT Orders.OrderID, Customers.CustomerName, Orders.OrderDate
FROM Orders
INNER JOIN Customers ON Orders.CustomerID=Customers.CustomerID;

-- The INNER JOIN selects records that have matching values in both tables.
-- Displays the OrderID and CustomerName WHERE CustomerID is the same
SELECT Orders.OrderID, Customers.CustomerName
FROM Orders
INNER JOIN Customers ON Orders.CustomerID = Customers.CustomerID;

-- The LEFT JOIN selects displays all entries from the left table. 
-- If a match is missing from the table, the data may be empty.
-- Displays all Customers even ones where there is no order associated.
SELECT Customers.CustomerName, Orders.OrderID
FROM Customers
LEFT JOIN Orders ON Customers.CustomerID = Orders.CustomerID
ORDER BY Customers.CustomerName;

-- The RIGHT JOIN
-- Displays all Employees even though no order may be associated.
SELECT Orders.OrderID, Employees.LastName, Employees.FirstName
FROM Orders
RIGHT JOIN Employees
ON Orders.EmployeeID = Employees.EmployeeID
ORDER BY Orders.OrderID;

-- The FULL JOIN
-- Displays all rows of both tables. There may be missing information on either side.
SELECT Customers.CustomerName, Orders.OrderID
FROM Customers
FULL OUTER JOIN Orders ON Customers.CustomerID=Orders.CustomerID
ORDER BY Customers.CustomerName;

-- The SELF JOIN clause 
-- This query shows pairs of customers organized by city.
SELECT A.CustomerName AS CustomerName1, B.CustomerName AS CustomerName2, A.City
FROM Customers AS A, Customers AS B
WHERE A.CustomerID <> B.CustomerID
AND A.City = B.City 
ORDER BY A.City;

-- UNION operator combines the results of multiple SELECT statements. 
-- The SELECT statements need to return the same number of columns.
-- The UNION applies a DISTINCT SELECT effect. UNION ALL does not.
SELECT City FROM Customers UNION SELECT City FROM Suppliers ORDER BY City;

SELECT City, Country FROM Customers WHERE Country='Germany' UNION
SELECT City, Country FROM Suppliers WHERE Country='Germany' ORDER BY City;

SELECT 'Customer' As Type, ContactName, City, Country FROM Customers UNION
SELECT 'Supplier', ContactName, City, Country FROM Suppliers;

-- GROUP BY is used with SQL functions to parse the results of the functions.
-- Second example returns how many orders were made by which customers.
SELECT COUNT(CustomerID), Country FROM Customers GROUP BY Country ORDER BY COUNT(CustomerID) DESC;

SELECT Shippers.ShipperName, COUNT(Orders.OrderID) AS NumberOfOrders FROM Orders
LEFT JOIN Shippers ON Orders.ShipperID = Shippers.ShipperID
GROUP BY ShipperName;

-- HAVING clause used in place of WHERE but applies to SQL Functions.

SELECT COUNT(CustomerID), Country FROM Customers GROUP BY Country HAVING COUNT(CustomerID) > 5;


-- EXISTS operator acts as a true false test for a query. 
-- The following SQL statement returns TRUE and lists the suppliers with a product price less than 20:
SELECT SupplierName
FROM Suppliers
WHERE EXISTS (SELECT ProductName FROM Products WHERE SupplierId = Suppliers.supplierId AND Price < 20);

-- ANY and ALL are used with WHERE or HAVING
-- The ANY operator returns true if any of the subquery values meet the condition.
-- The ALL operator returns true if all of the subquery values meet the contion
SELECT ProductName
FROM Products
WHERE ProductID = ANY (SELECT ProductID FROM OrderDetails WHERE Quantity > 99);
SELECT ProductName
FROM Products
WHERE ProductID = ALL (SELECT ProductID FROM OrderDetails WHERE Quantity = 10);

-- SELECT INTO used to copy information from one table to another.

-- IFNULL(), ISNULL(), COALESCE(), NVL()

-- Stored procedures are custom functions
CREATE PROCEDURE procedure_name
AS 
sql_statement
GO;

EXEC procedure_name
