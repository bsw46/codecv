# parts 1 and 2

portion_down_payment=.25
r=.04
annual_salary=float(input("Enter your starting annual salary: "))
portion_saved=float(input("Enter the cost of your salary to save, as a decimal: "))
total_cost = float(input("Enter the cost of your dream home: "))
down_payment=portion_down_payment*total_cost

current_savings=0
month_count=0
while current_savings < down_payment:
    month_count=month_count+1
    current_savings = current_savings*(1+r/12) + annual_salary*portion_saved/12
print("Number of months: "+str(month_count))

semi_annual_raise = float(input("Enter the semi-annual raise, as a decimal: "))

current_savings=0
month_count=0
while current_savings < down_payment:
    if ((month_count > 0) & (month_count%6==0)):
            annual_salary=annual_salary*(1+semi_annual_raise)
    month_count=month_count+1
    current_savings = current_savings*(1+r/12) + annual_salary*portion_saved/12
print("Number of months: "+str(month_count))


# part 3

total_cost=1000000
portion_down_payment=.25
r=.04
semi_annual_raise = .07

month_count=0
annual_salary=float(input("Enter your starting annual salary: "))
reset_salary=annual_salary*1.0
down_payment=portion_down_payment*total_cost
upper=1
lower=0

current_savings=0
for i in range(36):
    if ((i> 0) & (i%6==0)):
        annual_salary=annual_salary*(1+semi_annual_raise)
    current_savings = current_savings*(1+r/12) + annual_salary*(1)/12
if current_savings<down_payment:
    print("Not possible to pay for downpayment in 3 years.")
else:
    found_portion=False
    bi_steps=0
    while (not found_portion):
        month_count=0
        bi_steps=bi_steps+1
        portion_saved=upper*.5+lower*.5
        current_savings=0
        annual_salary=reset_salary
        for i in range(36):
            if ((i > 0) & (i%6==0)):
                annual_salary=annual_salary*(1+semi_annual_raise)
            current_savings = current_savings*(1+r/12) + annual_salary*portion_saved/12
        print(current_savings)
        if current_savings>down_payment+100:
            upper=portion_saved
            continue
        elif current_savings<down_payment-100:
            lower=portion_saved
            continue
        else:
            found_portion=True
            break
    print("Number of steps: "+str(bi_steps))
    print("portion saved: "+str(portion_saved))
