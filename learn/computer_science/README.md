
This repository contains non-project work I've done on my journey to learn computer science.
Mainly, this has worked out answers to course homeworks and exercises from books.

I'm following the guide to learning cs from https://teachyourselfcs.com/.

Under programming, the folder sicp contains my solutions to the books problems.

 
