# Answer for exercise 1.12 in 
# Structures and Interpretation of Computer Programs


(define (pascal r i)
    (if (or (= i 1) (= i r ) ) 1 
        (+ (pascal (- r 1) i) 
           (pascal (- r 1) (- i 1))
        ) 
    )
)

