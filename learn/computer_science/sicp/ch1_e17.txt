# Answer for exercise 1.17 in 
# Structures and Interpretation of Computer Programs

(define (m a b) (m-ss 0 a b))

(define (m-ss f a b) 
    (cond ((= b 0) f)
          ((even? b) (m-ss f (double a) (halve b)  ))
          (else (m-ss (+ f a) a (- b 1))) 
    )
)

(define (double x) (+ x x))
(define (halve x) (/ x 2))
(define (even? x) (= (remainder x 2) 0))
