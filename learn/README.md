# learn repo #
This repository presents learning I've done on my own after leaving academia. This added learning builds off and broadens the education I gained from completing a bachelor's of science in chemical engineering. 

Completed material:
+ MIT OCW, Intro to Computer Science with Python

+ Coursera, Andrew Ng's Machine Learning course 

+ TensorFlow Keras tutorials from TensorFlow

+ W3school, SQL tutorial

In progress material:
+ Reading the Deep Learning Book by Ian Goodfellow and Yoshua Bengio and Aaron Courville 

+ Al Sweigart's Automate the Boring Stuff

[Back to the main repo.](https://bitbucket.org/bsw46/codecv)
