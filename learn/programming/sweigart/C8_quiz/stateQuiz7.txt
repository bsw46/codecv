Name: 

Date: 

Section:

This is randomized state quiz number 7.
This quiz will test you on your knowledge of state capitols.
This quiz is unique from all the others, so don't cheat.


Question 0: What is the state capitol of Utah?
    0. Des Moines
    1. Charleston
    2. Charleston
    3. Salt Lake City
    4. Jackson

Question 1: What is the state capitol of Louisiana?
    0. Phoenix
    1. Jackson
    2. Baton Rouge
    3. Hartford
    4. Saint Paul

Question 2: What is the state capitol of Wyoming?
    0. Pierre
    1. Cheyenne
    2. Tallahassee
    3. Denver
    4. Oklahoma City

Question 3: What is the state capitol of Idaho?
    0. Bismarck
    1. Helena
    2. Albany
    3. Boise
    4. Pierre

Question 4: What is the state capitol of Arkansas?
    0. Oklahoma City
    1. Little Rock
    2. Indianapolis
    3. Baton Rouge
    4. Helena

Question 5: What is the state capitol of Colorado?
    0. Harrisburg
    1. Saint Paul
    2. Raleigh
    3. Carson City
    4. Denver

Question 6: What is the state capitol of Kansas?
    0. Topeka
    1. Tallahassee
    2. Richmond
    3. Santa Fe
    4. Olympia

Question 7: What is the state capitol of Pennsylvania?
    0. Harrisburg
    1. Tallahassee
    2. Columbia
    3. Dover
    4. Pierre

Question 8: What is the state capitol of Oklahoma?
    0. Concord
    1. Oklahoma City
    2. Montgomery
    3. Nashville
    4. Montpelier

Question 9: What is the state capitol of Alaska?
    0. Saint Paul
    1. Juneau
    2. Pierre
    3. Richmond
    4. Boise

Question 10: What is the state capitol of New York?
    0. Nashville
    1. Montgomery
    2. Indianapolis
    3. Hartford
    4. Albany

Question 11: What is the state capitol of Oregon?
    0. Trenton
    1. Montgomery
    2. Salem
    3. Bismarck
    4. Madison

Question 12: What is the state capitol of Illinois?
    0. Springfield
    1. Dover
    2. Boise
    3. Providence
    4. Boston

Question 13: What is the state capitol of Maine?
    0. Des Moines
    1. Atlanta
    2. Charleston
    3. Augusta
    4. Salem

Question 14: What is the state capitol of Rhode Island?
    0. Austin
    1. Providence
    2. Denver
    3. Montpelier
    4. Augusta

Question 15: What is the state capitol of Wisconsin?
    0. Montgomery
    1. Madison
    2. Richmond
    3. Montpelier
    4. Atlanta

Question 16: What is the state capitol of Florida?
    0. Charleston
    1. Des Moines
    2. Trenton
    3. Tallahassee
    4. Santa Fe

Question 17: What is the state capitol of Alabama?
    0. Nashville
    1. Montgomery
    2. Richmond
    3. Topeka
    4. Oklahoma City

Question 18: What is the state capitol of North Carolina?
    0. Baton Rouge
    1. Lansing
    2. Montpelier
    3. Baton Rouge
    4. Raleigh

Question 19: What is the state capitol of North Dakota?
    0. Salem
    1. Augusta
    2. Honolulu
    3. Harrisburg
    4. Bismarck

Question 20: What is the state capitol of Connecticut?
    0. Providence
    1. Hartford
    2. Montpelier
    3. Des Moines
    4. Augusta

Question 21: What is the state capitol of South Carolina?
    0. Boston
    1. Columbia
    2. Austin
    3. Carson City
    4. Salem

Question 22: What is the state capitol of New Jersey?
    0. Trenton
    1. Boise
    2. Honolulu
    3. Salem
    4. Carson City

Question 23: What is the state capitol of Nevada?
    0. Carson City
    1. Lincoln
    2. Juneau
    3. Dover
    4. Salem

Question 24: What is the state capitol of Washington?
    0. Springfield
    1. Albany
    2. Olympia
    3. Hartford
    4. Bismarck

Question 25: What is the state capitol of Ohio?
    0. Columbia
    1. Hartford
    2. Columbus
    3. Des Moines
    4. Boise

Question 26: What is the state capitol of Nebraska?
    0. Santa Fe
    1. Pierre
    2. Annapolis
    3. Carson City
    4. Lincoln

Question 27: What is the state capitol of New Hampshire?
    0. Atlanta
    1. Boston
    2. Concord
    3. Atlanta
    4. Jefferson City

Question 28: What is the state capitol of Hawaii?
    0. Austin
    1. Cheyenne
    2. Honolulu
    3. Baton Rouge
    4. Frankfort

Question 29: What is the state capitol of Montana?
    0. Helena
    1. Trenton
    2. Raleigh
    3. Concord
    4. Sacramento

Question 30: What is the state capitol of Arizona?
    0. Phoenix
    1. Montgomery
    2. Dover
    3. Annapolis
    4. Frankfort

Question 31: What is the state capitol of Missouri?
    0. Lansing
    1. Annapolis
    2. Montpelier
    3. Atlanta
    4. Jefferson City

Question 32: What is the state capitol of Indiana?
    0. Indianapolis
    1. Helena
    2. Des Moines
    3. Topeka
    4. Concord

Question 33: What is the state capitol of New Mexico?
    0. Santa Fe
    1. Carson City
    2. Bismarck
    3. Carson City
    4. Montgomery

Question 34: What is the state capitol of Vermont?
    0. Denver
    1. Hartford
    2. Montpelier
    3. Topeka
    4. Bismarck

Question 35: What is the state capitol of South Dakota?
    0. Columbia
    1. Providence
    2. Pierre
    3. Boston
    4. Juneau

Question 36: What is the state capitol of Mississippi?
    0. Raleigh
    1. Jackson
    2. Lansing
    3. Columbus
    4. Nashville

Question 37: What is the state capitol of Michigan?
    0. Madison
    1. Helena
    2. Charleston
    3. Helena
    4. Lansing

Question 38: What is the state capitol of California?
    0. Juneau
    1. Boston
    2. Denver
    3. Sacramento
    4. Trenton

Question 39: What is the state capitol of Georgia?
    0. Frankfort
    1. Atlanta
    2. Raleigh
    3. Juneau
    4. Charleston

Question 40: What is the state capitol of Virginia?
    0. Raleigh
    1. Boston
    2. Denver
    3. Tallahassee
    4. Richmond

Question 41: What is the state capitol of Massachusetts?
    0. Lincoln
    1. Phoenix
    2. Concord
    3. Boston
    4. Helena

Question 42: What is the state capitol of Texas?
    0. Frankfort
    1. Helena
    2. Phoenix
    3. Bismarck
    4. Austin

Question 43: What is the state capitol of Tennessee?
    0. Phoenix
    1. Nashville
    2. Madison
    3. Montgomery
    4. Jackson

Question 44: What is the state capitol of Delaware?
    0. Bismarck
    1. Montgomery
    2. Columbia
    3. Des Moines
    4. Dover

Question 45: What is the state capitol of Iowa?
    0. Honolulu
    1. Des Moines
    2. Topeka
    3. Albany
    4. Annapolis

Question 46: What is the state capitol of West Virginia?
    0. Charleston
    1. Nashville
    2. Frankfort
    3. Salem
    4. Boise

Question 47: What is the state capitol of Minnesota?
    0. Honolulu
    1. Columbia
    2. Annapolis
    3. Lincoln
    4. Saint Paul

Question 48: What is the state capitol of Maryland?
    0. Austin
    1. Little Rock
    2. Springfield
    3. Charleston
    4. Annapolis

Question 49: What is the state capitol of Kentucky?
    0. Atlanta
    1. Columbia
    2. Frankfort
    3. Augusta
    4. Richmond
