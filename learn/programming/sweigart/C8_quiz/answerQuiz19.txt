0. 0. Springfield is the capitol of Illinois
1. 2. Pierre is the capitol of South Dakota
2. 4. Hartford is the capitol of Connecticut
3. 2. Concord is the capitol of New Hampshire
4. 2. Sacramento is the capitol of California
5. 1. Providence is the capitol of Rhode Island
6. 3. Augusta is the capitol of Maine
7. 0. Oklahoma City is the capitol of Oklahoma
8. 2. Jackson is the capitol of Mississippi
9. 1. Juneau is the capitol of Alaska
10. 3. Madison is the capitol of Wisconsin
11. 3. Austin is the capitol of Texas
12. 4. Atlanta is the capitol of Georgia
13. 1. Santa Fe is the capitol of New Mexico
14. 0. Helena is the capitol of Montana
15. 1. Montpelier is the capitol of Vermont
16. 0. Tallahassee is the capitol of Florida
17. 0. Raleigh is the capitol of North Carolina
18. 1. Olympia is the capitol of Washington
19. 4. Phoenix is the capitol of Arizona
20. 1. Des Moines is the capitol of Iowa
21. 3. Charleston is the capitol of West Virginia
22. 0. Montgomery is the capitol of Alabama
23. 3. Jefferson City is the capitol of Missouri
24. 2. Trenton is the capitol of New Jersey
25. 2. Honolulu is the capitol of Hawaii
26. 2. Dover is the capitol of Delaware
27. 1. Lincoln is the capitol of Nebraska
28. 0. Carson City is the capitol of Nevada
29. 3. Columbus is the capitol of Ohio
30. 4. Denver is the capitol of Colorado
31. 1. Harrisburg is the capitol of Pennsylvania
32. 0. Frankfort is the capitol of Kentucky
33. 2. Cheyenne is the capitol of Wyoming
34. 1. Topeka is the capitol of Kansas
35. 0. Baton Rouge is the capitol of Louisiana
36. 0. Bismarck is the capitol of North Dakota
37. 3. Annapolis is the capitol of Maryland
38. 3. Boise is the capitol of Idaho
39. 4. Albany is the capitol of New York
40. 4. Indianapolis is the capitol of Indiana
41. 0. Saint Paul is the capitol of Minnesota
42. 2. Columbia is the capitol of South Carolina
43. 1. Nashville is the capitol of Tennessee
44. 4. Little Rock is the capitol of Arkansas
45. 4. Richmond is the capitol of Virginia
46. 1. Salt Lake City is the capitol of Utah
47. 1. Lansing is the capitol of Michigan
48. 1. Boston is the capitol of Massachusetts
49. 0. Salem is the capitol of Oregon
