import random

def choosePos(board,sym):
    while True:
        cpuPick=random.randint(0,8)
        choices = ['top-L','top-M','top-R',
            'mid-L', 'mid-M','mid-R',
            'low-L', 'low-M','low-R']        
        if board[choices[cpuPick]]==' ':
            board[choices[cpuPick]]=sym
            return board
    

def printBoard(board):
    print('')
    print('The current board state:')
    print(board['top-L'] + '|' + board['top-M'] + '|' + board['top-R'])
    print('-+-+-')
    print(board['mid-L'] + '|' + board['mid-M'] + '|' + board['mid-R'])
    print('-+-+-')
    print(board['low-L'] + '|' + board['low-M'] + '|' + board['low-R'])


theBoard = {'top-L':' ', 'top-M':' ','top-R':' ',
            'mid-L':' ', 'mid-M':' ','mid-R':' ',
            'low-L':' ', 'low-M':' ','low-R':' '}
print('Do you want to go first or second?')
print('Enter any character for first or nothing for second: ')

userTurn=False
if bool(input()):
    userTurn=True

gameFinished=False
for i in range(9):
    while userTurn==True:
        printBoard(theBoard)
        print('')
        print('It is your turn; your symbol is O.')
        print('Choose a position:')
        print('top-L, top-M, top-R')
        print('mid-L, mid-M, mid-R')
        print('low-L, low-M, low-R')
        userInput=input()
        if userInput not in theBoard:
            print('That is not a valid position. Try again.')
        elif theBoard[userInput]=='X' or theBoard[userInput]=='O':
            print('That position is already filled. Try again.')
        else:
            theBoard[userInput]='O'
            userTurn=False
            continue
    if userTurn==False:
        theBoard=choosePos(theBoard,'X')
        userTurn=True
        
        
    
