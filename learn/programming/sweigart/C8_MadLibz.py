#! python3
# randomQuizGenerator.py - Creates quizzes with questions and answers in
# random order, along with the answer key.

# import random
import os
os.chdir('G:\\Documents\\AutomateBoring\\C8_workspace')

print('This program takes in a Mad Libz script as an input')
print('and returns a filled MadLibz script as output.')

inputName=input('\nPlease enter the name of a MadLibz file or nothing to exit: ')
if inputName=='':
    print('You entered nothing')
elif os.path.isfile(inputName):
    fileReader = open(inputName,'r')
    inputString = fileReader.read()
    N=len(inputString)

    madKeys=['ADJECTIVE','ADVERB','NOUN','VERB']
    for j in range(len(madKeys)):
        while madKeys[j] in inputString:
            subWord=input('Enter a '+madKeys[j].lower()+':')
            inputString=inputString.replace(madKeys[j],subWord,1)
    fileWriter = open('madOut.txt','w')
    fileWriter.write(inputString)
    fileReader.close()
    fileWriter.close()
        
else:
    print('Sorry, '+inputName+' could not be found.')
    
    
