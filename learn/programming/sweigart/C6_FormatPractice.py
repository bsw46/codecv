
tableData = [['apples', 'oranges', 'cherries', 'banana'],
             ['Alice', 'Bob', 'Carol', 'David'],
             ['dogs', 'cats', 'moose', 'goose']]
outerList=len(tableData)
innerList=len(tableData[0][:])
maxStrLen=[0]*3
for j in range(outerList):
    for i in range(innerList):
        if maxStrLen[j]<len(tableData[j][i]):
            maxStrLen[j]=len(tableData[j][i])
for k in range(innerList):
    print(tableData[0][k].rjust(maxStrLen[0])
          +tableData[1][k].rjust(maxStrLen[1]+1)
          +tableData[2][k].rjust(maxStrLen[2]+1))
