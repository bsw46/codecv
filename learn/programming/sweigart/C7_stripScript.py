# stripFunction.py

import re

userStr = input('Please enter a string: ')
userChar = input('Enter characters you want removed from the string: ')

if userChar=='':
    stripRegex = re.compile(r'^[\s]*(.*?)[\s]*$')
else:
    stripRegex = re.compile(r'^['+userChar+']*(.*?)['+userChar+']*$')

#stripRegex = re.compile(r'^[\s]*(.*)[\s]*$')
mo = stripRegex.search(userStr)
print(mo.group(1))
