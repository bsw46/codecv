import random

print('I am thinking of a number between 1 and 20.')

guesses=0
userAnswer = 50
answer = random.randint(1,20)
while answer!=userAnswer:
    print('Take a guess! Alternatively, enter 0 or a negative number to give up.')
    userAnswer = int(input())
    guesses=guesses+1
    if userAnswer<=0:
        print('Thanks for playing.')
        print('The number I thought of was '+str(answer)+'.')
        break
    elif userAnswer==answer:
        print('Good job! You guessed my number in '+str(guesses)+' guesses')
    elif userAnswer>answer:
        print('Your guess is too high.')
    else:
        print('Your guess is too low.')
        
