# Print all subsets of the array.
def subSet(set,num,max):
    if num==0:
        return ''
    else:
        if num==2**(max-1):
            return set[max-1]
        elif num>2**(max-1):
            num=num-2**(max-1)
            return subSet(set[:max-1],num,max-1)+set[max-1]
        else:
            return subSet(set,num,max-1)


def counter(set):
    N=len(set)
    print(range(2**N))
    for i in range(2**N):
        print(i)
        print(subSet(set,i,N))


Array=['a','b','c','d']
counter(Array)
