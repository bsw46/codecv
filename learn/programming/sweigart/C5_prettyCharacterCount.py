# prettyCharacterCount.py
import pprint
message = 'Long ago in a distant land, I, Aku, the shape-shifting master of darkness...'
count={}

for character in message:
    count.setdefault(character,0)
    count[character] = count[character] +1


pprint.pprint(count)
