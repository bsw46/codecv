# strongPassword.py
import re

lengthRegex=re.compile(r'\w{8}\w*')
lowerRegex=re.compile(r'[a-z]')
upperRegex=re.compile(r'[A-Z]')
digitRegex=re.compile(r'\d')

print('Hello user, \nThis program checks the strength of your password.')
userInput=input('Please enter a password for us to assess: ')

securityLevel=0
if lengthRegex.search(userInput):
    securityLevel+=1
if lowerRegex.search(userInput):
    securityLevel+=1
if upperRegex.search(userInput):
    securityLevel+=1
if digitRegex.search(userInput):
    securityLevel+=1

if securityLevel==4:
    print('Your password is strong, I couldn\'t guess it easily.')
elif securityLevel==3:
    print('Your password could be stronger.')
    print('Consider adding more alphabet or digit characters.')
else:
    print('You need a stronger password.')
    print('A strong password is long, has digits, and has upper and lower case letters.')
    
