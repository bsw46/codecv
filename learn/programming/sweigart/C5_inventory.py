# inventory.py
inventory = {'food':0,'torch(es)':0,'shovel(s)':0,'antivenom':0,'bandages':0}

def displayInventory(inventory):
    print('Inventory:')
    itemTotal=0
    for k,v in inventory.items():
        print(str(v)+' '+str(k))
        itemTotal=itemTotal+v
    print('Total number of items: '+str(itemTotal))

def addToInventory(inventory,addedItems):
    for i in range(len(addedItems)):
        inventory.setdefault(addedItems[i],0)
        inventory[addedItems[i]]+=1

    print('Updated inventory!')

displayInventory(inventory)
skeletonLoot=['gold']*1000+['busts']*6
addToInventory(inventory,skeletonLoot)
displayInventory(inventory)
