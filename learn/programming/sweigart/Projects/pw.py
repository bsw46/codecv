#! python3
#pw.py - an insecure password locker program

PASSWORDS = {'gmail': '3qh2rgqc9hryo8yO4U3NYCP98QY23CANWKEJHROQ83nks34cnkiqy34l8',
             'outlook': 'o2c4ybi8tbcxaieywx8y3rb78t8TB8O77ABCIYLIYBO8Yybp98BYLQYC3',
             'GitHub': 'ahwk4ctuiha;owur09u09qi4ytl8ua90ULOUULEYT;AFrewc4twth dth'}
import sys

if len(sys.argv)<2:
    print('Usage: python pw.py [account] - copy account password')
    sys.exit()

account = sys.argv[1]

if account in PASSWORDS.keys():
    temp=PASSWORDS[account]
    print('Password for ' + account + ' copied to clipboard.')
else:
    print('There is no account named '+account)
