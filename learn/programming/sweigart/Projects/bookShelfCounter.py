

#import os
import re

#os.chdir('C:\\Users\\bnsnwg6\\Documents\\Python')
print('This program determines how many book shelves are required to display')
print('a set of books. The first line of the input describes available')
print('shelf widths. All following lines describe book widths and names.')
inputFileName=input('Enter the name of the file with shelves and books: ')

inputFile=open(inputFileName,'r')
inputDataList=inputFile.readlines()
getShelfSizes=re.compile(r'(\d*)\s')
shelfSizeList=getShelfSizes.findall(inputDataList[0])
getBookSizes=re.compile(r'^(\d*) (.*)$')
sizeOnly=[None]*(len(inputDataList)-1)
sizeBookPairs=[None]*(len(inputDataList)-1)
for i in range(1,len(inputDataList)):
    moBooks=getBookSizes.search(inputDataList[i])
    sizeOnly[i-1]=int(moBooks.group(1))
    sizeBookPairs[i-1]=moBooks.groups()
