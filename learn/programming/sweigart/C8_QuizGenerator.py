#! python3
# randomQuizGenerator.py - Creates quizzes with questions and answers in
# random order, along with the answer key.

import random
import os
os.chdir('F:\\Documents\\AutomateBoring\\C8_quiz')
# The quiz data. Keys are states and values are their capitals.

capitals = {'Alabama': 'Montgomery', 'Alaska': 'Juneau', 'Arizona': 'Phoenix',
            'Arkansas': 'Little Rock', 'California': 'Sacramento', 'Colorado':
            'Denver', 'Connecticut': 'Hartford', 'Delaware': 'Dover', 'Florida':
            'Tallahassee', 'Georgia': 'Atlanta', 'Hawaii': 'Honolulu', 'Idaho':
            'Boise', 'Illinois': 'Springfield', 'Indiana': 'Indianapolis',
            'Iowa': 'Des Moines', 'Kansas': 'Topeka', 'Kentucky': 'Frankfort',
            'Louisiana': 'Baton Rouge', 'Maine': 'Augusta', 'Maryland':
            'Annapolis', 'Massachusetts': 'Boston', 'Michigan': 'Lansing',
            'Minnesota': 'Saint Paul', 'Mississippi': 'Jackson', 'Missouri':
            'Jefferson City', 'Montana': 'Helena', 'Nebraska': 'Lincoln',
            'Nevada': 'Carson City', 'New Hampshire': 'Concord', 'New Jersey':
            'Trenton', 'New Mexico': 'Santa Fe', 'New York': 'Albany',
            'North Carolina': 'Raleigh', 'North Dakota': 'Bismarck', 'Ohio':
            'Columbus', 'Oklahoma': 'Oklahoma City', 'Oregon': 'Salem',
            'Pennsylvania': 'Harrisburg', 'Rhode Island': 'Providence',
            'South Carolina': 'Columbia', 'South Dakota': 'Pierre', 'Tennessee':
            'Nashville', 'Texas': 'Austin', 'Utah': 'Salt Lake City', 'Vermont':
            'Montpelier', 'Virginia': 'Richmond', 'Washington': 'Olympia',
            'West Virginia': 'Charleston', 'Wisconsin': 'Madison', 'Wyoming':
            'Cheyenne'}

# Generate 35 quiz files.
for quizNum in range(35):
    # TODO: Create the quiz and answer key files.
    quizWriter = open('stateQuiz'+str(quizNum)+'.txt','w')
    ansKeyWriter = open('answerQuiz'+str(quizNum)+'.txt','w')
    
    # TODO: Write out the header for the quiz.
    quizWriter.write("Name: \n\nDate: \n\nSection:\n\n")
    quizWriter.write("This is randomized state quiz number " + str(quizNum)+".\n")
    quizWriter.write("This quiz will test you on your knowledge of state capitols.\n")
    quizWriter.write("This quiz is unique from all the others, so don't cheat.\n\n")
    
    # TODO: Shuffle the order of the states.
    listPairs=list(capitals.items())
    N=len(listPairs)
    for i in range(N*2):
        x=random.randint(0,N-1)
        y=random.randint(0,N-1)
        if y!=x:
            listPairs[y],listPairs[x]=listPairs[x],listPairs[y]    
    
    # TODO: Loop through all 50 states, making a question for each.
    choices=5
    for j in range(N):
        quizWriter.write("\nQuestion "+str(j)+": What is the state capitol of "+(listPairs[j])[0]+"?\n")
        ansIndex=random.randint(0,choices-1)

        altAns=[None]*4
        l=0
        while l<4:
            newIndex=random.randint(0,N-1)
            if newIndex!=j & newIndex not in altAns:
                altAns[l]=newIndex
                l=l+1

        m=0
        for k in range(choices):
            if k==ansIndex:
                quizWriter.write("    "+str(k)+". "+(listPairs[j])[1]+"\n")
            else:
                quizWriter.write("    "+str(k)+". "+(listPairs[altAns[m]])[1]+"\n")
                m=m+1
        ansKeyWriter.write(str(j)+". "+str(ansIndex)+". "+(listPairs[j])[1]+" is the capitol of "+(listPairs[j])[0]+"\n")

    quizWriter.close()
    ansKeyWriter.close()
