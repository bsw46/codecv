# Following a pandas practice tutorial
# https://pandas.pydata.org/pandas-docs/stable/10min.html


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#### Object Creation

# Create a series (vector-like?)
series1 = pd.Series([1,2,3,4,np.nan,5,4,3])
print(series1)
print('')

# Series can contain mixed types.
series2 = pd.Series(['a',1,2,True])
print(series2)
print('')

# Create a dataframe
dates = pd.date_range('20180701',periods=6)
print(dates)
print('')

# Use the other dataframe dates as an index (index=dates).
# Returns error if index is wrong length (index = [1,2]
df = pd.DataFrame(np.random.randn(6,4), index=dates,columns = ['seven','409','qq','sp'])
print(df)
print('')

# Can turn a dictionary into a dataframe
example_dict = {'a':1,
                'B':pd.Timestamp('20130102'),
                'Cici':pd.Series(1,index=list(range(4)),dtype='float32'),
                'Dummy': pd.Categorical(["hot","cold","hot","cold"]),
                'E': 'foo',
                'arraynp': np.array([i for i in range(4)])}
df2 = pd.DataFrame(example_dict)
print(df2)
print('')

# dataframes can have different data types
print(df2.dtypes)
print('')

#### Viewing Data

print("print df.head() shows the first N (default = 5?) entries.")
print(df.head())
print('')

print("print df.tail(3) shows the last 3 entries.")
print(df.tail(3))
print('')

print("print df.index shows the index column")
print(df.index)
print('')

print("print df.columns shows the column labels")
print(df.columns)
print('')

print("print df.values shows the data entries")
print(df.values)
print('')

print("print df.describe shows a quick data summary")
print(df.describe())
print('')

# df.T to transpose data.

print(df.sort_index(axis=1,ascending = False)) #<- Sort columns by reverse ascii
print('')
print(df.sort_values(by = 'qq')) #<- Sort column entries of 'qq' by number
print('')

#### Selection

# To get a specific column (Series) from a dataframe:
print(df['qq'])
print('')

# Can Select specific rows using ron numbers or key?
print(df[1:4])
print('')
print(df['20180701':'20180705'])
print('')

# Select a row given key
print(df.loc[dates[0]])
print('')

# Select multiple columns
print(df.loc[:,['seven','qq']])
print('')

# Select by row and columns
print(df.loc['20180701':'20180705',['seven','qq']])
print('')
print(df.loc['20180702',['seven','qq']])
print('')
print(df.loc['20180702','qq'])
print('')

# Select row by index
print(df.iloc[3]) # Row 4, index 3.
print('')

# iloc can be used numpy style.
print(df.iloc[3:5,0:2])
print(df.iloc[[1,3,5],[0,2]])
print(df.iloc[1:3,:])
print('')

# Boolean Selection
print(df[df.qq>0]) # Retures df with elements of qq > 0
print(df[df>0])    # Returns df NaN or number
print(df2[df2['Dummy'].isin(['hot'])]) # Returns df where 'Dummy' elements are 'hot'
print('')

# Setting values
# Add a column to data frame
new_s = pd.Series([1,2,3,4,5,6],index=pd.date_range('20180703',periods=6))
df['new_col'] = new_s
print(df)
print('')
df.at[dates[0],'seven'] = 0 # Change based on column and label
df.iat[1,3] = 0 # Change element at index
df.loc[:,'409'] = np.array([5]*len(df))
print(df)
print('')

# Conditional setting 54
df[df>0] = -df
print(df)
print('')

#### Missing data
# Tends to be represented by np.NaN

# Get a subset of dataframe
df3 =df.reindex(index = dates[0:4],columns = list(df.columns)+['E'])
df3.iat[2,5] = 1
print(df3)

# Drop rows with mising data
print(df3.dropna(how='any'))

# Fill in missing data
print(df3.fillna(value=0))

# get boolean of NaN
print(pd.isna(df3))

#### Operations

# Get means
print(df.mean()) # by columns
print(df.mean(1)) # by rows

prime = pd.Series([np.nan,2,3,np.nan,5,np.nan],index = dates)

print(df.sub(prime,axis = 'index'))

# Use functions on dataframe
print(df)
print(df.apply(np.sin)-df.apply(np.cumsum))
print(df.apply(lambda x: x.max() - x.min()))

# Histogramming
hist = pd.Series(np.random.randint(0,7,size=10))
print(hist.value_counts())

# String Methods
stringy = pd.Series(['A','B','C','asdad', 'asgr', np.nan,'thse','ryftxh'])
print(stringy.str.lower())

#### Merge

#Concat
#Join
#Append

#### Grouping
df = pd.DataFrame({'A' : ['foo', 'bar', 'foo', 'bar',
                              'foo', 'bar', 'foo', 'foo'],
                       'B' : ['one', 'one', 'two', 'three',
                              'two', 'two', 'one', 'three'],
                       'C' : np.random.randn(8),
                       'D' : np.random.randn(8)})
print(df)
print(df.groupby('A').sum())
print(df.groupby(['A','B']).sum())

#### Reshaping

#Stack
#Pivot Tables

#### Time Series

#### Categoricals

#### Plotting

ts = pd.Series(np.random.randn(1000), index=pd.date_range('1/1/2000', periods=1000))
ts = ts.cumsum()
ts.plot() #matplotlib object
# plt.show()

tss = pd.DataFrame(np.random.randn(1000, 4), index=ts.index,
                   columns=['A', 'B', 'C', 'D'])
tss = tss.cumsum()
plt.figure();tss.plot();plt.legend(loc='best')
plt.show()

#### Reading and writing to files
df.to_csv('foo.csv')
pd.readcsv('foo.csv')
