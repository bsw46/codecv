# Following tutorials on https://www.tensorflow.org/tutorials/keras/basic_text_classification

# TensorFlow and tf.keras 
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt

print(tf.__version__)

### Import the data
imdb = keras.datasets.imdb
(train_data, train_labels), (test_data,test_labels) = imdb.load_data(num_words=10000)

### Explore the data
print("Training entries: {}, labels: {}".format(len(train_data), len(train_labels)))
print(train_data[0])

print("Lengths of movies 1 and 2: {} and {}".format(len(train_data[0]),len(train_data[1])))

# Get the dictionary to turn words to integer representation
word_index = imdb.get_word_index()


word_index = {k:(v+3) for k,v in word_index.items()}
word_index["<PAD>"] = 0
word_index["<START>"] = 1
word_index["<UNK>"] = 2  # unknown
word_index["<UNUSED>"] = 3

reverse_word_index = dict([(value,key) for (key,value) in word_index.items()])

def decode_review(text):
    return ' '.join([reverse_word_index.get(i,'?') for i in text])

print(decode_review(train_data[0]))

# There are two ways to deal with the inconsistent dimension size.
# The first is to make an n by m array where the n features are 0 or 1 depending on whether a word is present.
# The second is to keep every integer as is, but have zeros at the end of the

# The first method has a high memory requirement.
# The second method is called padding and is used.

### Prepare the data
train_data = keras.preprocessing.sequence.pad_sequences(train_data,
                                                        value=word_index["<PAD>"],
                                                        padding='post',
                                                        maxlen=256)

test_data = keras.preprocessing.sequence.pad_sequences(test_data,
                                                       value=word_index["<PAD>"],
                                                       padding='post',
                                                       maxlen=256)

print("Lengths of movies 1 and 2: {} and {}".format(len(train_data[0]),len(train_data[1])))

print(train_data[0])

### Build the model
# input shape is the vocabulary count used for the movie reviews (10,000 words)
vocab_size = 10000

model = keras.Sequential()
model.add(keras.layers.Embedding(vocab_size, 16))
# https://machinelearningmastery.com/use-word-embedding-layers-deep-learning-keras/
# Embedding projects words into a dense vector space. 
# The position of the word in the vector space is learned from text and based on words which surround the word when used.
# Similar words may appear in similar places based on their meaning?
model.add(keras.layers.GlobalAveragePooling1D())
# This layer seems to just change the dimension
model.add(keras.layers.Dense(16, activation=tf.nn.relu))
model.add(keras.layers.Dense(1, activation=tf.nn.sigmoid))

model.summary()

# Read about the AdamOptimizer
model.compile(optimizer=tf.train.AdamOptimizer(),
              loss='binary_crossentropy',
              metrics=['accuracy'])

### Validation data set
x_val = train_data[:10000] # First 10000 are valid
partial_x_train = train_data[10000:]

y_val = train_labels[:10000]
partial_y_train = train_labels[10000:]

### Train the model
history = model.fit(partial_x_train,
                    partial_y_train,
                    epochs=40,
                    batch_size=512,
                    validation_data=(x_val, y_val),
                    verbose=1)

### Evaluation
results = model.evaluate(test_data, test_labels)
print(results)

### Create a graph of accuracy and loss over time
history_dict = history.history
print(history_dict.keys())

acc = history.history['acc']
val_acc = history.history['val_acc']
loss = history.history['loss']
val_loss = history.history['val_loss']

epochs = range(1, len(acc) + 1)

# "bo" is for "blue dot"
plt.plot(epochs, loss, 'bo', label='Training loss')
# b is for "solid blue line"
plt.plot(epochs, val_loss, 'b', label='Validation loss')
plt.title('Training and validation loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()

plt.show()

plt.clf()   # clear figure
acc_values = history_dict['acc']
val_acc_values = history_dict['val_acc']

plt.plot(epochs, acc, 'bo', label='Training acc')
plt.plot(epochs, val_acc, 'b', label='Validation acc')
plt.title('Training and validation accuracy')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()

plt.show()

# Plots displays overfitting.
# Tutorial recommends early stop to prevent overfitting in this case. 

