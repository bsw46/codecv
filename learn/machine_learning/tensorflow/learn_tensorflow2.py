# Following a tutorial on tensorflow
# https://learningtensorflow.com/

##### Chapter 2 - Variables #####

import tensorflow as tf
import numpy as np

# The python version
x = 35
y = x+5
print(y)

# The tensorflow equivalent
x = tf.constant(35,name='x')  # Constant value is given value 35
y = tf.Variable(x+5,name='y') # y is the equation x+5
# y is not evaluated. when the variable y is computed, take the value of x and add 5
# print(x)
# print(y)

# To actually evaluate these variables:
model = tf.global_variables_initializer()
# Creates a graph of the dependencies between variables


with tf.Session() as session: # Create a session for computing the values
    session.run(model)        # Run the model created earlier
    print(session.run(y))     # Run the variable y and print out
# The previous steps determined the dependencies of the different variables to one another.
# The last step actually calculates y.


# Exercise 1 - a constant array
x = tf.constant([35,40,45],name='x')  
y = tf.Variable(x+5,name='y')

model = tf.global_variables_initializer()

with tf.Session() as session:
    session.run(model)       
    print(session.run(y))    
# This prints out the array [40 45 50]


# Exercise 2 - 
data = np.random.normal(size=10000)
x = tf.constant(data,name='x')  
y = tf.Variable(5*x*x-3*x+15,name='y')

model = tf.global_variables_initializer()

with tf.Session() as session:
    session.run(model)       
    print(session.run(y))    


# Exercise 3 - 
x = tf.Variable(0, name='x')

model = tf.global_variables_initializer()

with tf.Session() as session:
    session.run(model)
    for i in range(5):
        x = x + 1
        print(session.run(x))
        

# Exercise 4 - 
cumulative_sum = tf.Variable(0, name='cumulative_sum')
sum_count = tf.Variable(0, name='sum_count')

model = tf.global_variables_initializer()

with tf.Session() as session:
    session.run(model)
    for i in range(12):
        cumulative_sum = cumulative_sum + np.random.randint(1000)
        sum_count = sum_count + 1
    print(session.run(cumulative_sum)/session.run(sum_count))


# Exercise 5
x = tf.constant(35, name='x')
print(x)
y = tf.Variable(x + 5, name='y')

with tf.Session() as session:
    merged = tf.summary.merge_all()
    writer = tf.summary.FileWriter("/tmp/basic", session.graph)
    model =  tf.global_variables_initializer()
    session.run(model)
    print(session.run(y))
