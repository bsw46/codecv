import tensorflow as tf
x1 = tf.constant(5)
x2 = tf.constant(6)
result = tf.multiply(x1,x2)
print(result)

with tf.Session() as session:
    output = session.run(result)
    print(output)
# output is a python variable. but result is part of the graph. 
print(output)
