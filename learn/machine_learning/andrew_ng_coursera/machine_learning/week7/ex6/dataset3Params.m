function [C, sigma] = dataset3Params(X, y, Xval, yval)
%DATASET3PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = DATASET3PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example, 
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using 
%        mean(double(predictions ~= yval))
%
parameter = [.01 .03 .1 .3 1 3 10 30];
min_performance = inf;
best_C = 0;
best_sigma = 0;
cost = 0;

% Loop for C
for i = 1:8,
    % Loop for sigma
    for j = 1:8,
        model= svmTrain(X, y, parameter(i), @(x1, x2) gaussianKernel(x1, x2, parameter(j)));
        predictions = svmPredict(model,Xval);
        performance = mean(double(predictions ~=yval));
        if performance < min_performance,
            min_performance = performance;
            best_C = C;
            best_sigma = sigma;
        end
    end
end 



C = best_C;
sigma = best_sigma;
% =========================================================================

end
