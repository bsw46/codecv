import os
import re
import numpy as np
import matplotlib.pyplot as plt

os.chdir('./datasets')
data_stream = open('port_data.csv','r')

data=data_stream.read()

index_regex = re.compile(r'(\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+)')
                         #year    #SP500    #EAFE     #R2000
match_object = index_regex.findall(data)

data_array = np.array(match_object).astype('float')


annual_contribution = 100
account_value = 0
max_retirement = -1
growth = 0
optimal_ratio = None
res = 1000

for i in range(res):
    for j in range(res-i):
        account_value = annual_contribution
        for l in range(data_array.shape[0]-1):
            growth = np.dot(np.divide(data_array[l+1][1:],data_array[l][1:]),np.array([i,j,res-i-j]))/res
            account_value = growth* account_value+annual_contribution
        if account_value>max_retirement:
            max_retirement = account_value
            optimal_ratio = np.array([i,j,res-i-j])
print(max_retirement)
print(optimal_ratio)
