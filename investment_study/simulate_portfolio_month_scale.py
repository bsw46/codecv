import quandl
import matplotlib.pyplot as plt
import numpy as np
import math
quandl.ApiConfig.api_key = "1T_wqUo4FBGts7S56NBf"
mydata = quandl.get("MULTPL/SP500_REAL_PRICE_MONTH", returns="numpy",
                    start_date="1918-01-01",end_date="")

def bisection_search(sorted_list,target_value,min_index,max_index):
    midpoint = (min_index + max_index)//2
    if (abs(sorted_list[midpoint]-target_value)<1/24):
        return midpoint
    elif sorted_list[midpoint]>target_value:
        return bisection_search(sorted_list,target_value,min_index,midpoint)
    else:
        return bisection_search(sorted_list,target_value,midpoint,max_index)  

time = []
value = []
value_log = []

for i in range(len(mydata)):
    time = time + [mydata[i][0].year+1/12*(mydata[i][0].month-1)]
    value = value + [mydata[i][1]]

time = np.array(time)
value = np.array(value)
value_log = np.log(value)
percent_change = (value[1:]-value[:(len(value)-1)])/value[:(len(value)-1)]


initial_years = np.arange(1918,1977)
end_value = np.zeros(np.arange(1918,1977).shape)
simulation_years = 40
annual_contribution = 5500
monthly_buy = 5500/12

print("Strategy A: Buy the index in the beginning of the year.")
# The worst case for this strategy occurs when there is a crash right before retirement.
# If the strategy initiates in 1968, then the crash would occur in 2008.
# In the worst case, the account has a million dollars in it. 
for initial_year in initial_years:
    initial_index = bisection_search(time,initial_year,0,len(time))    
    cash = 0
    shares = 0
    for i in range(simulation_years*12):
        if (i%12 == 0):
            shares += annual_contribution/value[initial_index+i]
    end_value[initial_year-1918]=shares*value[initial_index+i]

plt.plot(initial_years,end_value/1000)
plt.xlabel("Start time (year)")
plt.ylabel("Value after "+str(simulation_years)+" years ($ thousands)")
plt.show()        
plt.hist(end_value/1000,bins=10)
plt.show()

print("Strategy B: Spread out the investments throughout the year.")
for initial_year in initial_years:
    initial_index = bisection_search(time,initial_year,0,len(time))    
    cash = 0
    shares = 0
    for i in range(simulation_years*12):
        shares += monthly_buy/value[initial_index+i]
    end_value[initial_year-1918]=shares*value[initial_index+i]

plt.plot(initial_years,end_value/1000)
plt.xlabel("Start time (year)")
plt.ylabel("Value after "+str(simulation_years)+" years ($)")
plt.show()        
