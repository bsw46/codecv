import os
import re
import numpy as np
import matplotlib.pyplot as plt

def bisection_search(sorted_list,target_value,min_index,max_index):
    midpoint = (min_index + max_index)//2
    if (abs(sorted_list[midpoint]-target_value)<1/200):
        return midpoint
    elif sorted_list[midpoint]>target_value:
        return bisection_search(sorted_list,target_value,min_index,midpoint)
    else:
        return bisection_search(sorted_list,target_value,midpoint,max_index)  

os.chdir('./datasets')
SP500_stream = open('SP500.csv','r')

data=SP500_stream.read()

index_regex = re.compile(r'(\d+)-(\d+)-(\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+),(\d+)')
                         #year   month day   open      high      low       close     adj        vol        
match_object = index_regex.findall(data)

SP500_low = []
SP500_time = []
   
    
SP500_array = np.array(match_object).transpose().astype(float)
SP500_low = np.array(SP500_array[5][:])
SP500_open = np.array(SP500_array[3][:])
SP500_time = SP500_array[0][:] + SP500_array[1][:]*(1.0/12.0) + SP500_array[2][:]*(1.0/365.0)
data_points = len(SP500_time)

annual_contribution = 25000
simulation_years = 40
start = 1951
end_time = 2019
initial_years = np.arange(start,end_time-simulation_years)

print("Benchmarks based on fixed growths.")
print("Investing a total of "+str(simulation_years*annual_contribution)+" over "+ str(simulation_years) +" years.")
for i in range(1,12):
    interest = i/100.
    end_bal=annual_contribution*(1+interest)**(simulation_years-1)/(1-1./(1+interest))*(1-(1./(1+interest)**simulation_years))
    print('Interest rate: '+str(interest).rjust(4)+', Final Bal: '+str(int(end_bal)).rjust(10))
print('')

print("Comparing historical performances versus time")
t = np.arange(0,40,.1) # short for time range
plt.plot(t,1.08**t,'k-',t,1.07**t,'b-',t,1.09**t,'b-')
plt.show()


end_value_A = np.zeros(initial_years.shape)
print("Strategy A: Buy the index in the first day of the year.")
# Simple strategy. Good baseline. Buy as early as possible.
for initial_year in initial_years:
    initial_index = bisection_search(SP500_time,initial_year,0,data_points)
    end_index = bisection_search(SP500_time,initial_year+simulation_years,0,data_points)
    cash = 0
    shares = 0
    temp_initial_year = initial_year + 0
    for i in range(initial_index,end_index):
        if (SP500_time[i] > temp_initial_year):
            shares += annual_contribution/SP500_open[i]
            temp_initial_year += 1
    end_value_A[initial_year-initial_years[0]] = shares*SP500_open[end_index]
    print(str(initial_year)+' - '+str(initial_year+40)+' : '+str(int(end_value_A[initial_year-initial_years[0]])).rjust(10))

plt.hist(end_value_A/1000,bins=15)
plt.xlabel("Value of account after 40 years ($ thousand)")
print("The average value of the account is "+str(np.mean(end_value_A)))
print("The median value of the account is "+str(np.median(end_value_A)))
print("The standard deviation of account value is "+str(np.std(end_value_A)))
print("")
plt.show()

end_value_B = np.zeros(initial_years.shape)        


print("Strategy B: Buy the index throughout the year.")
# Results in a slightly lower outcome, but also a lower deviation.
# Overall result is consistently lower.

for initial_year in initial_years:
    initial_index = bisection_search(SP500_time,initial_year,0,data_points)
    end_index = bisection_search(SP500_time,initial_year+simulation_years,0,data_points)
    cash = 0
    shares = 0
    purchases = 24.0
    temp_initial_year = initial_year + 0
    for i in range(initial_index,end_index):
        if (SP500_time[i] > temp_initial_year):
            shares += annual_contribution/purchases/SP500_open[i]
            temp_initial_year += 1/purchases
    end_value_B[initial_year-initial_years[0]] = shares*SP500_open[end_index]
    print(str(initial_year)+' - '+str(initial_year+40)+' : '+str(int(end_value_B[initial_year-initial_years[0]])).rjust(10))

print("The average value of the account is "+str(np.mean(end_value_B)))
print("The median value of the account is "+str(np.median(end_value_B)))
print("The standard deviation of account value is "+str(np.std(end_value_B)))
print("")
plt.hist(end_value_B/1000,bins=15)
plt.xlabel("Value of account after 40 years ($ thousand)")
plt.show()

average_difference_AB = 0
print("Difference in strategy A and B")

for initial_year in initial_years:
    print(str(initial_year)+' - '+str(initial_year+40)+' : '+str(int(end_value_A[initial_year-initial_years[0]])-int(end_value_B[initial_year-initial_years[0]])).rjust(10))
    average_difference_AB += int(end_value_A[initial_year-initial_years[0]])-int(end_value_B[initial_year-initial_years[0]])
print("The average difference of the accounts is "+str(np.mean(end_value_B-end_value_A)))
print("The median value of the account is "+str(np.median(end_value_B-end_value_A)))
print("The standard deviation of account value is "+str(np.std(end_value_B-end_value_A)))

plt.hist((end_value_B-end_value_A)/1000,bins=15)
plt.xlabel("Difference of account after 40 years ($ thousand)")
plt.show()

print("")

end_value_C = np.zeros(initial_years.shape)


##print("Strategy C: Buy limit index in the beginning of the year.")
### Buy limit strategy.
### This simulation does not yet work as intended. 
### May not be advantageous.
##for initial_year in initial_years:
##    initial_index = bisection_search(SP500_time,initial_year,0,data_points)
##    end_index = bisection_search(SP500_time,initial_year+simulation_years,0,data_points)
##    cash = 0
##    shares = 0
##    purchases = 1.0
##    buy_price = 0
##    days_past =0
##    temp_initial_year = initial_year + 0
##    for i in range(initial_index,end_index):
##        if (SP500_time[i] > temp_initial_year):
##            # Set buy price as a function of the current price.
##            buy_price = SP500_open[i]
##            days_past = 0
##
##        if (buy_price >= SP500_low[i+1]):
##            shares += annual_contribution/buy_price
##            buy_price = 0
##            temp_initial_year += 1
##            
##        if days_past == 60:
##            shares += annual_contribution/SP500_open[i]
##            buy_price = 0
##            temp_initial_year += 1
##        days_past += 1
##    end_value_C[initial_year-initial_years[0]] = shares*SP500_open[end_index]
##
##plt.hist(end_value_C/1000,bins=10)
##plt.xlabel("Value of account after 40 years ($ thousand)")
##print("The average value of the account is "+str(np.mean(end_value_C)))
##print("The median value of the account is "+str(np.median(end_value_C)))
##print("The standard deviation of account value is "+str(np.std(end_value_C)))
##print("")
##plt.show()

##print("Strategy D: Buy index in the first month of the year.")
##end_value_D = np.zeros(initial_years.shape)
##for initial_year in initial_years:
##    initial_index = bisection_search(SP500_time,initial_year,0,data_points)
##    end_index = bisection_search(SP500_time,initial_year+simulation_years,0,data_points)
##    cash = 0
##    shares = 0
##    purchases = 1.0
##    temp_initial_year = initial_year + 0
##    for i in range(initial_index,end_index):
##        if (SP500_time[i] > temp_initial_year):
##            # Set buy price as a function of the current price.
##            buy_price = SP500_open[i]
##            days_past = 0
##
##        if (buy_price >= SP500_low[i]):
##            shares += annual_contribution/buy_price
##            buy_price = 0
##            temp_initial_year += 1
##            
##        if days_past == 60:
##            shares += annual_contribution/SP500_open[i]
##            buy_price = 0
##            temp_initial_year += 1
##        days_past += 1
##    end_value_D[initial_year-initial_years[0]] = shares*SP500_open[end_index]
##print("")

print("Strategy E: Buy at minimum price each year.")
# Requires future prediction.
# A benchmark for future prediction performance 
end_value_E = np.zeros(initial_years.shape)
for initial_year in initial_years:
    initial_index = bisection_search(SP500_time,initial_year,0,data_points)
    end_index = bisection_search(SP500_time,initial_year+simulation_years,0,data_points)
    cash = 0
    shares = 0
    purchases = 1.0
    temp_initial_year = initial_year + 0
    for i in range(initial_index,end_index,260):
        shares += annual_contribution/min(SP500_low[i:i+260])
    end_value_E[initial_year-initial_years[0]] = shares*SP500_open[end_index]
    print(str(initial_year)+' - '+str(initial_year+40)+' : '+str(int(end_value_E[initial_year-initial_years[0]])).rjust(10))
plt.hist(end_value_E/1000,bins=15)
plt.xlabel("Value of account after 40 years ($ thousand)")
print("The average value of the account is "+str(np.mean(end_value_E)))
print("The median value of the account is "+str(np.median(end_value_E)))
print("The standard deviation of account value is "+str(np.std(end_value_E)))
plt.show()
print("")
average_difference_EA = 0
for initial_year in initial_years:
    print(str(initial_year)+' - '+str(initial_year+40)+' : '+str(int(end_value_E[initial_year-initial_years[0]])-int(end_value_A[initial_year-initial_years[0]])).rjust(10))
    average_difference_EA += int(end_value_E[initial_year-initial_years[0]])-int(end_value_A[initial_year-initial_years[0]])
print("The average difference of the accounts is "+str(np.mean(end_value_E-end_value_A)))
print("The median value of the account is "+str(np.median(end_value_E-end_value_A)))
print("The standard deviation of account value is "+str(np.std(end_value_E-end_value_A)))
print("")



##print("Strategy F: Buy at minimum price from today.")
### Requires future prediction.
### A benchmark for future prediction performance 
##end_value_F = np.zeros(initial_years.shape)
##for initial_year in initial_years:
##    initial_index = bisection_search(SP500_time,initial_year,0,data_points)
##    end_index = bisection_search(SP500_time,initial_year+simulation_years,0,data_points)
##    cash = 0
##    shares = 0
##    purchases = 1.0
##    temp_initial_year = initial_year + 0
##    for i in range(initial_index,end_index,260):
##        shares += annual_contribution/min(SP500_low[i:end_index])
##    end_value_F[initial_year-initial_years[0]] = shares*SP500_open[end_index]
##
##plt.hist(end_value_F/1000,bins=10)
##plt.xlabel("Value of account after 40 years ($ thousand)")
##print("The average value of the account is "+str(np.mean(end_value_F)))
##print("The median value of the account is "+str(np.median(end_value_F)))
##print("The standard deviation of account value is "+str(np.std(end_value_F)))
##plt.show()
##print("")
