# Looks at the trajectories of an S&P500 portfolio. 
# Assumes growth percentage falls into a normal distribution month to month.

import matplotlib.pyplot as plt
import numpy as np
import time
import random

# Change values calculated on a per month basis.
# Major result from characterize_snp500.py
PERCENT_CHANGE_MEAN = 0.006647584320306998
PERCENT_CHANGE_SD = 0.03448721498404183

class account(object):
    """This class will help track of an account's value. """
    def __init__(self, initial_value, proportions = {'Uninvested':[1,0,0]}, simulations=10000, initial_year = 2018, steps=800):
        self.sims=simulations
        self.value = np.zeros((simulations,steps))
        self.value[:,0] = initial_value
        self.ticker = 1
        self.time = np.zeros(steps)
        self.time[0]=initial_year
        self.proportions = proportions
            
        return None
        
    def grow_by_month(self):#,mean_change = PERCENT_CHANGE_MEAN,
                         #standard_deviation_change = PERCENT_CHANGE_SD):
        for i in range(self.sims):
            x=0
            for v in self.proportions.values():
                x = x+random.gauss(v[1],v[2])*v[0]
            self.value[i,self.ticker] = self.value[i,self.ticker-1]*(1+x)
        self.time[self.ticker] = self.time[self.ticker-1] + + 1.0/12.0
        self.value[:,self.ticker] = np.sort(self.value[:,self.ticker])
        self.ticker+=1
        return None
        
    def invest_amount(self, amount_added):
        self.value[:,self.ticker] = self.value[:,self.ticker-1]+ amount_added
        self.time[self.ticker] = self.time[self.ticker-1]
        self.ticker+=1
        return None

    def display_all_trajectory(self):
        plt.plot(self.time[0:self.ticker-2],self.value[:,0:self.ticker-2].transpose())
        plt.xlabel("Time (year)")
        plt.ylabel("IRA account value ($)")
        plt.show()
        
    def display_some_trajectory(self):
        plt.plot(self.time[0:self.ticker-1],self.value[::10,0:self.ticker-1].transpose())
        plt.xlabel("Time (year)")
        plt.ylabel("IRA account value")
        plt.show()

    def display_average_trajectory(self):
        plt.plot(self.time[0:self.ticker-1],self.value[int(self.sims/2),0:self.ticker-1].transpose())
        plt.xlabel("Time (year)")
        plt.ylabel("IRA account value")
        plt.show()

    def value_IQR(self,time=None):
        if time == None:
            index = self.ticker-1
        else:
            # Find the time indicated.
            # This would be more efficient with a bisection search.
            for i in range(self.ticker):
                if time > self.time[i]:
                    index = i
        plt.plot(self.time[0:index-1],self.value[
            [int(self.sims/4.0),int(self.sims/2.0), int(self.sims*3.0/4.0)]
            ,0:index-1].transpose()/1000)
        print(self.value[int(self.sims/4.0),index-1])
        print(self.value[int(self.sims/2.0),index-1])
        print(self.value[int(self.sims*3.0/4.0),index-1])
        plt.xlabel("Time (year)")
        plt.ylabel("IRA account value ($ thousand)")
        plt.show()
        

if __name__ == "__main__":
    proportions = { # name : [proportion, growth_mean, growth_SD] 
        'SP500':[.5,0.006647584320306998,0.03448721498404183],
        'USBOND':[.1,1.04**(1.0/12.0)-1,0],
        'NASDAQ100':[.4,0.009510520466276175,0.06090536048882875]}
    myIRA = account(1,proportions)
    retire_age = 40
    current_age = 24
    annual_contribution = 5500+54500
    for i in range(retire_age-current_age):
        print("Beginning of year "+str(i))
        myIRA.invest_amount(annual_contribution)
        for j in range(12):
            myIRA.grow_by_month()
        print("End of year "+str(i))
    myIRA.value_IQR()
    plt.hist(myIRA.value[:,myIRA.ticker-1],bins=50)
    plt.xlim((0,10000000))
    plt.show()
