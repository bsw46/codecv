import os
import re
import numpy as np
import matplotlib.pyplot as plt

os.chdir('./datasets')
SP500_stream = open('SP500.csv','r')

data=SP500_stream.read()

index_regex = re.compile(r'(\d+)-(\d+)-(\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+),(\d+)')
                         #year   month day   open      high      low       close     adj        vol        
match_object = index_regex.findall(data)

SP500_low = []
SP500_time = []
   
    
SP500_array = np.array(match_object).transpose().astype(float)
SP500_low = np.array(SP500_array[5][:])
SP500_open = np.array(SP500_array[3][:])
SP500_time = SP500_array[0][:] + SP500_array[1][:]*(1.0/12.0) + SP500_array[2][:]*(1.0/365.0)

future_range=60
past_range = 30
intervals = (len(SP500_low)//future_range)
data_points = len(SP500_open)
minimum_two_months = [1.0]*(data_points-past_range)
minimum_future = [1.0]*(data_points-past_range)
count_max = 0

# Determine the minimum price 
for i in range(data_points-future_range):
    
    for j in range(future_range):
        if minimum_two_months[i] > SP500_low[i+j]/SP500_open[i]:
            minimum_two_months[i] = SP500_low[i+j]/SP500_open[i]

    if minimum_two_months[i]==1:
        minimum_two_months[i] = SP500_open[i+j]/SP500_open[i]
        count_max +=1

minimum_two_months = np.sort(minimum_two_months)
cumulative_dist = np.arange(0,1,1/len(minimum_two_months))

print("The median percent of initial cost is " +str(np.median(minimum_two_months)))
print("The 25th percentile of initial cost is " +str(minimum_two_months[int((data_points-future_range)/4)]))
print("The 75th percentile of initial cost is " +str(minimum_two_months[int((data_points-future_range)*3/4)]))
print("The mean percent of initial cost is " +str(np.mean(minimum_two_months)))
print("The standard deviation of initial cost is " +str(np.std(minimum_two_months)))

plt.hist(minimum_two_months,bins=400)
plt.ylabel("frequency")
plt.xlabel("Percent of initial cost")
plt.show()

plt.plot(minimum_two_months,cumulative_dist)
plt.ylabel("percentile")
plt.xlabel("Percent of initial cost")
plt.show()

for i in range(data_points-future_range):
    minimum_future[i] = np.min(SP500_low[i:]/SP500_open[i])

minimum_future = np.sort(minimum_future)
cumulative_dist_future = np.arange(0,1,1/len(minimum_future))

print("The median percent of initial cost is " +str(np.median(minimum_future)))
print("The mean percent of initial cost is " +str(np.mean(minimum_future)))
print("The standard deviation of initial cost is " +str(np.std(minimum_future)))

plt.hist(minimum_future,bins=400)
plt.ylabel("frequency")
plt.xlabel("Percent of initial cost")
plt.show()

plt.plot(minimum_future,cumulative_dist_future)
plt.ylabel("percentile")
plt.xlabel("Percent of initial cost")
plt.show()


meta_data = np.ones((5,data_points-future_range-past_range))
# index 0 or 1 used for normalization.
# index 0 - current price
# index 1 - average price over last 60 days
# index 2 - standard deviation over last 60 days
# index 3 - minimum over last 60 days
# index 4 - minimum over next 60 days

for i in range(0,data_points-future_range-past_range):
    meta_data[0][i] = SP500_open[i+past_range]
    meta_data[1][i] = np.mean(SP500_open[i:i+past_range])
    meta_data[2][i] = np.std(SP500_open[i:i+past_range])
    meta_data[3][i] = np.amin(SP500_open[i:i+past_range])
    
    
    for j in range(future_range):
        if meta_data[4][i] > SP500_low[i+j+past_range]:
            meta_data[4][i] = SP500_low[i+j+past_range]
    if meta_data[4][i]==1:
        meta_data[4][i] = SP500_open[i+j+past_range]

plt.plot((meta_data[3][:]-meta_data[1][:])/meta_data[2][:],(meta_data[4][:]-meta_data[1][:])/meta_data[2][:],'b.')
#plt.plot([-.5,.5],[-.5,.5],'r')
#plt.xlim((-.5,.5))
plt.ylabel("Two month  normalized minimum price")
plt.xlabel("Normalized current price")

plt.show()
