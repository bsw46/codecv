import os
import re
import numpy as np
import matplotlib.pyplot as plt

def bisection_search(sorted_list,target_value,min_index,max_index):
    midpoint = (min_index + max_index)//2
    if (abs(sorted_list[midpoint]-target_value)<1/200):
        return midpoint
    elif sorted_list[midpoint]>target_value:
        return bisection_search(sorted_list,target_value,min_index,midpoint)
    else:
        return bisection_search(sorted_list,target_value,midpoint,max_index)  

os.chdir('./datasets')
dataset = 'NASDAQ.csv'
print(dataset)
nasdaq_stream = open(dataset,'r')

data=nasdaq_stream.read()

index_regex = re.compile(r'(\d+)-(\d+)-(\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+),(\d+)')
                         #year   month day   open      high      low       close     adj        vol        
match_object = index_regex.findall(data)

nasdaq_low = []
nasdaq_time = []
   
    
nasdaq_array = np.array(match_object).transpose().astype(float)
nasdaq_low = np.array(nasdaq_array[5][:])
nasdaq_open = np.array(nasdaq_array[3][:])
nasdaq_time = nasdaq_array[0][:] + nasdaq_array[1][:]*(1.0/12.0) + nasdaq_array[2][:]*(1.0/365.0)
data_points = len(nasdaq_time)

time = int(nasdaq_time[0])+1
annual_growth = []
initial_index= bisection_search(nasdaq_time,time,0,len(nasdaq_time))
last_price = nasdaq_open[initial_index]
i=0

while time<2018:
    if nasdaq_time[i]>time+1.0/12.0:
        time+=1.0/12.0
        annual_growth = annual_growth + [(nasdaq_open[i]-last_price)/last_price]
        last_price = nasdaq_open[i]
        
    i +=1
annual_growth=np.sort(annual_growth)
print("Since the year "+str(int(nasdaq_time[0])))
print("The mean monthly growth is "+str(np.mean(annual_growth)))
print("The standard deviation of monthly growth is "+str(np.std(annual_growth)))
print("The median monthly growth is "+str(np.median(annual_growth)))
print("The 25th monthly growth is "+str(annual_growth[int(len(annual_growth)*1.0/4.0)]))
print("The 75th monthly growth is "+str(annual_growth[int(len(annual_growth)*3.0/4.0)]))

plt.hist(annual_growth,bins=30)
plt.xlabel("Monthly change")
plt.show()
