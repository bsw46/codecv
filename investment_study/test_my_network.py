import my_network as mn
import numpy as np
import os
import re
import matplotlib.pyplot as plt

test_net = mn.Network([3,5,7])
print(test_net.weights[0])
print('')
print(test_net.weights[1])
print('')
print(test_net.biases[0])
print('')
print(test_net.biases[1])
print('')

test_input = np.array([[1,2],[3,4],[5,6]])
test_input2 = np.array([[1],[2],[3]])
print(test_input)
print('')
print(test_input.shape)
print('')
print(test_input2)
print('')
print(test_input2.shape)
print('')

print(test_net.feedforward(test_input))
print('')
print(test_net.feedforward(test_input2))
print('')

test_input3 = [ (np.array([[1],[2],[3]]),1),(np.array([[1],[2],[3]]),1)]
test_net.SGD(test_input3,10,1,1)

def bisection_search(sorted_list,target_value,min_index,max_index):
    midpoint = (min_index + max_index)//2
    if (abs(sorted_list[midpoint]-target_value)<1/200):
        return midpoint
    elif sorted_list[midpoint]>target_value:
        return bisection_search(sorted_list,target_value,min_index,midpoint)
    else:
        return bisection_search(sorted_list,target_value,midpoint,max_index)  


os.chdir('./datasets')
SP500_stream = open('SP500.csv','r')

data=SP500_stream.read()

index_regex = re.compile(r'(\d+)-(\d+)-(\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+),(\d+)')
                         #year   month day   open      high      low       close     adj        vol        
match_object = index_regex.findall(data)

SP500_low = []
SP500_time = []   

SP500_array = np.array(match_object).transpose().astype(float)
SP500_low = np.array(SP500_array[5][:])
SP500_open = np.array(SP500_array[3][:])
SP500_time = SP500_array[0][:] + SP500_array[1][:]*(1.0/12.0) + SP500_array[2][:]*(1.0/365.0)
data_points = len(SP500_time)

training_data =[]
testing_data = []
for i in range(60,data_points-6000,60):
    input_layer = SP500_open[i-60:i].reshape((60,1))/np.mean(SP500_open[i-60:i])-.5
    two_month_low = np.min(SP500_low[i:i+60])/np.mean(SP500_open[i-60:i])
    if two_month_low < .934:
        output_layer = np.array([[1],[0],[0],[0]])
    elif  two_month_low < .965:
        output_layer = np.array([[0],[1],[0],[0]])
    elif  two_month_low < .986:
        output_layer = np.array([[0],[0],[1],[0]])
    else:
        output_layer = np.array([[0],[0],[0],[1]])
    training_data = training_data +[(input_layer,output_layer)]

for i in range(data_points-6000,data_points,60):
    input_layer = SP500_open[i-60:i].reshape((60,1))/np.mean(SP500_open[i-60:i])-.5
    two_month_low = np.min(SP500_low[i:i+60])/np.mean(SP500_open[i-60:i])
    if two_month_low < .934:
        output_layer = np.array([[1],[0],[0],[0]])
    elif  two_month_low < .965:
        output_layer = np.array([[0],[1],[0],[0]])
    elif  two_month_low < .986:
        output_layer = np.array([[0],[0],[1],[0]])
    else:
        output_layer = np.array([[0],[0],[0],[1]])
    testing_data = testing_data +[(input_layer,output_layer)]


predictor = mn.Network([60,4])
answer = predictor.feedforward(training_data[0][0])
print(predictor.feedforward(training_data[0][0]))

predictor.SGD(training_data, 400,30,1,testing_data)

start = 1951
initial_years = np.arange(start,1977)
simulation_years = 40
annual_contribution = 5500
print("Strategy G: Use neural net to predict price.")
# Requires future prediction.
# A benchmark for future prediction performance 
end_value_G = np.zeros(initial_years.shape)
for initial_year in initial_years:
    initial_index = bisection_search(SP500_time,initial_year,0,data_points)
    end_index = bisection_search(SP500_time,initial_year+simulation_years,0,data_points)
    cash = 0
    shares = 0
    purchases = 1.0
    temp_initial_year = initial_year + 0
    buy_bias = 0
    # Every day
    for i in range(initial_index,end_index):
        # at start of year
        if (SP500_time[i] > temp_initial_year):
            # Predictor says price will go low
            prediction = np.argmax(predictor.feedforward(np.array(SP500_open[i-60:i].reshape((60,1)))))
            #It won't buy now
            if prediction==3:
                shares+= annual_contribution/SP500_open[i]
            #It will drop, set buy limit
            else:
                # The predicted price
                if prediction == 2:
                    buy_price = (.986+buy_bias)*np.mean(SP500_open[i-60:i])
                elif prediction == 1:
                    buy_price = (.965+buy_bias)*np.mean(SP500_open[i-60:i])
                else:
                    buy_price = (.934+buy_bias)*np.mean(SP500_open[i-60:i])
                # Does it work?
                if buy_price > np.min(SP500_low[i:i+60]):
                    shares+= annual_contribution/buy_price
                else:
                    shares+= annual_contribution/SP500_open[i+60]
            temp_initial_year += 1
    end_value_G[initial_year-initial_years[0]] = shares*SP500_open[end_index]

plt.hist(end_value_G/1000,bins=10)
plt.xlabel("Value of account after 40 years ($ thousand)")
print("The average value of the account is "+str(np.mean(end_value_G)))
print("The median value of the account is "+str(np.median(end_value_G)))
print("The standard deviation of account value is "+str(np.std(end_value_G)))
plt.show()
print("")
