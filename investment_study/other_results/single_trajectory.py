# Single trajectory for simulating profile. 
# Not as strong as ensemble predictions.

import matplotlib.pyplot as plt
import numpy as np
import time
import random

# Change values calculated on a per month basis.
# Major result from characterize_snp500.py
PERCENT_CHANGE_MEAN = 0.006647584320306998
PERCENT_CHANGE_SD = 0.03448721498404183

class account(object):
    """This class will help track of an account's value. """
    def __init__(self, initial_value, initial_year = 2018):
        self.value = [initial_value]
        self.time = [initial_year]
        return None
        
    def grow_by_month(self, mean_change = PERCENT_CHANGE_MEAN,
                         standard_deviation_change = PERCENT_CHANGE_SD):
        x = random.gauss(mean_change,standard_deviation_change)

        self.value = self.value + [(x+1)*self.value[len(self.value)-1]]
        self.time = self.time + [self.time[len(self.time)-1] + 1.0/12.0]
        return None
        
    def invest_amount(self, amount_added):
        self.value = self.value + [self.value[len(self.value)-1]+amount_added]
        self.time = self.time + [self.time[len(self.time)-1]]
        return None

    def display_trajectory(self):
        plt.plot(self.time,self.value)
        plt.show()
        

if __name__ == "__main__":
    myIRA = account(10000)
    retire_age = 60
    current_age = 23
    annual_contribution = 5500    
    for i in range(retire_age-current_age):
        print("Beginning of year "+str(i))
        myIRA.invest_amount(annual_contribution)
        for j in range(12):
            myIRA.grow_by_month()
        print("End of year "+str(i))
    myIRA.display_trajectory()
