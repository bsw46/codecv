# This file answers the following question:
# I choose to invest money at the beginning of the year, and buy at the start.
# What are the odds that I can get a better price if I buy at the start of any later month?
# This is not the most informative result as a big market crash could make years moot.   

import quandl
import matplotlib.pyplot as plt
import numpy as np
import math
quandl.ApiConfig.api_key = "1T_wqUo4FBGts7S56NBf"
mydata = quandl.get("MULTPL/SP500_REAL_PRICE_MONTH", returns="numpy",
                    start_date="1918-01-01",end_date="")

time = []
value = []
value_log = []

for i in range(len(mydata)):
    time = time + [mydata[i][0].year+1/12*mydata[i][0].month]
    value = value + [mydata[i][1]]

time = np.array(time)
value = np.array(value)
value_log = np.log(value)
percent_change = (value[1:]-value[:(len(value)-1)])/value[:(len(value)-1)]

# The probability distribution of different percent changes
# On initial inspection, the distribution is fairly normal.
bins = 200
x_max = .55
x_min = -.3
frequency_change = [0]*bins
temp_percent_change = (percent_change -x_min)*bins/(x_max-x_min)
sum_change = 0
for i in range(len(percent_change)):
    frequency_change[int(temp_percent_change[i] + .5*(x_max-x_min)/bins)]+=1
    sum_change += percent_change[i]
average_change = sum_change/len(percent_change)

std_dev_change = 0
for i in range(len(percent_change)):
    std_dev_change += (percent_change[i]-average_change)**2
std_dev_change = (std_dev_change/len(percent_change))**.5
    
print("The average monthly change is "+str(average_change))
print("The standard deviation for the distribution is "+str(std_dev_change))

# The raw value of the S&P 500
plt.plot(time,value)
plt.xlabel("Time (year)")
plt.ylabel("Value of the S&P 500 ($)")
plt.show()


minimum_after = [0]*99
start_value = [0]*99
for i in range(100-1):
    start_value[i] = value[0+12*i]
    minimum_after[i] = value[0+12*i]
    for j in range(12):
        if minimum_after[i]>value[i*12+j]:
            minimum_after[i] = value[i*12+j]

cheaper_count = 0
for i in range(100-1):
    start_value[i] = value[0+12*i]
    for new_price in value[12*i:]:
        if new_price < start_value[i]:
            cheaper_count +=1
            break
print("It is possible to get the S&P 500 at a cheaper price than at the year start")
print(str(cheaper_count)+" out of 99 years")
