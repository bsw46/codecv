# Presents S&P growth over time.
# Presents S&P growth over time (log scale).
# Presents a histogram of month to month percent change.

import os
import re
import quandl
import matplotlib.pyplot as plt
import numpy as np
import math
import random
quandl.ApiConfig.api_key = "1T_wqUo4FBGts7S56NBf"
mydata = quandl.get("MULTPL/SP500_REAL_PRICE_MONTH", returns="numpy",
                    start_date="1918-01-01",end_date="")

def bisection_search(sorted_list,target_value,min_index,max_index):
    midpoint = (min_index + max_index)//2
    if (abs(sorted_list[midpoint]-target_value)<1/200):
        return midpoint
    elif sorted_list[midpoint]>target_value:
        return bisection_search(sorted_list,target_value,min_index,midpoint)
    else:
        return bisection_search(sorted_list,target_value,midpoint,max_index)  
def least_square(x_data,y_data):
    x_mean = np.mean(x_data)
    y_mean = np.mean(y_data)
    x_deviation = np.array(x_data)-x_mean
    y_deviation = np.array(y_data)-y_mean
    numerator = np.dot(x_deviation,y_deviation)
    denominator = np.dot(x_deviation,x_deviation)
    return numerator/denominator


time = []
value = []
value_log = []

for i in range(len(mydata)):
    time = time + [1.0/12.0*np.double(np.datetime64(mydata[i][0],'M'))]
    value = value + [mydata[i][1]]

time = np.array(time)+52
value = np.array(value)
value_log = np.log(value)
percent_change = (value[1:]-value[:(len(value)-1)])/value[:(len(value)-1)]
linear_fit=np.polyfit(time,value_log,1)

start_year = 1918
# The raw value of the S&P 500
plt.plot(start_year+time,value)
plt.plot(start_year+time,np.exp( linear_fit[0]*time+linear_fit[1]))
plt.xlabel("Time (year)")
plt.ylabel("Value of the S&P 500 ($)")
plt.show()



# The value of the S&P 500 on a logarithmic scale
plt.plot(start_year+time,value_log)
plt.plot(start_year+time,linear_fit[0]*time+linear_fit[1])
plt.xlabel("Time (year)")
plt.ylabel("Value of the S&P 500 (exponential scale, $)")
plt.show()

### The percent_change of the S&P 500
### The scale prevents this plot from being too useful.
##plt.plot(time[:len(time)-1],percent_change)
##plt.xlabel("Time (year)")
##plt.ylabel("Percent change in the value of the S&P 500 per month (%/month)")
##plt.show()

# The probability distribution of different percent changes
# On initial inspection, the distribution is fairly normal.
bins = 200
x_max = .55
x_min = -.3
frequency_change = [0]*bins
temp_percent_change = (percent_change -x_min)*bins/(x_max-x_min)
sum_change = 0
for i in range(len(percent_change)):
    frequency_change[int(temp_percent_change[i] + .5*(x_max-x_min)/bins)]+=1
    sum_change += percent_change[i]
average_change = sum_change/len(percent_change)

std_dev_change = 0
for i in range(len(percent_change)):
    std_dev_change += (percent_change[i]-average_change)**2
std_dev_change = (std_dev_change/len(percent_change))**.5
    
print("The average monthly change is "+str(average_change))
print("The standard deviation for the distribution is "+str(std_dev_change))

plt.plot([x_min+(x_max-x_min)/bins*i for i in range(bins)],frequency_change)
plt.xlabel("Monthly percent change")
plt.ylabel("Frequency")
plt.xlim(-.2,.2)
plt.show()

# The probability distribution of different percent changes
# On initial inspection, the distribution is fairly normal.
plt.hist(percent_change,bins=100)
plt.show()
years_to_check = [1,2,5,10,20]
for n_years in years_to_check:
    delta = 12*n_years
    yearly_percent_change = (value[delta:]-value[:(len(value)-delta)])/value[:(len(value)-delta)]

    print("Median percent change is " +str(np.median(yearly_percent_change)))
    print("Mean annual percent change is " +str(np.mean(yearly_percent_change)))
    print("Standard deviation of annual percent change is " +str(np.std(yearly_percent_change)))
    print("Ratio increased in asset value is " +str(sum(yearly_percent_change>0)/len(yearly_percent_change)))
    print("Ratio increased in asset value is " +str(sum(yearly_percent_change>1.03**n_years-1)/len(yearly_percent_change)))
    print()

    plt.plot([0,0],[0,65],'k--')
    plt.plot([1.03**n_years-1,1.03**n_years-1],[0,65],'k--')
    plt.hist(yearly_percent_change,bins=100)
    plt.xlabel("Change in value of S&P500")
    plt.show()


t = np.arange(0,40,.1) # short for time range
Ao=10000

adj_value=Ao*value
median_line = []
mean_line = []
q25_line = []
q75_line = []
sample_t = []

for j in range(1,480):
    sample_t = sample_t + [j/12]
    sample = adj_value[j:]/value[0:-j]
    median_line = median_line + [np.median(sample)]
    mean_line = mean_line + [np.mean(sample)]
    q25_line = q25_line + [np.percentile(sample,25)]
    q75_line = q75_line + [np.percentile(sample,75)]
    

plt.plot(t,Ao*1.0676**t,'k-',sample_t,median_line,'r-',sample_t,mean_line,'b-',t,[10000]*len(t),'k--')
plt.xlabel('Time invested (years)')
plt.ylabel('Value of investment, initially $10,000')
plt.xlim([0,41])
plt.ylim([0,None])
plt.show()

tplot = []
scatter = []
sample_t = []
for j in range(1,480):
    sample_t = sample_t + [j/12]
    sample = []
    for k in range(6):
        x=random.randint(0,len(value)-j-1)
        tplot = tplot + [j/12]
        scatter = scatter + [adj_value[x+j]/value[x]]
        sample = sample + [adj_value[x+j]/value[x]]

plt.plot(tplot,scatter,'r.',sample_t,median_line,'k-',sample_t,q25_line,'b-',sample_t,q75_line,'b-',t,[10000]*len(t),'k--')
##plt.plot(tplot,scatter,'r.',t,Ao*1.07**t,'k-',sample_t,median_line,'b-',sample_t,mean_line,'b-',t,[10000]*len(t),'k--')
plt.xlabel('Time invested (years)')
plt.ylabel('Value of investment, initially $10,000')
plt.xlim([0,41])
plt.ylim([0,None])
print("25th percentile line approximated by growth rate: "+str(np.exp(np.polyfit(sample_t+[0]*1000000,np.log(q25_line+[Ao]*1000000),1)[0])))
print("Median line approximated by growth rate: "+str(np.exp(np.polyfit(sample_t+[0]*1000000,np.log(median_line+[Ao]*1000000),1)[0])))
print("75th percentile line approximated by growth rate: "+str(np.exp(np.polyfit(sample_t+[0]*1000000,np.log(q75_line+[Ao]*1000000),1)[0])))
print("Mean line approximated by growth rate: "+str(np.exp(np.polyfit(sample_t+[0]*1000000,np.log(mean_line+[Ao]*1000000),1)[0])))

plt.show()


annual_time = time[0:-12:12]+1918
annual_change = value[12::12]/value[0:-12:12]
np.polyfit(annual_time,annual_change,1)

####

   
SP500_open = value
SP500_time = time+1918
data_points = len(SP500_time)

annual_contribution = 25000
simulation_years = 40
start = 1919
end_time = 2019
initial_years = np.arange(start,end_time-simulation_years)
print('')
print("Benchmarks based on fixed growths.")
print("Investing a total of "+str(simulation_years*annual_contribution)+" over "+ str(simulation_years) +" years.")
for i in range(1,12):
    interest = i/100.
    end_bal=annual_contribution*((1+interest)**simulation_years-1)/(1-1./(1+interest))
    print('Interest rate: '+str(interest).rjust(4)+', Final Bal: '+str(int(end_bal)).rjust(10))
print('')

end_value_A = np.zeros(initial_years.shape)
print("Strategy A: Buy the index in the first day of the year.")
# Simple strategy. Good baseline. Buy as early as possible.
for initial_year in initial_years:
    initial_index = bisection_search(SP500_time,initial_year,0,data_points)
    end_index = bisection_search(SP500_time,initial_year+simulation_years,0,data_points)
    cash = 0
    shares = 0
    temp_initial_year = initial_year + 0
    for i in range(initial_index,end_index):
        if (SP500_time[i] > temp_initial_year):
            shares += annual_contribution/SP500_open[i]
            temp_initial_year += 1
    end_value_A[initial_year-initial_years[0]] = shares*SP500_open[end_index]
    print(str(initial_year)+' - '+str(initial_year+40)+' :'+str(int(end_value_A[initial_year-initial_years[0]])).rjust(9))

plt.hist(end_value_A/1000,bins=15)
plt.xlabel("Value of account at end of 40 years ($ thousand)")
plt.ylabel("Frequency or count")
print("The average value of the account is "+str(np.mean(end_value_A)))
print("The standard deviation of account value is "+str(np.std(end_value_A)))
print("The 25th percentile value of the account is "+str(np.percentile(end_value_A,25)))
print("The median value of the account is "+str(np.median(end_value_A)))
print("The 75th percentile value of the account is "+str(np.percentile(end_value_A,75)))

print("")
plt.show()

SP500ymb = np.polyfit(np.arange(1958,2018),np.array(end_value_A),1)
plt.plot(1918+40+time[0:720:12],np.array(end_value_A)/1000,'b-',np.arange(1958,2018),np.polyval(SP500ymb,np.arange(1958,2018))/1000,'k-')
plt.xlabel("Final year of 40-year period")
plt.ylabel("Value of Account at end of 40 year period ($ thousand)")
plt.show()



end_value_B = np.zeros(initial_years.shape)        


print("Strategy B: Buy the index throughout the year.")
# Results in a slightly lower outcome, but also a lower deviation.
# Overall result is consistently lower.

for initial_year in initial_years:
    initial_index = bisection_search(SP500_time,initial_year,0,data_points)
    end_index = bisection_search(SP500_time,initial_year+simulation_years,0,data_points)
    cash = 0
    shares = 0
    purchases = 12.0
    temp_initial_year = initial_year + 0
    for i in range(initial_index,end_index):
        if (SP500_time[i] > temp_initial_year):
            shares += annual_contribution/purchases/SP500_open[i]
            temp_initial_year += 1/purchases
    end_value_B[initial_year-initial_years[0]] = shares*SP500_open[end_index]
    print(str(initial_year)+' - '+str(initial_year+40)+' :'+str(int(end_value_B[initial_year-initial_years[0]])).rjust(9))

print("The average value of the account is "+str(np.mean(end_value_B)))
print("The median value of the account is "+str(np.median(end_value_B)))
print("The standard deviation of account value is "+str(np.std(end_value_B)))
print("")
plt.hist(end_value_B/1000,bins=15)
plt.xlabel("Value of account after 40 years ($ thousand)")
plt.show()

average_difference_AB = 0
print("Difference in strategy A and B")

for initial_year in initial_years:
    print(str(initial_year)+' - '+str(initial_year+40)+' :'+str(int(end_value_A[initial_year-initial_years[0]])-int(end_value_B[initial_year-initial_years[0]])).rjust(9))
    average_difference_AB += int(end_value_A[initial_year-initial_years[0]])-int(end_value_B[initial_year-initial_years[0]])
print("The average difference of the accounts is "+str(np.mean(end_value_A-end_value_B)))
print("The median value of the account is "+str(np.median(end_value_A-end_value_B)))
print("The standard deviation of account value is "+str(np.std(end_value_A-end_value_B)))

plt.hist((end_value_B-end_value_A)/1000,bins=15)
plt.xlabel("Difference of account after 40 years ($ thousand)")
plt.show()

print("")

end_value_C = np.zeros(initial_years.shape)




print("Strategy E: Buy at minimum price each year.")
# Requires future prediction.
# A benchmark for future prediction performance 
end_value_E = np.zeros(initial_years.shape)
for initial_year in initial_years:
    initial_index = bisection_search(SP500_time,initial_year,0,data_points)
    end_index = bisection_search(SP500_time,initial_year+simulation_years,0,data_points)
    cash = 0
    shares = 0
    temp_initial_year = initial_year + 0
    for i in range(initial_index,end_index,12):
        shares += annual_contribution/min(SP500_open[i:i+12])
    end_value_E[initial_year-initial_years[0]] = shares*SP500_open[end_index]
    print(str(initial_year)+' - '+str(initial_year+40)+' :'+str(int(end_value_E[initial_year-initial_years[0]])).rjust(9))
plt.hist(end_value_E/1000,bins=15)
plt.xlabel("Value of account after 40 years ($ thousand)")
print("The average value of the account is "+str(np.mean(end_value_E)))
print("The median value of the account is "+str(np.median(end_value_E)))
print("The standard deviation of account value is "+str(np.std(end_value_E)))
plt.show()
print("")
average_difference_EA = 0
for initial_year in initial_years:
    print(str(initial_year)+' - '+str(initial_year+40)+' :'+str(int(end_value_E[initial_year-initial_years[0]])-int(end_value_A[initial_year-initial_years[0]])).rjust(9))
    average_difference_EA += int(end_value_E[initial_year-initial_years[0]])-int(end_value_A[initial_year-initial_years[0]])
print("The average difference of the accounts is "+str(np.mean(end_value_E-end_value_A)))
print("The median value of the account is "+str(np.median(end_value_E-end_value_A)))
print("The standard deviation of account value is "+str(np.std(end_value_E-end_value_A)))
print("")

