#Investment Study Repo#
This repo contains a variety of python scripts and functions I wrote to study the growth of market indices over time. I pulled historical datasets of market indices from sources such as Yahoo Finance and Quandl. I performed this study because I needed to practice python, numpy, and matplotlib, but also because I needed the real value of understanding the growth of investments for managing brokerage accounts. 

Findings: 

+ Historical annual growth of index funds averages 7 to 10 percent. (characterize\_snp500.py, characterize\_nasdaq.py).

+ Index funds have minimal (near .03 percent) annual operating costs compared to mutual funds (near 1 percent). Most mutual funds (>98 percent) fail to outperform their target index fund in the long term. 

+ With an annually maxed out Roth IRA investing in the S&P500 from age 18 to 60, the median value of the account by retirement would be 2.7 million with an IQR of 2.3 million. (ensemble\_prediction.py)

+ The performance of different investment strategies were compared with a portfolio simulator. (simulate\_portfolio\_day\_scale.py) Overall, I determined the best strategy for me was annual investment as early in the year as possible: minimal time commitment, minimal cost, minimal research, moderately high return, moderately low risk. 

+ I attempted to apply machine learning models to these datasets, but these models did not lead to predictions which significantly improved over the annual early investment strategy. 

[Back to the main repo.](https://bitbucket.org/bsw46/codecv)

