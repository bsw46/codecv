import network
import os
import re
import numpy as np
import matplotlib.pyplot as plt

def bisection_search(sorted_list,target_value,min_index,max_index):
    midpoint = (min_index + max_index)//2
    if (abs(sorted_list[midpoint]-target_value)<1/200):
        return midpoint
    elif sorted_list[midpoint]>target_value:
        return bisection_search(sorted_list,target_value,min_index,midpoint)
    else:
        return bisection_search(sorted_list,target_value,midpoint,max_index)  

os.chdir('./datasets')
SP500_stream = open('SP500.csv','r')

data=SP500_stream.read()

index_regex = re.compile(r'(\d+)-(\d+)-(\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+),(\d+.\d+),(\d+)')
                         #year   month day   open      high      low       close     adj        vol        
match_object = index_regex.findall(data)

SP500_low = []
SP500_time = []
   
    
SP500_array = np.array(match_object).transpose().astype(float)
SP500_low = np.array(SP500_array[5][:])
SP500_open = np.array(SP500_array[3][:])
SP500_time = SP500_array[0][:] + SP500_array[1][:]*(1.0/12.0) + SP500_array[2][:]*(1.0/365.0)
data_points = len(SP500_time)

training_data = []
testing_data = []
for i in range(20,12000,20):
    if np.sum(SP500_low[i:i+20]/SP500_open[i] < .99).astype(int)>0:
        training_data = training_data + [(np.array(SP500_open[i-20:i]),[0,1])]
    else:
        training_data = training_data + [(np.array(SP500_open[i-20:i]),[1,0])]
for i in range(12000,17000,20):
    if np.sum(SP500_low[i:i+20]/SP500_open[i] < .99).astype(int)>0:
        testing_data = testing_data + [(np.array(SP500_open[i-20:i]),np.array([0,1]))]
    else:
        testing_data = testing_data + [(np.array(SP500_open[i-20:i]),np.array([1,0]))]
predictor = network.Network([20,12,2])
predictor.SGD(training_data, 100,40,1,testing_data)
