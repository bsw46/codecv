#Advanced Process Controls Repo#

This repo contains the aspenplus file, the spreadsheets, and the final report for a chemical plant design. The plant was designed to produce 100M lbs. of ethanolamines per year while observing environmental regulation, thermodynamic limits, and other engineering constraints. The profitability of the plant was evaluated against the time-value of money. 
  
[Read the report.](https://bitbucket.org/bsw46/codecv/src/master/chemical_plant/FINAL%20REPORT.pdf)

[Back to the main repo.](https://bitbucket.org/bsw46/codecv)
